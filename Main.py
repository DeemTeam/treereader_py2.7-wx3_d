#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

from platform import system as OSname
from sys import exit, argv
from time import sleep
from pg import connect

import wx, os, MainFrame, mmap
import lib.Configurator as Configurator
import lib.GeneralFunc as GeneralFunc

def DisconnectDB(db_hand):
	if db_hand : db_hand.close()

def ConnectDB(Configurator):
	Configurator.db = connect(dbname=Configurator.pgDatabase, host=Configurator.pgServerIP, port=5432, user=Configurator.pgUser, passwd=Configurator.pgPassword)
# ---- Main Application -----
class mainApp(wx.App):
	def OnInit(self):
		# ---- Get Working Directories ----
		SoftDir = os.path.split(os.path.abspath(argv[0]))[0]
		
		SwapName = os.path.join(SoftDir,'swap')
		FileToOpen = False
		
		# ---- Trying to find tree file to open ----- 
		if len(argv)>1:
			FileArg = argv[1]
			if os.path.isfile(FileArg) and (FileArg.find('.tre')>0 or FileArg.find('.ph')>0):
				FileToOpen = FileArg
		
		# --- Trying to contact running file ----
		if FileToOpen:
			#print "Trying to Send Open File" 
			if OSname() =='Windows': SwapObj = open(SwapName,'r+')
			else: SwapObj = open(SwapName,'w')
			SwapObj.write("OPEN:"+FileToOpen)
			SwapObj.close()
			sleep(1)
			SwapObj = open(SwapName,'r')
			data = SwapObj.readline()
			#print data
			if data.find('DONE')==0 : 
				#print "Got You"
				SwapObj.close()
				exit()
			SwapObj.close()
		else :
			#print "Trying to test running"
			if OSname() =='Windows': SwapObj = open(SwapName,'r+')
			else: SwapObj = open(SwapName,'w')
			SwapObj.write("RUNNING")
			SwapObj.close()
			sleep(1)
			SwapObj = open(SwapName,'r')
			data = SwapObj.readline()
			if data.find('YES')==0 : 
				SwapObj.close()
				exit()
			SwapObj.close()
			
		# ---- Get Configuration from .ini file ----
		iniObj = open(os.path.join(SoftDir, 'config.ini'),'r')
		self.Configurator = Configurator.importIniFile(iniObj)
		iniObj.close()

		# ---- Get Data Directories ----
		self.Configurator.SoftDir = SoftDir
		self.Configurator.ImgDir = os.path.join(SoftDir, 'img')
		self.Configurator.DbData = os.path.join(SoftDir, 'dbdata')

		# ---- Connection to db -------
		print  'self.Configurator.UseDB', self.Configurator.UseDB
		if self.Configurator.UseDB:
			#try: 
			ConnectDB(self.Configurator)
			#	print "Connected to db"
			#except:
			#	print "Unable to connect to database"
			#	exit()
		else : self.Configurator.db = False
		
		# Getting group UserID
		if self.Configurator.db:
			try : 
				self.Configurator.UserID = self.Configurator.db.query("SELECT db_users_id from db_users WHERE db_users_name='"+self.Configurator.UserName+"' LIMIT 1").getresult()[0][0]
			except : 
				pass

		print 'User ID', self.Configurator.UserID

		self.SetAppName("TreeReader")
		
		mainWindow = MainFrame.mainFrame(None,"MpT",self.Configurator)
		mainWindow.Show(True)
		self.SetTopWindow(mainWindow)
		if self.Configurator.UseColors:
			mainWindow.LoadColorFiles()
			mainWindow.LoadSettingFrame()
		
		# ---- Setting up SwapMap ANd Timer ------
		SwapFile = open(SwapName,'w')
		i=0
		while i<=500 : 
			SwapFile.write(" ")
			i+=1
		SwapFile.close()
		
		if OSname() =='Windows':
			self.SwapFile = open(SwapName,'r+')
			SwapMap =  mmap.mmap(self.SwapFile.fileno(), 500)
			self.FileTimer = GeneralFunc.FileTimer(SwapMap, mainWindow)
			self.FileTimer.Start(1000)
		else:
			self.SwapFile = open(SwapName,'rw+')
			SwapMap =  mmap.mmap(self.SwapFile.fileno(), 500)
			self.FileTimer = GeneralFunc.FileTimer(SwapMap, mainWindow)
			self.FileTimer.Start(1000)
		
		# ---- Trying to open tree file if asked -----
		if FileToOpen : mainWindow.OpenTree(FileToOpen, os.path.split(FileToOpen)[1])
		
		return True
		
	def OnExit(self):
		self.FileTimer.Stop()
		self.SwapFile.close()
		del self.FileTimer
		# ---- Closing db ----
		if self.Configurator.db: DisconnectDB(self.Configurator.db)

def main():
	app = mainApp(False)
	app.MainLoop()

#----------------------------------------------------------------------------

if __name__ == '__main__':
	__name__ = 'main'
	main()

