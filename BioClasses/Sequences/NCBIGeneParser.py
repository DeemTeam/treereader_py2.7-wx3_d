#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from pg import escape_string

def GetNCBIGenomeData(inFile):
	ifObj = open(inFile,'r')
	XMLdata = ''
	for line in ifObj:
		XMLdata += line.strip('\r\n')
	ifObj.close()
	TempoObj = NcbiGeneParser()
	return TempoObj.DoParse(XMLdata)

class Gene(object):
	def __init__(self):
		self.GeneName = 'DEFAULT'
		self.GeneGI = 'DEFAULT'
		self.SeqFrom = 'DEFAULT'
		self.SeqTo = 'DEFAULT'
		self.Strand = 'DEFAULT'
		self.GeneFunc = 'DEFAULT'

#<Xtra-Terms>
#            <Xtra-Terms_tag>PROTEIN</Xtra-Terms_tag>
#            <Xtra-Terms_value>118195411</Xtra-Terms_value>
#        </Xtra-Terms>


class NcbiGeneParser(object):
	def __init__(self):
		self.Entrezgene_open = False
		self.Gene_ref_locus_tag_open = False 
		self.Gene_commentary_products_open = False
		self.Gene_commentary_heading_open = False
		self.Object_id_id_open = False
		self.Seq_interval_from_open = False
		self.Seq_interval_to_open = False
		self.Strand_open = False
		self.Prot_ref_name_E_open = False
		
		self.GetProtID = False
		
		self.CurrentGene = Gene()
		self.Genes = []
	
	def DoParse(self, XMLdata):
		open_tag = False
		close_tag = False
		inside_tag = False
		between_tag = False
		current_tag = ''
		current_chars = ''
		for char in XMLdata:
			if char == '<':
				if between_tag :
					#print "Chars = "+current_chars
					self.characters(current_chars)
					current_chars = ''
					between_tag = False
				inside_tag = True
				open_tag = True
			elif char == '>':
				if open_tag:
					#print "Open tag = "+current_tag
					self.startElement(current_tag)
					current_tag = ''
					open_tag = False
					inside_tag = False
					between_tag = True
				if close_tag:
					#print "Close tag = "+current_tag
					self.endElement(current_tag)
					current_tag = ''
					close_tag = False
					inside_tag = False
			elif char == '/':
				if inside_tag:
					close_tag = True
					open_tag = False
			else :
				if inside_tag :
					current_tag += char
				if between_tag :
					current_chars += char
		#Renvoi des lignes de blast
		if len(self.Genes)==0:
			return False, self.query
		else:
			return True, self.Genes
	
	def startElement(self, name):
	#"fonction appelee lorsque le parser rencontre une balise ouvrante"
		if name=='Entrezgene':				self.Entrezgene_open = True
		if name=='Gene-ref_locus-tag':		self.Gene_ref_locus_tag_open = True
		if name=='Gene-commentary_products':self.Gene_commentary_products_open = True
		if name=='Gene-commentary_heading':	self.Gene_commentary_heading_open = True
		if name=='Object-id_id':			self.Object_id_id_open = True
		if name=='Seq-interval_from':		self.Seq_interval_from_open = True
		if name=='Seq-interval_to':			self.Seq_interval_to_open = True
		if name=='Strand':					self.Strand_open = True
		if name=='Prot-ref_name_E':			self.Prot_ref_name_E_open = True

	def characters(self, ch):
	#"fonction appelee lorsque le parser rencontre des donnees dans un element"
		if self.Entrezgene_open : pass
		if self.Gene_ref_locus_tag_open : self.CurrentGene.GeneName = ch
		if self.Gene_commentary_products_open : pass
		if self.Gene_commentary_heading_open :
			if ch == 'Product' : self.GetProtID = True
		if self.Object_id_id_open : 
			if self.GetProtID:
				self.CurrentGene.GeneGI = ch
		if self.Seq_interval_from_open : self.CurrentGene.SeqFrom = int(ch)
		if self.Seq_interval_to_open : self.CurrentGene.SeqTo = int(ch)
		if self.Strand_open : 
			if ch == 'plus': self.CurrentGene.Strand = 1
			elif ch == 'minus' : self.CurrentGene.Strand = -1
		if self.Prot_ref_name_E_open : 
			ch = ch.replace(':','')
			ch = ch.replace("'",'p')
			ch = ch.replace(";",'')
			ch = ch.replace("(",'')
			ch = ch.replace(")",'')
			ch = ch.replace(",",'')
			ch = ch.replace("\\",'')
			ch = ch.replace("/",'')
			self.CurrentGene.GeneFunc = ch[:98]
	
	def endElement(self, name):
	#"fonction appelee lorsque le parser rencontre une balise fermante"
		if name=='Entrezgene':
			self.Entrezgene_open = False
			self.Genes.append(self.CurrentGene)
			self.CurrentGene = Gene()
		if name=='Gene-ref_locus-tag':		self.Gene_ref_locus_tag_open = False
		if name=='Gene-commentary_products':
			self.Gene_commentary_products_open = False
		if name=='Gene-commentary_heading':	self.Gene_commentary_heading_open = False
		if name=='Object-id_id':			
			self.Object_id_id_open = False
			self.GetProtID = False
		if name=='Seq-interval_from':		self.Seq_interval_from_open = False
		if name=='Seq-interval_to':			self.Seq_interval_to_open = False
		if name=='Strand':					self.Strand_open = False
		if name=='Prot-ref_name_E':			self.Prot_ref_name_E_open = False
		
		
