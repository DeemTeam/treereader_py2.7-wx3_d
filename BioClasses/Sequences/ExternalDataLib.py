# -*- coding: iso-8859-1 -*-

import os


class ExtGroup():
	def __init__(self, parent):
		self.GpName = ''
		self.Members = []
		self.Annotations = []
		self.parent = parent

	def AddMember(self, LocalID):
		if LocalID not in self.Members : 
			self.Members.append(LocalID)
			self.parent.AddToAllMembers(LocalID)

	def RemoveMember(self, LocalID):
		if LocalID in self.Members : 
			self.Members.remove(LocalID)
			self.parent.RemoveFromAllMembers(LocalID)

	def UpdateAnnotations(self, CommaSepAnnotations):
		CommaSepAnnotations = CommaSepAnnotations.strip('\r\n,')
		CommaSepAnnotations = CommaSepAnnotations.replace('\r\n<>','')
		self.Annotations = []
		NewAnnotationsList = CommaSepAnnotations.split(',')
		for NewAnnotation in NewAnnotationsList :
			self.Annotations.append(NewAnnotation)

class ExtDataStruct(list):
	def __init__(self, inFile):
		# Data Stored in the object
		self.ListOfGpNames = []
		self.CompleteListOfMembers = []

		# Single sequences Annotations
		self.SingleAnnotations = {}
		self.AnnotatedLocalIDs = []

		# For parsing purposes
		self.ExtGpList_open = False
		self.ExtGp_open = False
		self.ExtGpName_open = False
		self.ExtGpMember_open = False
		self.ExtGpAnnotation_open = False

		self.SingleAnnotationsList_open = False
		self.SingleAnnotation_open = False
		self.LocalID_open = False

		self.CurrentLocalID = False
		self.CurrentGp = False

		print "Starting XML Parser"
		ifObj = open(inFile,'r')
		XMLdata = ''
		for line in ifObj:
			XMLdata += line.strip('\r\n')
		ifObj.close()
		open_tag = False
		close_tag = False
		inside_tag = False
		between_tag = False
		current_tag = ''
		current_chars = ''
		for char in XMLdata:
			if char == '<':
				if between_tag :
					#print "Chars = "+current_chars
					self.characters(current_chars)
					current_chars = ''
					between_tag = False
				inside_tag = True
				open_tag = True
			elif char == '>':
				if open_tag:
					#print "Open tag = "+current_tag
					self.startElement(current_tag)
					current_tag = ''
					open_tag = False
					inside_tag = False
					between_tag = True
				if close_tag:
					#print "Close tag = "+current_tag
					self.endElement(current_tag)
					current_tag = ''
					close_tag = False
					inside_tag = False
			elif char == '/':
				if inside_tag:
					close_tag = True
					open_tag = False
			else :
				if inside_tag :
					current_tag += char
				if between_tag :
					current_chars += char
		#Renvoi des lignes de blast

		print "Done"
	
	def startElement(self, name):
	#"fonction appelee lorsque le parser rencontre une balise ouvrante"
		if name=='ExtGpList':			self.ExtGpList_open = True
		if name=='ExtGp':				
			self.ExtGp_open = True
			self.CurrentGp = ExtGroup(self)
		if name=='GpName':				self.ExtGpName_open = True
		if name=='Member':				self.ExtGpMember_open = True
		if name=='Annotation':			self.ExtGpAnnotation_open = True
		if name=='SingleAnnotationsList':		self.SingleAnnotationsList_open = True
		if name=='SingleAnnotation':			self.SingleAnnotation_open = True
		if name=='LocalID':						self.LocalID_open = True

	def characters(self, ch):
	#"fonction appelee lorsque le parser rencontre des donnees dans un element"
		if self.ExtGpName_open:				
			self.CurrentGp.GpName = ch
			self.ListOfGpNames.append(ch)
		if self.ExtGpMember_open: 			
			self.CurrentGp.Members.append(ch)
			self.CompleteListOfMembers.append(ch)
		if self.ExtGpAnnotation_open:		
			if self.ExtGp_open:
				self.CurrentGp.Annotations.append(ch)
			elif self.SingleAnnotation_open:
				self.SingleAnnotations[self.CurrentLocalID].append(ch)
		if self.SingleAnnotationsList_open:
			if self.LocalID_open:
				if ch not in self.AnnotatedLocalIDs:
					self.SingleAnnotations[ch] = []
					self.AnnotatedLocalIDs.append(ch)
				self.CurrentLocalID = ch

	def endElement(self, name):
	#"fonction appelee lorsque le parser rencontre une balise fermante"
		if name=='ExtGpList':			self.ExtGpList_open = False
		if name=='ExtGp':				
			self.ExtGp_open = False
			self.append(self.CurrentGp)
		if name=='GpName':				self.ExtGpName_open = False
		if name=='Member':				self.ExtGpMember_open = False
		if name=='Annotation':			self.ExtGpAnnotation_open = False
		if name=='SingleAnnotationsList':	self.SingleAnnotationsList_open = False
		if name=='SingleAnnotation':		self.SingleAnnotation_open = False
		if name=='LocalID':					self.LocalID_open = False

	def ExtractXMLdata(self, FileHandler):
		# Converts the object into XML string to save to file
		print "Outputing XML"
		FileHandler.write("<SingleAnnotationsList>\n")
		for LocalID, AnnotationList in self.SingleAnnotations.iteritems():
			FileHandler.write("\t<SingleAnnotation>\n")
			# Write LocalID
			FileHandler.write("\t\t<LocalID>"+LocalID+"</LocalID>\n")
			# Write Annotations
			for Annotation in AnnotationList:
				FileHandler.write("\t\t<Annotation>"+Annotation+"</Annotation>\n")
			FileHandler.write("\t</SingleAnnotation>\n")
		FileHandler.write("</SingleAnnotationsList>\n")

		FileHandler.write("<ExtGpList>\n")
		for Group in self:
			FileHandler.write("\t<ExtGp>\n")
			# Write Name
			FileHandler.write("\t\t<GpName>"+Group.GpName+"</GpName>\n")
			# Write Members
			for Member in Group.Members:
				FileHandler.write("\t\t<Member>"+Member+"</Member>\n")
			# Write Annotations
			for Annotation in Group.Annotations:
				FileHandler.write("\t\t<Annotation>"+Annotation+"</Annotation>\n")
			FileHandler.write("\t</ExtGp>\n")
		FileHandler.write("</ExtGpList>\n")
		print "Done"

	def AddGroup(self, GpName=False):
		NewGroup = ExtGroup(self)
		if GpName : NewGroup.GpName = GpName
		self.append(NewGroup)
		return NewGroup
	
	def GetSingleAnnotations(self, ListofLocalID):
		ListOfSingleAnnotations = []
		LocalIDsAlreadyAnnotated = self.SingleAnnotations.keys()
		for LocalID in ListofLocalID:
			if str(LocalID) in LocalIDsAlreadyAnnotated:
				for Annotation in self.SingleAnnotations[LocalID]:
					if Annotation not in ListOfSingleAnnotations:
						ListOfSingleAnnotations.append(Annotation)
		return ListOfSingleAnnotations

	def AddSingleAnnotations(self, ListofLocalID, CommaSepAnnotations):
		CommaSepAnnotations = CommaSepAnnotations.strip('\r\n,')
		CommaSepAnnotations = CommaSepAnnotations.replace('\r\n<>','')
		NewAnnotationsList = CommaSepAnnotations.split(',')
		
		LocalIDsAlreadyAnnotated = self.SingleAnnotations.keys()

		for LocalID in ListofLocalID:
			if str(LocalID) not in LocalIDsAlreadyAnnotated:
				self.SingleAnnotations[str(LocalID)] = []
			for NewAnnotation in NewAnnotationsList:
				if NewAnnotation not in self.SingleAnnotations[str(LocalID)]:
					self.SingleAnnotations[str(LocalID)].append(NewAnnotation)

	def RemoveSingleAnnotations(self, ListofLocalID, Annotation):
		LocalIDsAlreadyAnnotated = self.SingleAnnotations.keys()
		for LocalID in ListofLocalID:
			if str(LocalID) in LocalIDsAlreadyAnnotated:
				if Annotation in self.SingleAnnotations[str(LocalID)]:
					self.SingleAnnotations[str(LocalID)].remove(Annotation)
			# Check if dict is empty and erase it
				if len(self.SingleAnnotations[str(LocalID)])==0:
					del self.SingleAnnotations[str(LocalID)]

	def AddToAllMembers(self, LocalID):
		if LocalID not in self.CompleteListOfMembers : 
			self.CompleteListOfMembers.append(LocalID)

	def RemoveFromAllMembers(self, LocalID):
		# Test First if this Sequences is indeed absent from any groups before deleting it
		DoRemoveFromAllMembers = True
		for Group in self:
			if LocalID in Group.Members : DoRemoveFromAllMembers = False
		if DoRemoveFromAllMembers:
			if LocalID in self.CompleteListOfMembers : 
				self.CompleteListOfMembers.remove(LocalID)

