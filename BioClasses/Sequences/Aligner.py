#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from os import system as StartCommand
from BioClasses import BioPATH
from BioClasses import GetFilePrefix

def RunAligner(Filename, Software='muscle', MakeSeqNames=False):
	if Software in ['muscle','probcons','mafft']:
		# Make seq naes if required
		if MakeSeqNames:
			SeqNameFile = MakeSeqNamesList(Filename)
		# Lauch the work
		if Software == 'muscle' : AlignedFile = RunMuscle(Filename)
		if Software == 'probcons' : AlignedFile = RunProbcons(Filename)
		if Software == 'mafft' : AlignedFile = RunMafft(Filename)
		# Restore seqnames if required
		if MakeSeqNames:
			RestoreSeqNamesInAlignment(Filename, SeqNameFile)
			RestoreSeqNamesInAlignment(AlignedFile, SeqNameFile)
			
		return AlignedFile
	else :
		raise Exception('Software option incorrect')
	
def RunProbcons(Filename):
	# Preparinf file names
	AlignedFile = GetFilePrefix(Filename) + ".aln"
	# Running ProbCons
	print "Running ProbCons on " + Filename
	cmd = BioPATH.probcons + " " + Filename + " > " + AlignedFile
	StartCommand(cmd)
	# Getting resultFile
	return AlignedFile
	
def RunMuscle(Filename):
	AlignedFile = GetFilePrefix(Filename) + ".aln"
	print "Running Muscle on "+Filename
	
	cmd = BioPATH.muscle + " -maxmb 1000 -in " + Filename + " -out " + AlignedFile
	StartCommand(cmd)
	# Getting resultFile
	return AlignedFile
	
def RunMafft(Filename):
	AlignedFile = GetFilePrefix(Filename) + ".aln"
	print "Running mafft on "+Filename
	cmd = BioPATH.mafft + " --auto " + Filename + " > " + AlignedFile
	StartCommand(cmd)
	return AlignedFile

