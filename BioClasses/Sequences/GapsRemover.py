#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from BioClasses import BioPATH
from BioClasses import GetFilePrefix
from BioClasses.Sequences import MyFasta
from random import randint
from os import system as StartCommand
from os import remove as RemoveFile

def RunUngap(Filename, Software='BMGE', PercentGapTolerance=20, MakeSeqNames=False):
	AlnLen = 0
	if Software in ['gblocks','BMGE','Homemade','TrimAl_gappyout','TrimAl_strict','TrimAl_automated1']:
		# Make seq naes if required
		if MakeSeqNames:
			SeqNameFile = MakeSeqNamesList(Filename)
		# Lauch the work
		if Software == 'gblocks' : UngappedFile = RunGblocks(Filename)
		if Software == 'BMGE' :
			GapTolerance = float(PercentGapTolerance) / 100.0
			UngappedFile, AlnLen = BMGE(Filename, GapTolerance)
		if Software == 'Homemade' : UngappedFile = MyGapsRemover(Filename, PercentGapTolerance)
		if Software == 'TrimAl_gappyout' : UngappedFile, AlnLen = TrimAl(Filename, 'gappyout')
		if Software == 'TrimAl_strict' : UngappedFile, AlnLen = TrimAl(Filename, 'strict')
		if Software == 'TrimAl_automated1' : UngappedFile, AlnLen = TrimAl(Filename, 'automated1')
		AddQuestionMarks(UngappedFile)
		# Restore seqnames if required
		if MakeSeqNames: 
			RestoreSeqNamesInAlignment(Filename, SeqNameFile)
			RestoreSeqNamesInAlignment(UngappedFile, SeqNameFile)
			
		return UngappedFile, AlnLen
	else :
		raise Exception('Software option incorrect')
	
def BMGE(Filename, GapTolerance=0.2):
	# Preparing filnames
	UngappedFile = GetFilePrefix(Filename) + '.gap'
	TemporaryFile = str(randint(10000, 99999)) + ".txt"
	# Launching BMGE
	cmd = "java -jar "+ BioPATH.BMGE + " -i " + Filename + " -o " + TemporaryFile +" -t AA -g "+str(GapTolerance)
	print "running BMGE on " + Filename
	StartCommand(cmd)
	# Retrieving data from tempo file
	FastaObj = MyFasta(TemporaryFile,'BMGE')
	ofObj = open(UngappedFile, 'w')
	ofObj.write(FastaObj.FullOutputInFasta())
	ofObj.close()
	# Removing tempo file
	RemoveFile(TemporaryFile)
	# Returning File name 
	return UngappedFile, FastaObj.LenAln

def TrimAl(Filename, mode='gappyout'):
	# Preparing filnames
	UngappedFile = GetFilePrefix(Filename) + '.gap'
	TemporaryFile = str(randint(10000, 99999)) + ".txt"
	# Launching BMGE
	cmd = BioPATH.TrimAl + " -in " + Filename + " -out " + TemporaryFile +" -"+str(mode)
	print "running TrimAl ("+mode+") on " + Filename
	StartCommand(cmd)
	# Retrieving data from tempo file
	FastaObj = MyFasta(TemporaryFile)
	for seq in FastaObj.SequencesList:
		seq.definition = seq.definition.split(' ')[0]
	ofObj = open(UngappedFile, 'w')
	ofObj.write(FastaObj.FullOutputInFasta())
	ofObj.close()
	# Removing tempo file
	RemoveFile(TemporaryFile)
	# Returning File name 
	return UngappedFile, FastaObj.LenAln
	
def RunGblocks(Filename):
	# Preparing filnames
	UngappedFile = GetFilePrefix(Filename) + '-gb'
	# Launching Gblocks
	print "Running Gblocks on " + Filename
	cmd = gblocks_path + ' ' + muscle_outfile + " -b6=y -g"
	StartCommand(cmd)
	# Returning File name 
	return UngappedFile
	
def MyGapsRemover(Filename, GapTolerance = False):
	UngappedFile = GetFilePrefix(Filename) + '.gap'
	seqNames = []
	seqSeqs = []
	
	# Fill the lists with sequences
	ifObj=open(Filename,'r')
	addseq = False 
	seqline = ''
	for line in ifObj:
		line = line.strip('\r\n')
		if not line.startswith('#'):
			if line.startswith('>'):
				if addseq :
					seqSeqs.append(seqline)
					seqline = ''
					addseq = False
				seqNames.append(line.strip('>'))
			else : 
				line = line.replace('*','-')
				seqline+=line
				addseq = True
	seqSeqs.append(seqline)
	ifObj.close()
	#--------------------------------------------------------------------------#
	
	gapObj = open(UngappedFile,'w')
	# Check if all sequences are of the same length
	nbPosition = len(seqSeqs[0])
	for elem in seqSeqs:
		if len(elem) != nbPosition :
			raise Exception("Error : All sequences don't have the same length")
	#--------------------------------------------------------------------------#
	nbSequence =  len(seqNames)
	#Calculate the number of gaps at each position
	positionstate = []
	for position in range(nbPosition):
		nbTiret = 0
		for i in range(nbSequence):
			if seqSeqs[i][position] == '-':
				nbTiret+=1
		PCgaps = float(nbTiret)/nbSequence*100
		positionstate.append(PCgaps)
	
	if not GapTolerance :
		gapChoiceTab = ['',5,10,20,30,40,50]
		ratiosList = {}
		ratiosList['5'] = 0
		ratiosList['10'] = 0
		ratiosList['20'] = 0
		ratiosList['30'] = 0
		ratiosList['40'] = 0
		ratiosList['50'] = 0
		for PCgaps in positionstate:
			if PCgaps <= 5 :
				ratiosList['5']+=1
			if PCgaps <= 10 :
				ratiosList['10']+=1
			if PCgaps <= 20 :
				ratiosList['20']+=1
			if PCgaps <= 30 :
				ratiosList['30']+=1
			if PCgaps <= 40 :
				ratiosList['40']+=1
			if PCgaps <= 50 :
				ratiosList['50']+=1

		print "Choose the %age of gaps to accept:"
		print "1) 5% "+str(ratiosList['5'])+" sites"
		print "2) 10% "+str(ratiosList['10'])+" sites"
		print "3) 20% "+str(ratiosList['20'])+" sites"
		print "4) 30% "+str(ratiosList['30'])+" sites"
		print "5) 40% "+str(ratiosList['40'])+" sites"
		print "6) 50% "+str(ratiosList['50'])+" sites"
		gapsChoice = raw_input("\nYour Choice: ")
		gapsChoice = int(gapsChoice)
		if 0 > gapsChoice > 6 :
			gapsChoice = 6
		gapsToUse = gapChoiceTab[gapsChoice]
	
	else:
		gapsToUse = int(GapTolerance)
	
	#--------------------------------------------------------------------------#
	#Create the New lines
	
	newSeqs = []
	for i in range(nbSequence):
		newSeqs.append('')
		for position in range(nbPosition):
			if positionstate[position] < gapsToUse:
				newSeqs[i]+= seqSeqs[i][position]
	#--------------------------------------------------------------------------#
	for i in range(nbSequence):
		gapObj.write(">"+seqNames[i]+"\n"+newSeqs[i]+"\n")
	
	return UngappedFile

def AddQuestionMarks(Filename):
	FastaObj = MyFasta(Filename)
	for seq in FastaObj.SequencesList:
		seq0 = seq.sequence
		partial_seq = ''
		inseq = False
		i=0
		for char in seq0:
			if char == "-" and not inseq:
				i+=1
				partial_seq = partial_seq + '?'
			else:
				inseq = True
				partial_seq = partial_seq + char
		seq0 = partial_seq
		partial_seq = ''
		i = -1
		inseq = False
		while i > (-1 - len(seq0)):
			if seq0[i] == '-' and not inseq:
				partial_seq = '?' + partial_seq
			else:
				inseq = True
				partial_seq = seq0[i] + partial_seq
			i = i - 1
		seq.sequence=partial_seq
	ofObj = open(Filename,'w')
	ofObj.write(FastaObj.FullOutputInFasta())
	ofObj.close()
	
