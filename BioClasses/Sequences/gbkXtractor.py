#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from BioClasses.Sequences import MyFasta
from BioClasses import GetFilePrefix

def Extract(Filename):
	
	outFasta = MyFasta()
	
	insideCDS = False
	locusTag = ''
	inlocusTag = False
	product =  ''
	inproduct = False
	translation =  ''
	intranslation = False
	closingCDS = False

	ifObj = open(Filename,'r')
	logObj = open("LogData",'w')
	for line in ifObj:

		if closingCDS:
			
			product = product.replace('\n',' ')
			product = product.replace("                     ",'')
			translation = translation.replace('\n','')
			translation = translation.replace(' ','')

			logObj.write(locusTag+"|"+product+" "+translation+'\n')
			outFasta.AddEntry(locusTag+"|"+product,translation)


			insideCDS = False
			locusTag = ''
			inlocusTag = False
			product =  ''
			inproduct = False
			translation =  ''
			intranslation = False
			closingCDS = False
			logObj.write("Closing CDS"+'\n')

			

		if line.find("   CDS    ")>0:
			insideCDS = True
			logObj.write("Opening CDS"+'\n')
		if insideCDS:
			if line.find("/locus_tag=")>0:
				for char in line:
					if char == '"':
						if not inlocusTag: 
							inlocusTag = True
							logObj.write("Opening locusTag"+'\n')
						elif inlocusTag: 
							inlocusTag = False
							logObj.write("First Closing locusTag"+" "+locusTag+'\n')
					elif inlocusTag : locusTag+= char
			elif inlocusTag:
				for char in line:
					if char == '"': 
						inlocusTag = False
						logObj.write("Second Closing locusTag"+" "+locusTag+'\n')
					elif inlocusTag: locusTag+= char

			if line.find("/product=")>0:
				for char in line:
					if char == '"':
						if not inproduct: 
							inproduct = True
							logObj.write("Opening product"+'\n')
						elif inproduct: 
							inproduct = False
							logObj.write("First Closing product"+'\n')
					elif inproduct : product+= char
			elif inproduct:
				for char in line:
					if char == '"': 
						inproduct = False
						logObj.write( "Second Closing product"+'\n')
					elif inproduct: product+= char


			if line.find("/translation=")>0:
				for char in line:
					if char == '"':
						if not intranslation: 
							intranslation = True
							logObj.write( "Opening translation"+'\n')
						elif intranslation: 
							intranslation = False
							logObj.write( "First Closing translation"+'\n')
							closingCDS = True
					elif intranslation : translation+= char
			elif intranslation:
				for char in line:
					if char == '"': 
						intranslation = False
						logObj.write( "Second Closing translation"+'\n')
						closingCDS = True
					elif intranslation: translation+= char

	ofObj = open(GetFilePrefix(Filename)+'.ext.fas','w')
	ofObj.write(outFasta.FullOutputInFasta())
	ofObj.close()
	