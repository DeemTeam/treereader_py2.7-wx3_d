#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import math
from os.path import isfile as CheckfileExist

ScoreTable = {
			'!':0,'"':1,'#':2,'$':3,'%':4,'&':5,"'":6,'(':7,')':8,'*':9,'+':10,
			',':11,'-':12,'.':13,'/':14,'0':15,'1':16,'2':17,'3':18,'4':19,'5':20,
			'6':21,'7':22,'8':23,'9':24,':':25,';':26,'<':27,'=':28,'>':29,'?':30,
			'@':31,'A':32,'B':33,'C':34,'D':35,'E':36,'F':37,'G':38,'H':39,'I':40,'J':41
			}

ScoreList = [	
			'!','"','#','$','%','&',"'",'(',')','*','+',',','-','.','/','0',
			'1','2','3','4','5','6','7','8','9',':',';','<','=','>','?','@',
			'A','B','C','D','E','F','G','H','I','J'
			]

class FastqEntry(object):
	def __init__(self, identifier, sequence, quality, Address=False):
		self.identifier = identifier.strip('@')
		self.sequence = sequence
		self.quality = quality
		self.Address = Address

	def PrintInFastq(self):
		sortie = '@'+self.identifier+"\n"+self.sequence+"\n+\n"+self.quality+'\n'
		return sortie

	def ContainsLowQualityBases(self, QualityTreshold=0.10):
		if QualityTreshold < 1.0 and QualityTreshold > 0.0:
			ToBeReturned = False
			SubScoreList = ScoreList[:int(41*QualityTreshold)]
			for Char in self.quality:
				if Char in SubScoreList : ToBeReturned = True
			return ToBeReturned
		else : print "Error: QualityTreshold argument must be 0< real <1"

	def GetTotalScore(self):
		TotalScore = 0
		for Char in self.quality:
			TotalScore += ScoreTable[Char]
		return TotalScore

	def GetBestlen(self):
		return len(self.quality.strip('#'))

	def GetScoreRatio(self):
		Bestlen = self.GetBestlen()
		if Bestlen > 0: 
			ScoreRatio = int(math.ceil(float(TotalScore)/(self.Bestlen*41)*100))
		return ScoreRatio

	def GetSeqLen(self):
		return len(self.sequence)

	def GetAddress(self):
		return self.Address

def ReadFastq(InputFile):
	# Parsing Input File
	if CheckfileExist(InputFile):
		ifObj = open(InputFile,'r')
		InsideEntry = False
		for line in ifObj:
			if line.startswith('@') and not InsideEntry:
				InsideEntry = True
				FullLine = line
				numNewLine = 1
			else : 
				numNewLine += 1
				FullLine += line
			if numNewLine == 4:
				data = FullLine.split('\n')
				yield FastqEntry(data[0], data[1], data[3])
				InsideEntry = False
		ifObj.close()
	else : raise Exception, "Error parsing file! file does not exist"


class MyFastq(list):
	def __init__(self, InputFile):
		# Variables
		self.InputFile = InputFile
		# Parsing Input File
		if CheckfileExist(InputFile):
			ifObj = open(self.InputFile,'r')
			InsideEntry = False
			counter = 0
			for line in ifObj:
				if line.startswith('@') and not InsideEntry:
					InsideEntry = True
					StartPos = counter
					numNewLine = 1
					counter += len(line)
				else : 
					numNewLine += 1
					counter += len(line)
				if numNewLine == 4:
					self.append((StartPos,counter-StartPos))
					InsideEntry = False
			if InsideEntry :
				self.append((StartPos,counter-1-StartPos))
				InsideEntry = False

			ifObj.close()

		else : raise Exception, "Error parsing file! file does not exist"

	def RemoveEntry(self, Address):
		try: self.remove(Address)
		except : print "This Address does not exist"

	def IterEntries(self):
		ifObj = open(self.InputFile,'rb')
		for Address in self:
			ifObj.seek(Address[0])
			data = ifObj.read(Address[1]).split('\n')
			yield FastqEntry(data[0], data[1], data[3], Address)
		ifObj.close()

	def SaveInFile(self, Filename):
		ofObj = open(Filename,'w')
		for Entry in self.IterEntries():
			ofObj.write(Entry.PrintInFastq())
		ofObj.close()

	def GetNumOfEntries(self):
		return len(self)

	def SortEntriesUsingIndentifiers(self):
		IsDone = False
		ifObj = open(self.InputFile,'rb')
		while not IsDone:
			IsDone = True

			for i in range(len(self)-1):
				ifObj.seek(self[i][0])
				data = ifObj.read(self[i][1]).split('\n')
				Seq_i = FastqEntry(data[0], data[1], data[3], self[i])

				ifObj.seek(self[i+1][0])
				data = ifObj.read(self[i+1][1]).split('\n')
				Seq_iplus1 = FastqEntry(data[0], data[1], data[3], self[i+1])

				if  Seq_i.identifier > Seq_iplus1.identifier:
					#print "Moving sequence up"
					TempStorage = self[i]
					self[i] = self[i+1]
					self[i+1] = TempStorage
					IsDone = False


class MyFastq_real(list):
	def __init__(self, InputFile):
		# Variables
		self.InputFile = InputFile
		# Parsing Input File
		if CheckfileExist(InputFile):
			ifObj = open(InputFile,'r')
			InsideEntry = False
			for line in ifObj:
				if line.startswith('@') and not InsideEntry:
					InsideEntry = True
					FullLine = line
					numNewLine = 1
				else : 
					numNewLine += 1
					FullLine += line
				if numNewLine == 4:
					data = FullLine.split('\n')
					self.append(FastqEntry(data[0], data[1], data[3]))
					InsideEntry = False
			ifObj.close()
		
		else : raise Exception, "Error parsing file! file does not exist"

	def RemoveEntry(self, Address):
		try: self.remove(Address)
		except : print "This Address does not exist"

	def SaveInFile(self, Filename):
		ofObj = open(Filename,'w')
		for Entry in self:
			ofObj.write(Entry.PrintInFastq())
		ofObj.close()

	def GetNumOfEntries(self):
		return len(self)

	def SortEntriesUsingIndentifiers(self):
		IsDone = False
		while not IsDone:
			IsDone = True
			for i in range(len(self)-1):
				if  self[i].identifier > self[i+1].identifier:
					#print "Moving sequence up"
					TempStorage = self[i]
					self[i] = self[i+1]
					self[i+1] = TempStorage
					IsDone = False

					