#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from os.path import isfile as CheckfileExist
from BioClasses import GetFilePrefix
from random import choice

#-----------------Sequences class and related functions--------------
class MyFasta(object):
	def __init__(self, InputFile=False, FileType=False):
		# Variables
		self.SequencesList = []
		self.NbSeq = 0
		self.LenAln = 0
		self.IsEmpty = True
		# Parsing Input File
		if InputFile:
			if CheckfileExist(InputFile):
				if FileType:
					if FileType == 'Fasta' : self.ParseFasta(InputFile)
					elif FileType == 'BMGE' : self.ParseBMGE(InputFile)
				else : self.ParseFasta(InputFile)
			else : raise Exception, "Error parsing file! file does not exist"
		
	def ParseFasta(self, InputFile):
		ifObj = open(InputFile,'r')
		EntryName = ''
		EntrySequence = ''
		for line in ifObj:
			line = line.strip('#\r\n')
			if (line.startswith('>')):
				if (EntrySequence!=''):
					EntrySequence = EntrySequence.strip('*')
					self.AddEntry(EntryName, EntrySequence)
					if len(EntrySequence) > self.LenAln : self.LenAln = len(EntrySequence)
					EntrySequence=''
				line = line.strip('*')
				line = line.replace(':','')
				line = line.replace("'",'')
				line = line.replace(";",'')
				line = line.replace("(",'')
				line = line.replace(")",'')
				line = line.replace(",",'')
				EntryName = line.strip('>')
			else:
				if line.find(">")>0:
					ErrorData = line.split(">")
					EntrySequence += ErrorData[0]
					EntrySequence = EntrySequence.replace(' ','')
					EntrySequence = EntrySequence.strip('*')
					self.AddEntry(EntryName, EntrySequence)
					if len(EntrySequence) > self.LenAln : self.LenAln = len(EntrySequence)
					EntrySequence=''
					EntryName = ErrorData[1].strip('*')
					EntryName = EntryName.replace(':','')
					EntryName = EntryName.replace("'",'')
					EntryName = EntryName.replace(";",'')
					EntryName = EntryName.replace("(",'')
					EntryName = EntryName.replace(")",'')
					EntryName = EntryName.replace(",",'')
					EntryName = EntryName.strip('>')
				else:
					EntrySequence += line
					EntrySequence = EntrySequence.replace(' ','')
		self.AddEntry(EntryName, EntrySequence)
		if len(EntrySequence) > self.LenAln : self.LenAln = len(EntrySequence)
		ifObj.close()
		self.IsEmpty = False
		
	def ParseBMGE(self, InputFile):
		ifObj = open(InputFile,'r')
		EntryName = ''
		EntrySequence = ''
		FirstLine = True
		for line in ifObj:
			if FirstLine :
				self.LenAln = line.split(' ')[2]
				FirstLine = False
			else:
				line = line.strip('\r\n')
				line = line.strip('#')
				EntryName = line.split(' ')[0]
				EntrySequence = line.split(' ')[-1]
				self.AddEntry(EntryName, EntrySequence)
		ifObj.close()
		self.IsEmpty = False
		
	def AddEntry(self, definition, sequence):
		self.NbSeq +=1
		NewSeqObj = MyFastaEntry(definition, sequence, self.NbSeq)
		self.SequencesList.append(NewSeqObj)
	
	def AddSequence(self, FastaEntry):
		self.NbSeq +=1
		self.SequencesList.append(FastaEntry)
		
	def FullOutputInFasta(self):
		sortie = ''
		for seqs in self.SequencesList:
			line = ">"+seqs.definition+"\n"+seqs.sequence+'\n'
			sortie+=line
		return sortie
	
	def FullOutputInNexus(self):
		sortie = "#NEXUS\n[Version of Thursday 23 October 2003 at 10 hours 16]\nbegin data;\n"
		sortie+= "	dimensions ntax="+str(self.NbSeq)+" nchar="+str(self.LenAln)+";\n	format datatype=protein gap=- missing=?;\n	matrix\n"
		LongestDef = 0
		for seqs in self.SequencesList:
			if len(seqs.definition)>LongestDef : LongestDef=len(seqs.definition)
		for seqs in self.SequencesList:
			#print seqs.definition.ljust(LongestDef,"_")
			line = seqs.definition.ljust(LongestDef,"_")+" "+seqs.sequence+'\n'
			sortie+=line
		sortie+="	;\nend;"
		return sortie
	
	def FullOutputInFastaWithSeqNumbers(self):
		sortie = ''
		for seqs in self.SequencesList:
			line = ">"+seqs.seqNum+"\n"+seqs.sequence+'\n'
			sortie+=line
		return sortie
	
	def FullOutputInFastaWithIncluding(self, ExclusionList):
		sortie = ''
		for Tag in ExclusionList:
			found = False
			for Seq in self.SequencesList:
				if Seq.definition == str(Tag):
					found = True
					line = ">"+Seq.definition+"\n"+Seq.sequence+'\n'
					sortie+=line
			if not found : print Tag, "not found"
		return sortie
	
	def FullOutputInFastaWithExcluding(self, ExclusionList):
		sortie = ''
		for Tag in ExclusionList:
			NotFound = True
			for Seq in self.SequencesList:
				if Seq.definition != str(Tag):
					line = ">"+Seq.definition+"\n"+Seq.sequence+'\n'
					sortie+=line
				else:
					NotFound = False
			if not NotFound : print Tag, "found"
		return sortie
	
	def ExcludeSequences(self, ExclusionList):
		NewSequencesList = []
		newnbSeq = 0
		for Seq in self.SequencesList:
			Exclude = False
			for Tag in ExclusionList:
				if Seq.definition == str(Tag): Exclude = True
			if not Exclude:
				NewSequencesList.append(Seq)
				newnbSeq += 1
		self.SequencesList = NewSequencesList
		self.NbSeq = newnbSeq
		
		
	def IncludeSequences(self, InclusionList):
		NewSequencesList = []
		newnbSeq = 0
		for Seq in self.SequencesList:
			Include = False
			for Tag in InclusionList:
				if Seq.definition == str(Tag): Include = True
			if Include:
				NewSequencesList.append(Seq)
				newnbSeq += 1
		self.SequencesList = NewSequencesList
		self.NbSeq = newnbSeq
		
		
	def DelSeqBiggerThan(self, long):
		NewSequencesList = []
		newnbSeq = 0
		for seqs in self.SequencesList:
			if seqs.len < long:
				NewSequencesList.append(seqs)
				newnbSeq += 1
		self.SequencesList = NewSequencesList
		self.NbSeq = newnbSeq
	
	def DelSeqSmallerThan(self,long):
		NewSequencesList = []
		newnbSeq = 0
		for seqs in self.SequencesList:
			if seqs.len > long:
				NewSequencesList.append(seqs)
				newnbSeq += 1
		self.SequencesList = NewSequencesList
		self.NbSeq = newnbSeq
	
	def RemoveEmptySeq(self):
		NewSequencesList = []
		newnbSeq = 0
		for seqs in self.SequencesList:
			Remove = True
			for res in seqs.sequence:
				if res != '-' and res != '?' : Remove = False
				
			if not Remove:
				NewSequencesList.append(seqs)
				newnbSeq += 1
		
		self.SequencesList = NewSequencesList
		self.NbSeq = newnbSeq
	
	def RandomiseSeq(self):
		newSequences = []
		while len(self.SequencesList)!=0:
			RandElem = choice(self.SequencesList)
			newSequences.append(RandElem)
			self.SequencesList.remove(RandElem)
		self.SequencesList = newSequences
		
	def DelSeqUsingDefinition(self, DefSearch):
		NewSequencesList = []
		newnbSeq = 0
		for seqs in self.SequencesList:
			if seqs.definition != DefSearch:
				NewSequencesList.append(seqs)
				newnbSeq += 1
		self.SequencesList = NewSequencesList
		self.NbSeq = newnbSeq
		
#	def GetLongestSeqLen(self):
#		LongestLen = 0
#		for seqs in self.SequencesList:
#			if seqs.len > LongestLen : LongestLen = seqs.len
#		return LongestLen
		
	def GetLongestDefLen(self):
		LongestLen = 0
		for seqs in self.SequencesList:
			if len(seqs.definition) > LongestLen : LongestLen = len(seqs.definition)
		return LongestLen
		
	def GetLongestDef(self):
		LongestDef = ''
		LongestLen = 0
		for seqs in self.SequencesList:
			if len(seqs.definition) > LongestLen : LongestLen = len(seqs.definition)
			LongestDef = seqs.definition
		return LongestDef
		
	def FindSeqByDef(self, Query):
		result = False
		for seq in self.SequencesList:
			if seq.definition == Query:
				result = seq
				break
		return result
		
	def FindPatternInSequence(self, Pattern):
		result = False
		for seq in self.SequencesList:
			if seq.sequence.find(Pattern):
				result = seq
				break
		return result
		
		
	def SortSequenceByDefinition(self, reverse=False):
		IsDone = False
		
		if not reverse:
			while not IsDone:
				IsDone = True
				for i in range(len(self.SequencesList)-1):
					if self.SequencesList[i].definition > self.SequencesList[i+1].definition:
						#print "Moving sequence up"
						TempStorage = self.SequencesList[i]
						self.SequencesList[i] = self.SequencesList[i+1]
						self.SequencesList[i+1] = TempStorage
						IsDone = False
		else : 
			while not IsDone:
				IsDone = True
				for i in range(len(self.SequencesList)-1):
					if self.SequencesList[i].definition < self.SequencesList[i+1].definition:
						TempStorage = SequenceList[i]
						SequenceList[i] = SequenceList[i+1]
						SequenceList[i+1] = TempStorage
						IsDone = False
		
		
class MyFastaEntry(object):
	def __init__(self, definition, sequence, seqNum):
		self.definition = definition
		self.sequence = sequence
		self.len = len(sequence)
		self.seqNum = str(seqNum).rjust(5,'0')
	
	def GetInFasta(self):
		sortie = ">"+self.definition+"\n"+self.sequence+'\n'
		return sortie
		
	def CountOccurOf(self, querychar):
		total = 0
		for char in self.sequence:
			if char == querychar: total+=1
		return total
		
	def Reverse(self):
		self.sequence = self.sequence[::-1]
		
	def Complement(self):
		ComplementBase = {'A':'T','T':'A','C':'G','G':'C','N':'N','R':'Y','Y':'R','M':'K','K':'M','W':'W','S':'S','B':'V','D':'H','H':'D','V':'B','a':'t','t':'a','g':'c','c':'g','n':'n','r':'y','y':'r','m':'k','k':'m','w':'w','s':'s','b':'v','d':'h','h':'d','v':'b'}
		NewSeq = ''
		try:
			for char in self.sequence:
				NewSeq += ComplementBase[char]
			self.sequence = NewSeq
		except:
			print "Error while Making Complement"
			print self.sequence
			
	
