#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

# BioClasses Package
# Sequences part __init__ file

import string, random
from FastaBase import *
from FastQBase import *
from Aligner import *
from GapsRemover import *
from NCBIGeneParser import *
from gbkXtractor import *
from ExternalDataLib import *
from BioClasses import GetFilePrefix

def ReplaceSelenoCysteines(Filename):
	FastaObj = MyFasta(Filename)
	
	# Generating Seqnames File
	for seq in FastaObj.SequencesList: 
		if seq.sequence.find('U') > 0:
			print "Replacing U by X"
			seq.sequence = seq.sequence.replace('U','X')
	
	# Updating Fasta file
	ofObj = open(Filename,'w')
	ofObj.write(FastaObj.FullOutputInFasta())
	ofObj.close()

def GetNumSeq(Filename):
	FastaObj = MyFasta(Filename)
	return FastaObj.NbSeq

def id_generator(size=7, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

# def MakeSeqNamesList(Filename):
# 	FastaObj = MyFasta(Filename)
	
# 	# Generating Seqnames File
# 	outFile = GetFilePrefix(Filename) + ".seqnames"
# 	ofObj = open(outFile,'w')
# 	for seq in FastaObj.SequencesList: ofObj.write(seq.seqNum+"!"+seq.definition+"\n")
# 	ofObj.close()
	
# 	# Updating Fasta file
# 	ofObj = open(Filename,'w')
# 	ofObj.write(FastaObj.FullOutputInFastaWithSeqNumbers())
# 	ofObj.close()
	
# 	return outFile

def MakeSeqNamesList(Filename):
	IDs_List = []
	FastaObj = MyFasta(Filename)
	# Generating Seqnames File
	outFile = GetFilePrefix(Filename) + ".seqnames"
	ofObj = open(outFile,'w')
	for seq in FastaObj.SequencesList: 
		new_ID = id_generator()
		while new_ID in IDs_List: new_ID = id_generator()
		ofObj.write(new_ID+"!"+seq.definition+"\n")
		IDs_List.append(new_ID)
		seq.definition = new_ID
	ofObj.close()
	# Updating Fasta file
	ofObj = open(Filename,'w')
	ofObj.write(FastaObj.FullOutputInFasta())
	ofObj.close()
	
	return outFile
	
def RestoreSeqNamesInAlignment(FastaFile, SeqNamesFile=False):
	# Getting seqnames file if not provided
	if not SeqNamesFile :
		SeqNamesFile = GetFilePrefix(FastaFile) + ".seqnames"
		AskForSeqNamesFile = raw_input("\nDoes "+SeqNamesFile+" contain your sequences names and numbers ?? \n Press Enter if OK or type the correct name : ")
		if AskForSeqNamesFile != '': SeqNamesFile = AskForSeqNamesFile
	# Tryin to open SeqName file
	try : SeqNameFileObj = open(SeqNamesFile,'r')
	except : 
		print "Error while opening file, does this file exist ?"
		sys.exit()
	
	# Getting seqNums 2 seqNames data
	SeqNameFileDict = {}
	for line in SeqNameFileObj:
		sptuple = line.strip('\r\n').split('!')
		SeqNameFileDict[sptuple[0]]=sptuple[1]
	SeqNameFileObj.close()
	
	# Changing names in alignement file
	FastaObj = MyFasta(FastaFile)
	for seq in FastaObj.SequencesList : seq.definition = SeqNameFileDict[seq.definition]
	
	# Updating Fasta file
	ofObj = open(FastaFile,'w')
	ofObj.write(FastaObj.FullOutputInFasta())
	ofObj.close()
	
	
	
	
