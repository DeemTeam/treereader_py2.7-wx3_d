#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

# BioClasses Package
# Root __init__.py file
__version__ = "0.1.0"

import os
from sys import exit
from platform import system as OSname

#Define Path to Executables
class PathMaker(object):
	def __init__(self):
		self.blast = ''
		self.fasttree = ''
		self.Muscle = ''
		self.gblocks = ''
		self.BMGE = ''
		self.probcons = ''
		self.netblast = ''
		self.treefinder = ''
		self.mafft = ''
		self.mdust = ''
		self.cdhitest = ''
		self.TrimAl = ''
		if OSname() =='Windows':
			self.blast = "blastall"
			self.fasttree = "fasttree.exe"
			self.Muscle = "muscle.exe"
			self.gblocks = "gblocks.exe"
			self.BMGE = "c:\BI\BMGE.jar"
			self.probcons = "c:\BI\probcons.exe"
			self.netblast = "blastcl3"
			self.treefinder = 'tf'
			self.mafft = 'mafft'
			self.mdust = 'mdust'
			self.mrBayes = 'mb'
			self.cdhitest = 'cdhit-est'
		else :
			self.blast = "blastall"
			self.fasttree = "fasttree"
			self.muscle = "muscle"
			self.gblocks = "Gblocks"
			self.BMGE = "/usr/bin/BMGE.jar"
			self.probcons = "probcons"
			self.netblast = "blastcl3"
			self.treefinder = 'tf'
			self.mafft = 'mafft'
			self.mdust = 'mdust'
			self.mrBayes = 'mb'
			self.cdhitest = 'cdhit-est'
			self.TrimAl = 'trimal'


BioPATH = PathMaker()

# Funstions
def ChooseOneOrMultiFile(One_or_multi=False, files_extension=False):
	if not One_or_multi : One_or_multi = raw_input("Apply on one file or several files?? (O/M): ")
	if One_or_multi == "O" :
		file_name = raw_input("Enter the source file: ")
		files_to_treat = [file_name]
		return files_to_treat
	elif One_or_multi == "M" :
		if not files_extension : files_extension = raw_input("Enter the file extension to use : ")
		# retrieve a list folder_dir of all files in the current folder
		CurrentFolder = os.getcwd()
		folder_files = os.listdir(CurrentFolder)
		folder_files = sorted(folder_files)
		# retrieve only the files with the correct extension
		files_to_treat = []
		for filename in folder_files:
			if filename.endswith(files_extension):
				files_to_treat.append(filename)
		print '\n', len(files_to_treat), 'files to treat'
		return files_to_treat
	else :
		print "Choix impossible - End of script"
		exit()
		
def GetFilePrefix(filename):
	return '.'.join(filename.split('.')[:-1])
	
def GetWorkingDirectoryForTreeFinder():
	if OSname() =='Windows':
		CurrentFolder = os.getcwd()
		WorkingDir = CurrentFolder.replace("\\","\\\\")+"\\\\"
		return WorkingDir
	else :
		CurrentFolder = os.getcwd()
		WorkingDir = CurrentFolder+'/'
		return WorkingDir
		
def ChooseSubstitutionModel():
	Models ={
			'1' : 'WAG[,Empirical]',
			'2' : 'LG[,]:G[Optimum]:4',
			'3' : 'WAG[,]:G[Optimum]:4',
			'4' : 'HKY[Optimum,Empirical]:G[Optimum]:4',
			'5' :' GTR[Optimum,Empirical]:GI[Optimum]:4'
			}
	invit = """
	Choose a substitution model 
	1 : WAG[,Empirical]
	2 : LG[,]:G[Optimum]:4
	3 : WAG[,]:G[Optimum]:4
	4 : HKY[Optimum,Empirical]:G[Optimum]:4
	5 : GTR[Optimum,Empirical]:GI[Optimum]:4
	"""
	i=1
	Choice = raw_input(invit)
	if Models.has_key(Choice) : return Models[Choice]
	else : 
		print "Error, no such option"
		exit()
	
def ChooseAligner():
	Aligners = {
				'1' : 'muscle',
				'2' : 'probcons',
				'3' : 'mafft',
				'4' : False
				}
	invit = """
	Choose your aligner:
	1) muscle
	2) probcons
	3) mafft
	4) don't align
	""" 
	Choice = raw_input(invit)
	if Aligners.has_key(Choice) : return Aligners[Choice]
	else : 
		print "Error, no such option"
		exit()
		
def ChooseUngapper():
	UnGappers = {
				'1' : 'BMGE',
				'2' : 'gblocks',
				'3' : 'Homemade',
				'4' : 'TrimAl_gappyout',
				'5' : 'TrimAl_strict',
				'6' : 'TrimAl_automated1',
				'7' : False
				}
	invit = """
	Choose your ungapper:
	1) BMGE
	2) gblocks
	3) Home made
	4) TrimAl (gappyout)
	5) TrimAl (strict)
	6) TrimAl (automated1)
	7) don't un-gap
	""" 
	Choice = raw_input(invit)
	if UnGappers.has_key(Choice) : return UnGappers[Choice]
	else : 
		print "Error, no such option"
		exit()
	
def ChooseTreeSoftware():
	TreeSofts = {
				'1' : 'TreeFinder',
				'2' : 'FastTree',
				'3' : 'FastTree_Nt',
				'4' : 'mrBayes',
				'5' : 'mrBayes_MPI',
				'6' : False
				}
	invit = """
	Choose your tree software:
	1) TreeFinder
	2) FastTree
	3) FastTree Nucleotide
	4) mrBayes
	5) mrBayes MPI
	6) Don't reconstruct tree
	"""
	Choice = raw_input(invit)
	if TreeSofts.has_key(Choice) : return TreeSofts[Choice]
	else : 
		print "Error, no such option"
		exit()
		
def KeepReport():
	invit = """
	Keep TreeFinder report files? (Y/N)
	"""
	Choice = raw_input(invit)
	if Choice == "Y" or Choice == "y":
		return True
	elif Choice == "N" or Choice == "n":
		return False
	else : 
		print "Error, no such option"
		exit()

class Codon(object):
	def __init__(self, AAname, AAshort, AAletter, codon):
		self.AAname = AAname
		self.AAshort = AAshort
		self.AAletter = AAletter
		self.codon = codon

class GeneticTable(dict):
	def __init__(self, type=1, direction='Codon2letter'):
		# Variables
		if type == 1 :
			# List to use to build oblect
			RawList = [
			['Alanine','Ala','A',['GCT','GCC','GCA','GCG']],
			['Arginine','Arg','R',['CGT','CGC','CGA','CGG','AGA','AGG']],
			['Asparagine','Asn','N',['AAT','AAC']],
			['Aspartic acid','Asp','D',['GAT','GAC']],
			['Cysteine','Cys','C',['TGT','TGC']],
			['Glutamine','Gln','Q',['CAA','CAG']],
			['Glutamic acid','Glu','E',['GAA','GAG']],
			['Glycine','Gly','G',['GGT','GGC','GGA','GGG']],
			['Histidine','His','H',['CAT','CAC']],
			['Isoleucine','Ile','I',['ATT','ATC','ATA']],
			['Leucine','Leu','L',['TTA','TTG','CTT','CTC','CTA','CTG']],
			['Lysine','Lys','K',['AAA','AAG']],
			['Méthionine','Met','M',['ATG']],
			['Phenylalanine','Phe','F',['TTT','TTC']],
			['Proline','Pro','P',['CCT','CCC','CCA','CCG']],
			['Serine','Ser','S',['TCT','TCC','TCA','TCG','AGT','AGC']],
			['Threonine','Thr','T',['ACT','ACC','ACA','ACG']],
			['Tryptophane','Trp','W',['TGG']],
			['Tyrosine','Tyr','Y',['TAT','TAC']],
			['Valine','Val','V',['GTT','GTC','GTA','GTG']],
			['Ambre','Stp','*',['TAG']],
			['Ocre','Stp','*',['TAA']],
			['Opale','Stp','*',['TGA']],]

		if direction == 'Codon2letter':
			for AAdata in RawList:
				Current_AAname = AAdata[0]
				Current_AAshort = AAdata[1]
				Current_AAletter = AAdata[2]
				for AAcodon in AAdata[3]:
					self[AAcodon] = Codon(Current_AAname, Current_AAshort, Current_AAletter, AAcodon)
		
def NumLinesInFile(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
        