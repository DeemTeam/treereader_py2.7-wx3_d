#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import tempfile
from os import system as StartCommand
from BioClasses import BioPATH, GetFilePrefix
from BioClasses.Sequences import MyFasta
from os.path import exists as FileExists

def Skip(Filename): return Filename

def GetWorkingDirectoryForTreeFinder():
	if OSname() =='Windows':
		CurrentFolder = os.getcwd()
		WorkingDir = CurrentFolder.replace("\\","\\\\")+"\\\\"
		return WorkingDir
	else :
		CurrentFolder = os.getcwd()
		WorkingDir = CurrentFolder+'/'
		return WorkingDir
	
# ------------------------------ Aligners ------------------------------ 

def Launch_Muscle(Filename, Arguments):
	"""	-------------------- Launch_Muscle -----------------------
		Will launch an alignement with muscle on Filename
		Args:
			Filename (text): A multiple sequences file name
		Returns:
			AlignedFile (text): if success : the aligned file
			False (Bolean): if fails
		Raises:
			None
	"""
	# ------------ Launch_Muscle -----------------
	AlignedFile = GetFilePrefix(Filename) + ".aln"
	print "Running Muscle on "+Filename
	cmd = BioPATH.muscle + " -maxmb 1000 -in " + Filename + " -out " + AlignedFile
	try :
		StartCommand(cmd)
		return AlignedFile
	except :
		return False

def Launch_Mafft(Filename, Arguments):
	"""	-------------------- Launch_Mafft -----------------------
		Will launch an alignement with Mafft on Filename
		Args:
			Filename (text): A multiple sequences file name
		Returns:
			AlignedFile (text): if success : the aligned file
			False (Bolean): if fails
		Raises:
			None
	"""
	# ------------ Launch_Muscle -----------------
	AlignedFile = GetFilePrefix(Filename) + ".aln"
	print "Running mafft on "+Filename
	cmd = BioPATH.mafft + " --auto " + Filename + " > " + AlignedFile
	try :
		StartCommand(cmd)
		return AlignedFile
	except :
		return False

def Launch_Probcons(Filename):
	"""	-------------------- Launch_Probcons -----------------------
		Will launch an alignement with Probcons on Filename
		Args:
			Filename (text): A multiple sequences file name
		Returns:
			AlignedFile (text): if success : the aligned file
			False (Bolean): if fails
		Raises:
			None
	"""
	# Preparinf file names
	AlignedFile = GetFilePrefix(Filename) + ".aln"
	# Running ProbCons
	print "Running ProbCons on " + Filename
	cmd = BioPATH.probcons + " " + Filename + " > " + AlignedFile
	try :
		StartCommand(cmd)
		return AlignedFile
	except :
		return False

# ---------------------------- UnGappers ---------------------------- 

def Param_BMGE(Parameters, GapTolerance=0.2):
	"""	-------------------- Param_BMGE -----------------------
		Will setup Parameters for BMGE
		Args:
			Parameters (dict): A key:value variable from the caller script
		Returns:
			Parameters (dict): The input variable with additional parameters
		Raises:
			None
	"""
	LocalParameters =	{
							'EntropyScoreCutoff' : 0.5,
							'GapRateCutoff' : 0.2,
							'SequenceType' : 'AA'
						}
	print "BMGE Default parameters are : "
	for Name, Value in LocalParameters.iteritems():
		print Name, " : ", Value

	print 

	UngappedFile = GetFilePrefix(Filename) + '.gap'
	TemporaryFile = str(randint(10000, 99999)) + ".txt"
	# Launching BMGE
	cmd = "java -jar "+ BioPATH.BMGE + " -i " + Filename + " -o " + TemporaryFile +" -t AA -g "+str(GapTolerance)
	print "running BMGE on " + Filename
	StartCommand(cmd)
	# Retrieving data from tempo file
	FastaObj = MyFasta(TemporaryFile,'BMGE')
	ofObj = open(UngappedFile, 'w')
	ofObj.write(FastaObj.FullOutputInFasta())
	ofObj.close()
	# Removing tempo file
	RemoveFile(TemporaryFile)
	# Returning File name 
	return UngappedFile, FastaObj.LenAln

def BMGE(Filename, GapTolerance=0.2):
	# Preparing filnames
	UngappedFile = GetFilePrefix(Filename) + '.gap'
	TemporaryFile = str(randint(10000, 99999)) + ".txt"
	# Launching BMGE
	cmd = "java -jar "+ BioPATH.BMGE + " -i " + Filename + " -o " + TemporaryFile +" -t AA -g "+str(GapTolerance)
	print "running BMGE on " + Filename
	StartCommand(cmd)
	# Retrieving data from tempo file
	FastaObj = MyFasta(TemporaryFile,'BMGE')
	ofObj = open(UngappedFile, 'w')
	ofObj.write(FastaObj.FullOutputInFasta())
	ofObj.close()
	# Removing tempo file
	RemoveFile(TemporaryFile)
	# Returning File name 
	return UngappedFile, FastaObj.LenAln


	
def ChooseTreeSoftware():
	TreeSofts = {
				'1' : 'TreeFinder',
				'2' : 'FastTree',
				'3' : 'FastTree_Nt',
				'4' : 'mrBayes',
				'5' : 'mrBayes_MPI',
				'6' : False
				}
	invit = """
	Choose your tree software:
	1) TreeFinder
	2) FastTree
	3) FastTree Nucleotide
	4) mrBayes
	5) mrBayes MPI
	6) Don't reconstruct tree
	"""
	Choice = raw_input(invit)
	if TreeSofts.has_key(Choice) : return TreeSofts[Choice]
	else : 
		print "Error, no such option"
		exit()
		
def KeepReport():
	invit = """
	Keep TreeFinder report files? (Y/N)
	"""
	Choice = raw_input(invit)
	if Choice == "Y" or Choice == "y":
		return True
	elif Choice == "N" or Choice == "n":
		return False
	else : 
		print "Error, no such option"
		exit()

class Codon(object):
	def __init__(self, AAname, AAshort, AAletter, codon):
		self.AAname = AAname
		self.AAshort = AAshort
		self.AAletter = AAletter
		self.codon = codon

class GeneticTable(dict):
	def __init__(self, type=1, direction='Codon2letter'):
		# Variables
		if type == 1 :
			# List to use to build oblect
			RawList = [
			['Alanine','Ala','A',['GCT','GCC','GCA','GCG']],
			['Arginine','Arg','R',['CGT','CGC','CGA','CGG','AGA','AGG']],
			['Asparagine','Asn','N',['AAT','AAC']],
			['Aspartic acid','Asp','D',['GAT','GAC']],
			['Cysteine','Cys','C',['TGT','TGC']],
			['Glutamine','Gln','Q',['CAA','CAG']],
			['Glutamic acid','Glu','E',['GAA','GAG']],
			['Glycine','Gly','G',['GGT','GGC','GGA','GGG']],
			['Histidine','His','H',['CAT','CAC']],
			['Isoleucine','Ile','I',['ATT','ATC','ATA']],
			['Leucine','Leu','L',['TTA','TTG','CTT','CTC','CTA','CTG']],
			['Lysine','Lys','K',['AAA','AAG']],
			['Méthionine','Met','M',['ATG']],
			['Phenylalanine','Phe','F',['TTT','TTC']],
			['Proline','Pro','P',['CCT','CCC','CCA','CCG']],
			['Serine','Ser','S',['TCT','TCC','TCA','TCG','AGT','AGC']],
			['Threonine','Thr','T',['ACT','ACC','ACA','ACG']],
			['Tryptophane','Trp','W',['TGG']],
			['Tyrosine','Tyr','Y',['TAT','TAC']],
			['Valine','Val','V',['GTT','GTC','GTA','GTG']],
			['Ambre','Stp','*',['TAG']],
			['Ocre','Stp','*',['TAA']],
			['Opale','Stp','*',['TGA']],]

		if direction == 'Codon2letter':
			for AAdata in RawList:
				Current_AAname = AAdata[0]
				Current_AAshort = AAdata[1]
				Current_AAletter = AAdata[2]
				for AAcodon in AAdata[3]:
					self[AAcodon] = Codon(Current_AAname, Current_AAshort, Current_AAletter, AAcodon)
		
def NumLinesInFile(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
        


# if TreeWith == 'TreeFinder' : 
# 	SubModel = BioClasses.ChooseSubstitutionModel()
# 	KeepTFfiles = BioClasses.KeepReport()

# elif TreeWith == 'mrBayes_MPI' : 
# 	nbProc = raw_input("Number of prcessors ? ")

# else : 
# 	SubModel = False
# 	KeepTFfiles = False
# 	nbProc = False

# MoveFilesWhenDone = False
# MoveFilesQuestion = raw_input("Move files if success/fail?? (O/N)")
# if MoveFilesQuestion=='O' or MoveFilesQuestion=='o':MoveFilesWhenDone = True

# # Create dirs
# if MoveFilesWhenDone:
# 	if not FileExists("./done"): mkdir("./done")
# 	if not FileExists("./crash"): mkdir("./crash")