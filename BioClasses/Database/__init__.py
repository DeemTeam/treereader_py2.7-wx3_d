#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

# BioClasses Package
# Database __init__.py file
__version__ = "0.1.0"

from pg import connect
from sys import exit
from Manage import *
from BioClasses.Blasts import DatabaseFormatedBlastHit

def ConnectDB(Arguments=[], user=False, pgServerIP=False):
	db = False
	if not user: 
		user=['pguser','Genomes']
		
	if not pgServerIP:
		if 'local' in Arguments and 'chaos' in Arguments and 'karenia' in Arguments and 'nodb' in Arguments:
			print "Script cannot be local AND remote"
			exit()
	
		if 'local' not in Arguments and 'chaos' not in Arguments and 'karenia' not in Arguments and 'nodb' not in Arguments:
			print "Argument missing : local, karenia, chaos or no connection for DB"
			exit()
	
		if 'nodb' not in Arguments:
			if 'local' in Arguments:
				pgServerIP='localhost'
			if 'karenia' in Arguments:
				pgServerIP='129.175.107.103'
			if 'chaos' in Arguments:
				pgServerIP='129.175.107.102'
			db = connect('genomes',pgServerIP,5432,None,None,user[0],user[1])
	
	else:
		#db = connect('genomes',pgServerIP,5432,None,None,user[0],user[1])
		db = connect('microdb',pgServerIP,5432,None,None,user[0],user[1])
	
	return db, pgServerIP

def DisconnectDB(db):
	if db: db.close()
	
def GetDbsize(): return 1400290747
	
class DBBlastHit(object):
	def __init__(self):
		self.query = ''
		self.hit = ''
		self.eValue = ''
		self.spname = ''
		self.aa = ''
		self.spid = ''
		self.iden = ''
		self.align_len = ''


def GetBlastFromDb(db, localid, limEval=1e-5):
	db_len = GetDbsize()
	allblastid = []
	allblastdef = []
	OutPut = []
	get_blastdef = db.query("SELECT blastid,seq2,bitscore,seqlen,spname,aa,spid,iden,align_len FROM blast LEFT JOIN aaseqs ON aaseqs.localid=blast.seq2 LEFT JOIN sp ON sp.spid=aaseqs.idsp WHERE blast.seq1="+str(localid))
	for blastdef in get_blastdef.dictresult():
		if int(blastdef['blastid']) not in allblastid:
			if blastdef['bitscore'] > 1000:
				eValue = 0
			else :
				eValue = (blastdef['seqlen']*db_len)/(pow(2, blastdef['bitscore']))
			newblastdef = [eValue,blastdef['seq2'],blastdef['spname'],blastdef['aa'],blastdef['spid'],blastdef['iden'],blastdef['align_len']]
			# Eliminate seq with evalue too high or too long
			if eValue < limEval and blastdef['seqlen'] < 5000:
				allblastdef.append(newblastdef)
				allblastid.append(int(blastdef['blastid']))
	get_blastdef = db.query("SELECT blastid,seq1,bitscore,seqlen,spname,aa,spid,iden,align_len FROM blast LEFT JOIN aaseqs ON aaseqs.localid=blast.seq1 LEFT JOIN sp ON sp.spid=aaseqs.idsp WHERE blast.seq2="+str(localid))
	for blastdef in get_blastdef.dictresult():
		if int(blastdef['blastid']) not in allblastid:
			if blastdef['bitscore'] > 1000:
				eValue = 0
			else :
				eValue = (blastdef['seqlen']*db_len)/(pow(2, blastdef['bitscore']))
			newblastdef = [eValue,blastdef['seq1'],blastdef['spname'],blastdef['aa'],blastdef['spid'],blastdef['iden'],blastdef['align_len']]
			# Eliminate seq with evalue too high or too long
			if eValue < limEval and blastdef['seqlen'] < 5000:
				allblastdef.append(newblastdef)
				allblastid.append(int(blastdef['blastid']))
	allblastdef.sort()
	for blastdef in allblastdef:
		newDBBlastHit = DBBlastHit()
		newDBBlastHit.query = localid
		newDBBlastHit.hit = blastdef[1] #blastdef['seq1']
		newDBBlastHit.eValue = blastdef[0] # eValue
		newDBBlastHit.spname = blastdef[2] # blastdef['spname']
		newDBBlastHit.aa = blastdef[3] # blastdef['aa']
		newDBBlastHit.spid = blastdef[4] # blastdef['spid']
		newDBBlastHit.iden = blastdef[5] # blastdef['iden']
		newDBBlastHit.align_len = blastdef[6] # blastdef['align_len']
		OutPut.append(newDBBlastHit)
	#return allblastdef
	return OutPut

def GetSeqFromGroups(db, GroupID):
	query = "SELECT spid, spname, aaseqs.localid, aa FROM aaseqs LEFT JOIN sp ON aaseqs.idsp=sp.spid LEFT JOIN seq2usergroups ON seq2usergroups.seqid=aaseqs.localid WHERE seq2usergroups.groupid="+str(GroupID)
	#print query
	get_GroupSeq = db.query(query)
	return get_GroupSeq.dictresult()
	
