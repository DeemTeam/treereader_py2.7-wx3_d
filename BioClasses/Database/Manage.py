#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import os, zipfile
from sys import exit, exc_info
from platform import system as OSname
from BioClasses import ChooseOneOrMultiFile, GetFilePrefix
from BioClasses.Sequences import MyFasta
from BioClasses.Blasts import GetDataBaseFormatedBlastHit
import BioClasses.Database

#________________________ Database management _______________________
# Add a species in the DB with its group affiliation
def AddaSpecies(db):
	New_SP_name = raw_input("\nType the full name of the new species :")
	New_SP_name_div = New_SP_name.split(' ',2)
	found_related_names = []
	# Checking if the SP is not already present in the DB
	for sub_name in New_SP_name_div:
		query = db.query("SELECT sp.spid,sp.spname FROM sp WHERE sp.spname LIKE '%"+sub_name+"%' ORDER by sp.spid")
		for related_sp in query.dictresult():
			found_related_names.append(str(related_sp['spid'])+" - "+str(related_sp['spname']))
	
	print "These related species where found in the DB\n"
	for name in found_related_names:
		print name
	yes_or_no = raw_input("\nPlease confirm that you want to add the new species (y or n)")
	if yes_or_no == "Y" or yes_or_no == "y":
		# Choosing the group affiliation for the new SP
		query = db.query("SELECT * FROM gp ORDER BY gp.gpid")
		print "Group ID	Group name"
		for group in query.dictresult():
			print str(group['gpid'])+"	"+str(group['gpname'])
		INGPs = raw_input("\nPlease type the IDs of the groups you want to use for this new SP separated with comas :")
		GPs=INGPs.split(',')
		groups = ''
		for GP in GPs:
			GP = int(GP)
			query = db.query("SELECT * FROM gp WHERE gpid="+str(GP))
			groups += query.dictresult()[0]['gpname']+", "
		print groups
		yes_or_no2 = raw_input("Please confirm that you want to use these groups (y or n)")
		if yes_or_no2 == "Y" or yes_or_no == "y":
			insert_sp = db.query("INSERT INTO sp VALUES (DEFAULT, '"+New_SP_name+"', DEFAULT) RETURNING spid")
			inserted_id = insert_sp.dictresult()[0]['spid']
			for GP in GPs:
				GP = int(GP)
				db.query("INSERT INTO sp2gp VALUES (DEFAULT, '"+str(inserted_id)+"' ,'"+str(GP)+"')")
		else :
			print "Leaving script"
			exit()
	else :
		print "Leaving script"
		exit()
# Add sequences for a specific species
def AddSeqOneSp(db):
	New_SP_name = raw_input("\nType the full name of the species :")
	query = db.query("SELECT sp.spid,sp.spname FROM sp WHERE sp.spname LIKE'%"+New_SP_name+"%' ORDER by sp.spid")
	found_related_names = []
	for related_sp in query.dictresult():
		found_related_names.append(str(related_sp['spid'])+" - "+str(related_sp['spname']))
	print "These species where found in the DB\n"
	for name in found_related_names:
		print name
	spid_to_use = raw_input("\nPlease type the spID that you want to use: ")
	inFile = raw_input("\nPlease type the filename containing the sequences: ")
	try:
		print "Checking sequences"
		fastaObj = MyFasta(inFile)
		for seq in fastaObj.SequencesList:
			seq_split = seq.definition.split('|')
			if len(seq_split) !=4 :
				print "Error while processing the file, too much values"
				print seq.definition
				exit()
		whattodo = raw_input("\nData OK, Press enter send to DB, X to exit")
		if whattodo=="X" or whattodo=="x":
			print "Exiting program"
			exit()
		print "Writing sequences"
		for seq in fastaObj.SequencesList:
			seq_split = seq.definition.split('|')
			Anno = seq_split[3][:250]
			query = "INSERT INTO aaseqs VALUES(DEFAULT,'"+str(spid_to_use)+"','"+str(seq_split[1])+"','"+str(seq_split[2])+"','"+str(seq.sequence)+"','"+str(seq.len)+"','"+str(Anno)+"','"+""+"','"+""+"','"+"0"+"','"+""+"','"+"0"+"')"
			db.query(query)
	except:
		print "Error while processing the file", exc_info()
# Add sequences for multiple species
def AddSeqMultiSp(db):
	inFile = raw_input("\nPlease type the filename containing the sequences: ")
	logFile = open('ManageDbLog.txt','w')
	fastaObj = MyFasta(inFile)
	print "Checking sequences"
	linenum = 1
	for seq in fastaObj.SequencesList:
		SplitDef = seq.definition.split('|')
		error = ''
		try :
			spname = SplitDef[0]
			query = db.query("SELECT sp.spid FROM sp WHERE sp.spname='"+spname+"' ORDER by sp.spid")
			spid = query.getresult()[0][0]
		except :
			error = 'Bad species on line '+str(linenum)
		try :
			gi = SplitDef[1]
		except :
			gi = ''
			print "nogi"
		try : 
			ref = SplitDef[2]
		except :
			ref = ''
			print "noref"
		try :
			anno = SplitDef[3]
		except :
			anno =''
			print "no anno"
		if error =='':
			logFile.write("INSERT INTO aaseqs VALUES(DEFAULT,'"+str(spid)+"','"+str(gi)+"','"+str(ref)+"','"+str(seq.sequence)+"','"+str(seq.len)+"','"+str(anno)+"',DEFAULT,DEFAULT,'0',DEFAULT,'0')\n")
		else :
			print error
			exit()
		linenum+=2
	logFile.close
	whattodo = raw_input("\nData OK, Press enter send to DB, X to exit")
	if whattodo=="X" or whattodo=="x":
		print "Exiting program"
		exit()
	for seq in fastaObj.SequencesList:
		SplitDef = seq.definition.split('|')
		spname = SplitDef[0]
		query = db.query("SELECT sp.spid FROM sp WHERE sp.spname='"+spname+"' ORDER by sp.spid")
		spid = query.getresult()[0][0]
		try :
			gi = SplitDef[1]
		except :
			gi = ''
		try : 
			ref = SplitDef[2]
		except :
			ref = ''
		try :
			anno = SplitDef[3]
		except :
			anno =''
		db.query("INSERT INTO aaseqs VALUES(DEFAULT,'"+str(spid)+"','"+str(gi)+"','"+str(ref)+"','"+str(seq.sequence)+"','"+str(seq.len)+"','"+str(anno)+"',DEFAULT,DEFAULT,'0',DEFAULT,'0')")

# Add sequences for multiple species using multiple files
def AddSeqMultiSpMultiFile(db):
	print "Cheking Files"
	
	Error = False

	for Filename in ChooseOneOrMultiFile() :
		fastaObj = MyFasta(Filename)
		spname = GetFilePrefix(Filename)
		get_spid = db.query("SELECT sp.spid FROM sp WHERE sp.spname='"+spname+"' ORDER by sp.spid")
		if get_spid.ntuples()==1:
			spid = get_spid.getresult()[0][0]
			i = 1
			for seq in fastaObj.SequencesList:
				gi = ''
				ref = ''
				anno = ''
				SplitDef = seq.definition.split('|')
				try :
					gi = SplitDef[1]
					ref = SplitDef[2]
					anno = SplitDef[3][:250]
				except :
					Error = True
					print "Data Error on sequence "+str(i)

				i+=1
		else :
			print "Can't find species id for "+str(Filename)
			Error = True

	if not Error:
		whattodo = raw_input("\nData OK, Press enter send to DB, X to exit")
		if whattodo=="X" or whattodo=="x":
			print "Exiting program"
			exit()

		for Filename in ChooseOneOrMultiFile() :
			fastaObj = MyFasta(Filename)
			spname = GetFilePrefix(Filename)
			get_spid = db.query("SELECT sp.spid FROM sp WHERE sp.spname='"+spname+"' ORDER by sp.spid")
			if get_spid.ntuples()==1:
				spid = get_spid.getresult()[0][0]
				i = 1
				for seq in fastaObj.SequencesList:
					gi = ''
					ref = ''
					anno = ''
					
					SplitDef = seq.definition.split('|')
					
					gi = SplitDef[1]
					ref = SplitDef[2]
					anno = SplitDef[3][:250]

					db.query("INSERT INTO aaseqs VALUES(DEFAULT,'"+str(spid)+"','"+str(gi)+"','"+str(ref)+"','"+str(seq.sequence)+"','"+str(seq.len)+"','"+str(anno)+"',DEFAULT,DEFAULT,'0',DEFAULT,'0')")

					i+=1
		
# Send blastP results to blast db
def RecordBlastP(db):
	# Opening file
	inFile = raw_input("\nFile containing blast results:")
	ifObj = open(inFile,'r')
	# Checking data
	print "-----------Checking data------------"
	i=0
	for line in ifObj:
		if not line.startswith("-EOB-") and not line.startswith("Empty"):
			blastdata = GetDataBaseFormatedBlastHit(line)
			if not blastdata:
				print "Error on line "+str(i)
				exit()
			else:
				try :
					blastdata.Hit_def = int(blastdata.Hit_def.split('@')[1])
				except :
					print "Error for hit def on line "+str(i)+" - "+ str(exc_info()[0])+"\n"+str(blastdata)
					exit()
		i+=1
		if i%10000 == 0:
			print str(i)
	# Closing file
	ifObj.close()
	afaire = i
	# Asking for sending to DB
	whattodo = raw_input("\nData OK, Press enter send to DB, X to exit")
	if whattodo=="X" or whattodo=="x":
		print "Exiting program"
		exit()
	else:
		# Sending data to db
		print "-----------Sending data------------"
		ifObj = open(inFile,'r')
		i=0
		for line in ifObj:
			if not line.startswith("-EOB-"):
				if not line.startswith("Empty"):
					blastdata = GetDataBaseFormatedBlastHit(line)
					blastdata.query = int(blastdata.query.split('@')[1])
					blastdata.Hit_def = int(blastdata.Hit_def.split('@')[1])
					get_done = db.query("SELECT seq1,seq2 FROM blast WHERE (seq1='"+str(blastdata.query)+"' AND seq2='"+str(blastdata.Hit_def)+"') OR (seq1='"+str(blastdata.Hit_def)+"' AND seq2='"+str(blastdata.query)+"')")
					if get_done.ntuples() == 0:
						query = "INSERT INTO blast VALUES(DEFAULT,'"+str(blastdata.query)+"','"+str(blastdata.Hit_def)+"','"+str(blastdata.Hsp_score)+"','"+str(blastdata.Hsp_bit_score)+"','"+str(blastdata.Hsp_evalue)+"','"+str(blastdata.Hsp_identity)+"','"+str(blastdata.Hsp_positive)+"','"+str(blastdata.Hsp_query_from)+"','"+str(blastdata.Hsp_query_to)+"','"+str(blastdata.Hsp_hit_from)+"','"+str(blastdata.Hsp_hit_to)+"','"+str(blastdata.Hsp_align_len)+"')"
						#print query
						db.query(query)
			i+=1
			if i%10000 == 0:
				print str(i)+" done on "+str(afaire) 
			
	ifObj.close()
# Calculate the size of the DB
def CalculateDBSize(db):
	nbseq_tot=0
	nbAA_tot=0
	query = db.query("SELECT sp.spid,sp.spname FROM sp ORDER by sp.spid")
	for row in query.getresult():
		spid=row[0]
		esp=row[1]
		print "Espece en cours :"+esp
		query2 = db.query("SELECT length(aaseqs.aa) as aaLong FROM aaseqs WHERE aaseqs.idsp="+str(spid))
		nbseq_tot += query2.ntuples()
		for long in query2.getresult():
			nbAA_tot+=long[0]
	print "nb total de seq : "+str(nbseq_tot)+"nb total AA : "+str(nbAA_tot)
#__________________________ Data extraction _________________________
# Extract the full DB and format the DB for blast
def ExtractAndFormatDB(db):
	sortie = raw_input("\nType the output filname: ")
	ofObj = open(sortie,'w')
	#Get all species id
	query = db.query("SELECT sp.spid,sp.spname FROM sp ORDER by sp.spid")
	for row in query.getresult():
		spid=row[0]
		esp=row[1]
		print "Now extracting : "+esp
		#Getting all seqs for the current species
		req = "SELECT aaseqs.localid,aaseqs.aa FROM aaseqs WHERE aaseqs.idsp="+str(spid)
		query2 = db.query(req)
		i=1
		afaire=query2.ntuples()
		for cols in query2.getresult():
			ofObj.write('>'+str(esp)+'@'+str(cols[0])+'\n'+str(cols[1])+'\n')
			i=i+1
			if i%1000 == 0:
				print str(i)+' done over '+str(afaire)
	ofObj.close()
	yes_or_no = raw_input("Do you want to format the DB for blast (y or n) ?")
	if yes_or_no == "Y" or yes_or_no == "y":
		print "Formating db for blast"
		cmd = "formatdb -i "+sortie+" -p T"
		os.system(cmd)
	else :
		print "Leaving script"
		exit()
# Extract some specific groups and format the DB for blast
def ExtractGroupsAndFormatDB(db):
	query = db.query("SELECT * FROM gp ORDER BY gp.gpid")
	print "Numero	Nom"
	for row in query.getresult():
		gpid=row[0]
		gpname=row[1]
		print str(gpid)+"	"+str(gpname)

	INGPs = raw_input("\nPlease enter the IDs of the groups to extract, separated with comas : ")
	sortie = raw_input("\nOutFile name ? ")
	ofObj = open(sortie,'w')
	SpCondition = raw_input("\nAdd SQL condition for species (Enter for no)? ")
	SeqCondition = raw_input("\nAdd SQL condition for sequences (Enter for no)? ")
	
	GPs=INGPs.split(',')
	for GP in GPs:
		GP = int(GP)
		query = db.query("SELECT idsp,spname FROM sp2gp LEFT JOIN sp on sp.spid=sp2gp.idsp WHERE idgp='"+str(GP)+"' "+SpCondition)
		for row in query.getresult():
			spid=row[0]
			#query2 = db.query("SELECT spname FROM sp WHERE spid='"+str(spid)+"'")
			spname = row[1]
			req = "SELECT aaseqs.localid,aaseqs.aa FROM aaseqs WHERE aaseqs.idsp='"+str(spid)+"' "+SeqCondition
			query2 = db.query(req)
			i=0
			afaire=query2.ntuples()
			for cols in query2.getresult():
				ofObj.write('>'+str(spname)+'@'+str(cols[0])+'\n'+str(cols[1])+'\n')
				i=i+1
				if i%1000 == 0:
					print str(i)+' done over '+str(afaire)
	ofObj.close()
	yes_or_no = raw_input("Do you want to format the DB for blast (y or n) ?")
	if yes_or_no == "Y" or yes_or_no == "y":
		print "Formating db for blast"
		cmd = "formatdb -i "+sortie+" -p T"
		os.system(cmd)
	else :
		print "Leaving script"
		exit()
# Extract some specific species and format the DB for blast
def ExctractSpeciesAndFormatDB(db):
	INSPs = raw_input("\nPlease enter the species names to extract, separated with comas : ")
	sortie = raw_input("\nOutFile name ? ")
	ofObj = open(sortie,'w')
	condition = raw_input("\nAdd a condition in SQL (Enter for no)? ")
	
	SPs=INSPs.split(',')
	for SP in SPs:
		query = db.query("SELECT sp.spid,sp.spname FROM sp WHERE sp.spname LIKE '%"+SP+"%'")
		for row in query.getresult():
			spid=row[0]
			esp=row[1]
			print "Espece en cours :"+esp
			req = "SELECT aaseqs.localid,aaseqs.aa FROM aaseqs WHERE aaseqs.idsp="+str(spid)+" "+condition
			query2 = db.query(req)
			i=0
			afaire=query2.ntuples()
			for cols in query2.getresult():
				ofObj.write('>'+str(esp)+'@'+str(cols[0])+'\n'+str(cols[1])+'\n')
				i=i+1
				if i%1000 == 0:
					print str(i)+' done over '+str(afaire)
	ofObj.close()
	yes_or_no = raw_input("Do you want to format the DB for blast (y or n) ?")
	if yes_or_no == "Y" or yes_or_no == "y":
		print "Formating db for blast"
		cmd = "formatdb -i "+sortie+" -p T"
		os.system(cmd)
	else :
		print "Leaving script"
		exit()
# Get A fasta from a local ID list or a sp@localid list
def MakeFastaFromIDList(db, localidList=False, FastaName=False):
	if not localidList:
		for filename in ChooseOneOrMultiFile() :
			# Opening file
			ifObj = open(filename,'r')
			outFile = filename + '.fas'
			ofObj = open(outFile,'w')
			# Checking data
			for line in ifObj:
				line = line.strip('\r\n')
				try : seq_id = line.split('@')[1].split('|')[0]
				except : seq_id = line
				query = db.query("SELECT aaseqs.localid,aaseqs.aa,sp.spname FROM aaseqs LEFT JOIN sp ON sp.spid=aaseqs.idsp WHERE aaseqs.localid="+str(seq_id))
				data = query.dictresult()[0]
				ofObj.write(">"+str(data['spname'])+"@"+str(data['localid'])+"\n"+str(data['aa'])+"\n")
			ifObj.close()
			ofObj.close()
	else:
		ofObj = open(FastaName,'w')
		query = "SELECT aaseqs.localid,aaseqs.aa,sp.spname FROM aaseqs "
		query+= "LEFT JOIN sp ON sp.spid=aaseqs.idsp WHERE aaseqs.localid IN ("+','.join(localidList)+");"

		GetSequences = db.query(query)
		for Sequences in GetSequences.dictresult():
			ofObj.write(">"+str(Sequences['spname'])+"@"+str(Sequences['localid'])+"\n"+str(Sequences['aa'])+"\n")

		ofObj.close()

# List Species From The DB
def ListAllSpecies(db):
	sortie = raw_input("\nOutFile name ? ")
	ofObj = open(sortie,'w')
	
	query = db.query("select spid,spname,gpid,gpname from sp left join sp2gp on sp.spid=sp2gp.idsp left join gp on gp.gpid=sp2gp.idgp where gp.rang=1 group by sp.spid,sp.spname,gp.gpid,gp.gpname")
	for row in query.dictresult():
		query2 = db.query("select count(localid) from aaseqs where idsp="+str(row['spid']))
		nbProt = query2.getresult()[0][0]
		ofObj.write(str(row['spid'])+";"+str(row['spname'])+";"+str(row['gpid'])+";"+str(row['gpname'])+";"+str(nbProt)+"\n")
	ofObj.close()
# Generate fasta from a local blast result list
def MakeFastaFromLocalBlastList(db):
	eValueLim = float(raw_input("eValue limit:"))
	NumSeqLim = int(raw_input("Max number of sequences to get:"))
	for filename in ChooseOneOrMultiFile() :
		print filename
		# Opening file
		ifObj = open(filename,'r')
		for line in ifObj:
			LocalID = line.strip('\r\n')
			ofObj = open(LocalID+'.fas','w')
			BlastData = BioClasses.Database.GetBlastFromDb(db, LocalID, limEval=eValueLim)
			for Blast in BlastData[:NumSeqLim]:
				ofObj.write(">"+str(Blast[2])+"@"+str(Blast[1])+"\n"+str(Blast[3])+"\n")
			ofObj.close()
		ifObj.close()
		
#_____________________ Whole Database Management ____________________
# Link ID list to user group
def LinkIDListToUserGroup(db):
	print "takes local IDs sep with comas and send to usergroup"
	usergpnum = raw_input("\nGroup number	? ")
	list_locID = raw_input("\nlocal IDs sep with comas	? ")
	locID = list_locID.split(',')
	for ID in locID:
		ID = ID.strip(' ')
		ID = ID.strip('\r\n')
		query = db.query("INSERT INTO seq2usergroups VALUES(DEFAULT,"+str(ID)+","+str(usergpnum)+")")
		print ID
#_____________________ Whole Database Management ____________________
# Save Whole DB
def SaveFulldb(db, pgServerIP):
	outFile = raw_input("\nEnter Output Filname : ")
	zipObj = zipfile.ZipFile(outFile, 'w', zipfile.ZIP_STORED, True)
	if OSname() =='Windows':
		pg_dump_path = "C:\\BI\\pg_dump.exe"
	else :
		pg_dump_path = "pg_dump"
	TfObj = open('SavedTables.txt','w')
	get_AllTables = db.query("SELECT tablename FROM pg_tables where tablename not like 'pg_%' AND tablename not like 'sql_%'")
	#get_AllTables = db.query("SELECT tablename FROM pg_tables where tablename='gp'")
	num_AllTables = get_AllTables.ntuples()
	Done = 1
	for AllTables in get_AllTables.dictresult():
		Go_on = True
		print "-------------- Saving Table "+AllTables['tablename']+" ("+str(Done)+"/"+str(num_AllTables)+")  -----------------" 
		try : 
			Commande = pg_dump_path+' --host '+pgServerIP+' --port 5432 --username pguser --format plain --schema-only --verbose --file "'+AllTables['tablename']+'.structure.sql" --table \'public.'+AllTables['tablename']+'\' "genomes"'
			print Commande
			#/usr/bin/pg_dump --host 129.175.107.103 --port 5432 --username pguser --format plain --schema-only --verbose --file "/media/StoreFile/Projects Current/EvolDeep/DB_modif/gp.sql" --table 'public.gp' "Genomes"
			
			os.system(Commande)
			Commande = pg_dump_path+' --host '+pgServerIP+' --port 5432 --username pguser --format custom --verbose --file "'+AllTables['tablename']+'.backup" --table \'public.'+AllTables['tablename']+'\' "genomes"'
			print Commande
			os.system(Commande)
			TfObj.write(AllTables['tablename']+"\n")
		except :
			print "Error on table "+AllTables['tablename']
			Go_on = False
		if Go_on:
			print "-------------- Compressing files -----------------" 
			zipObj.write(AllTables['tablename']+".structure.sql")
			zipObj.write(AllTables['tablename']+'.backup')
			print "-------------- Removing temp files -----------------" 
			os.remove(AllTables['tablename']+".structure.sql")
			os.remove(AllTables['tablename']+'.backup')
		Done+=1
	TfObj.close()
	zipObj.write('SavedTables.txt')
	zipObj.close()
	os.remove('SavedTables.txt')

#Various Old Functions
#Fill the db whith sequences for all species
def Send_species_complexe():
	etat=0
	get_sp = db.query("SELECT spname,spid FROM sp ORDER BY spid")
	for sp in get_sp.dictresult():
		fileopen = False
		send_seq = False
		data_dict = {}
		
		print sp['spname'], sp['spid']
		inFile = sp['spname']+".fasta"
		try :
			ifObj = open(inFile,'r')
			fileopen = True
		except :
			print "Can't open file "+inFile
			inFile = raw_input("\nEntrez le bon fichier: ")
			ifObj = open(inFile,'r')
			fileopen = True
			
		if fileopen :
			for line in ifObj:
				line = line.strip('\r\n')
				if (line.startswith('>')):
					if send_seq :
						query = "INSERT INTO aaseqs VALUES(DEFAULT,'"+str(sp['spid'])+"','"+data_dict['gi']+"','"+data_dict['ref']+"','"+data_dict['aa']+"','"+data_dict['len']+"','"+data_dict['def']+"','"+""+"','"+""+"','"+"0"+"','"+""+"','"+"0"+"')"
						#ofObj.write(query+"\n")
						db.query(query)
						data_dict = {}
						send_seq = False
					data_dict['gi']=line.split('|')[1].split('|')[0]
					data_dict['ref']=line.split('|')[2].split('|')[0]
					data_dict['def']=line.split('|')[3].split('|')[0]
				else:
					AAseq = line.strip(' *')
					data_dict['aa']=AAseq
					data_dict['len']=str(len(AAseq))
					send_seq = True
		query = "INSERT INTO aaseqs VALUES(DEFAULT,'"+str(sp['spid'])+"','"+data_dict['gi']+"','"+data_dict['ref']+"','"+data_dict['aa']+"','"+data_dict['len']+"','"+data_dict['def']+"','"+""+"','"+""+"','"+"0"+"','"+""+"','"+"0"+"')"
		#ofObj.write(query+"\n")
		db.query(query)
		ifObj.close()
		fileopen = False
	db.close()

def JGI_2_NCBI(db):
	# Opening file
	inFile = raw_input("\nFile containing data:")
	ifObj = open(inFile,'r')
	# Checking data
	print "Checking data"
	i=1
	for line in ifObj:
		try :
			line = line.strip('\r\n')
			data = line.split('|')
			localid = data[0]
			gi = data[1]
			ref = data[2]
		except :
			print "Error line "+str(i)+" - "+ str(exc_info()[0])+"\n"+str(blastdata)
			exit()
		if i%1000 == 0:
			print str(i)
		i+=1
	print "Sending data"
	i=1
	ifObj.close()
	ifObj = open(inFile,'r')
	for line in ifObj:
		line = line.strip('\r\n')
		data = line.split('|')
		localid = data[0]
		gi = data[1]
		ref = data[2]
		#print "UPDATE aaseqs SET gi='"+str(gi)+"',ref='"+str(ref)+"' WHERE localid="+str(localid)
		db.query("UPDATE aaseqs SET gi='"+str(gi)+"',ref='"+str(ref)+"' WHERE localid="+str(localid))
		if i%1000 == 0:
			print str(i)
		i+=1
def old_IDs_into_new_DB(db):
	# Opening file
	inFile = raw_input("\nFile containing data:")
	ifObj = open(inFile,'r')
	# Checking data
	print "Checking data"
	i=1
	for line in ifObj:
		try :
			line = line.strip('\r\n')
			data = line.split('|')
			new_id = data[0]
			old_id = data[1]
		except :
			print "Error line "+str(i)+" - "+ str(exc_info()[0])+"\n"+str(blastdata)
			exit()
		if i%1000 == 0:
			print str(i)
		i+=1
	print "Sending data"
	i=1
	ifObj.close()
	ifObj = open(inFile,'r')
	for line in ifObj:
		line = line.strip('\r\n')
		data = line.split('|')
		new_id = data[0]
		old_id = data[1]
		#print "UPDATE aaseqs SET old_id='"+str(old_id)+"' WHERE localid="+str(new_id)
		db.query("UPDATE aaseqs SET old_id='"+str(old_id)+"' WHERE localid="+str(new_id))
		if i%1000 == 0:
			print str(i)
		i+=1


	
