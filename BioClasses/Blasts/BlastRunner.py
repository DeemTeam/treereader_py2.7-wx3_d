#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import BioClasses, os, sys
from BioClasses.Sequences import MyFasta

def MultiLocalBlast(BlastProgram,NbResults,eValueLimit='1e-5',inFile=False,Filter=True,DbFile=False,procNum=False,StartAt=False):
	#---- Asking for query file, if not provided ----#
	if not inFile:	query_file = raw_input("Query file name: ")
	else:	query_file = inFile
	
	#---- Trying to find the formated DB file ----#
	BiggestSize = 0
	if not DbFile:
		BiggestFile = ''
		folder_files = os.listdir(os.getcwd())
		for filename in folder_files:
			if int(os.path.getsize(filename)) > BiggestSize:
				BiggestSize = int(os.path.getsize(filename))
				BiggestFile = filename
		DbFilename = raw_input("Is "+BiggestFile+" your database name ?\nEnter if OK, or write an other name: ")
		if DbFilename=='': DbFilename = BiggestFile
	else : DbFilename = DbFile
	
	#---- If the number of processor is not given, ask for it ----#
	if not procNum:
		procNum = raw_input("Number of processors to use: ")
		procNum = int(procNum)
	if not StartAt:
		StartAt = 1
		StartAt = raw_input("Starting at sequence 1, enter an other number if needed: ")
	StartAt = int(StartAt)-1
		
	#---- Count the number of sequences in the query file ----#
	SeqObject = MyFasta(query_file)
	print '\nquery file contains ' + str(SeqObject.NbSeq) + ' sequences'
	seq_number = 0
	for seq in SeqObject.SequencesList[StartAt:]:
		# write a temporary tmp_query.query file containing the query
		TempFilename = seq.definition.replace(' ','_')
		TempFilename = TempFilename.replace('|','_')
		QueryFilename = TempFilename + '.query'
		
		tmpObj = open(QueryFilename, 'w')
		tmpObj.write(seq.GetInFasta())
		tmpObj.close()
		#Running the blast command
		XMLFilename = TempFilename+'.'+BlastProgram+'.xml'
		CmdTorun = BioClasses.BioPATH.blast + ' -i ' + QueryFilename + ' -d ' + DbFilename + ' -p ' + BlastProgram +' -a ' + str(procNum) + ' -v '+ str(NbResults) + ' -e ' +str(eValueLimit) + ' -b '+ str(NbResults) +' -m 7 -o ' + XMLFilename
		if not Filter:
			CmdTorun+=' -F F'
		os.system(CmdTorun)
		
		seq_number += 1
		print str(seq_number) + ' sequences blasted out of ' + str(SeqObject.NbSeq)
		os.remove(QueryFilename)
		
def MultiLocalBlastPlus(BlastProgram,NbResults,eValueLimit='1e-5',inFile=False,Filter=True,DbFile=False,procNum=False,StartAt=False):
	#---- Asking for query file, if not provided ----#
	if not inFile:	query_file = raw_input("Query file name: ")
	else:	query_file = inFile
	
	#---- Trying to find the formated DB file ----#
	BiggestSize = 0
	if not DbFile:
		BiggestFile = ''
		folder_files = os.listdir(os.getcwd())
		for filename in folder_files:
			if int(os.path.getsize(filename)) > BiggestSize:
				BiggestSize = int(os.path.getsize(filename))
				BiggestFile = filename
		DbFilename = raw_input("Is "+BiggestFile+" your database name ?\nEnter if OK, or write an other name: ")
		if DbFilename=='': DbFilename = BiggestFile
	else : DbFilename = DbFile
	
	#---- If the number of processor is not given, ask for it ----#
	if not procNum:
		procNum = raw_input("Number of processors to use: ")
		procNum = int(procNum)
	if not StartAt:
		StartAt = 1
		StartAt = raw_input("Starting at sequence 1, enter an other number if needed: ")
	StartAt = int(StartAt)-1
		
	#---- Count the number of sequences in the query file ----#
	SeqObject = MyFasta(query_file)
	print '\nquery file contains ' + str(SeqObject.NbSeq) + ' sequences'
	seq_number = 0
	for seq in SeqObject.SequencesList[StartAt:]:
		# write a temporary tmp_query.query file containing the query
		TempFilename = seq.definition.replace(' ','_')
		TempFilename = TempFilename.replace('|','_')
		QueryFilename = TempFilename + '.query'
		
		tmpObj = open(QueryFilename, 'w')
		tmpObj.write(seq.GetInFasta())
		tmpObj.close()
		#Running the blast command
		XMLFilename = TempFilename+'.'+BlastProgram+'.xml'
		#CmdTorun = BlastProgram + ' -query ' + QueryFilename + ' -db ' + DbFilename +' -num_threads ' + str(procNum) + ' -max_target_seqs '+ str(NbResults) + ' -evalue ' +str(eValueLimit) + ' -num_alignments '+ str(NbResults) +' -outfmt 5 -out ' + XMLFilename
		CmdTorun = BlastProgram + ' -query ' + QueryFilename + ' -db ' + DbFilename +' -num_threads ' + str(procNum) + ' -max_target_seqs '+ str(NbResults) + ' -evalue ' +str(eValueLimit) + ' -outfmt 5 -out ' + XMLFilename

		if not Filter:
			if BlastProgram == 'blastp':
				CmdTorun+=' -seg no'
			if BlastProgram == 'blastn':
				CmdTorun+=' -dust no'
		#print CmdTorun
		os.system(CmdTorun)
		
		seq_number += 1
		print str(seq_number) + ' sequences blasted out of ' + str(SeqObject.NbSeq)
		os.remove(QueryFilename)

def MultiLocalBlastPlusOneFile(BlastProgram,NbResults,eValueLimit='1e-5',inFile=False,Filter=True,DbFile=False,procNum=False,StartAt=False):
	#---- Asking for query file, if not provided ----#
	if not inFile:	query_file = raw_input("Query file name: ")
	else:	query_file = inFile
	
	#---- Trying to find the formated DB file ----#
	BiggestSize = 0
	if not DbFile:
		BiggestFile = ''
		folder_files = os.listdir(os.getcwd())
		for filename in folder_files:
			if int(os.path.getsize(filename)) > BiggestSize:
				BiggestSize = int(os.path.getsize(filename))
				BiggestFile = filename
		DbFilename = raw_input("Is "+BiggestFile+" your database name ?\nEnter if OK, or write an other name: ")
		if DbFilename=='': DbFilename = BiggestFile
	else : DbFilename = DbFile
	
	#---- If the number of processor is not given, ask for it ----#
	if not procNum:
		procNum = raw_input("Number of processors to use: ")
		procNum = int(procNum)
	if not StartAt:
		StartAt = 1
		StartAt = raw_input("Starting at sequence 1, enter an other number if needed: ")
		StartAt = int(StartAt)-1
		
	#---- Count the number of sequences in the query file ----#
	SeqObject = MyFasta(query_file)
	print '\nquery file contains ' + str(SeqObject.NbSeq) + ' sequences'
	#Running the blast command
	XMLFilename = query_file+'.'+BlastProgram+'.xml'
	#CmdTorun = BlastProgram + ' -query ' + QueryFilename + ' -db ' + DbFilename +' -num_threads ' + str(procNum) + ' -max_target_seqs '+ str(NbResults) + ' -evalue ' +str(eValueLimit) + ' -num_alignments '+ str(NbResults) +' -outfmt 5 -out ' + XMLFilename
	CmdTorun = BlastProgram + ' -query ' + query_file + ' -db ' + DbFilename +' -num_threads ' + str(procNum) + ' -max_target_seqs '+ str(NbResults) + ' -evalue ' +str(eValueLimit) + ' -outfmt 5 -out ' + XMLFilename

	if not Filter:
		if BlastProgram == 'blastp':
			CmdTorun+=' -seg no'
		if BlastProgram == 'blastn':
			CmdTorun+=' -dust no'
	#print CmdTorun
	os.system(CmdTorun)
	
	
def UniLocalBlast(BlastProgram,NbResults,eValueLimit='1e-5',QuerySeq=False ,Filter=True,DbFile=False,procNum=False):
	#---- Trying to find the formated DB file ----#
	BiggestSize = 0
	if not DbFile:
		BiggestFile = ''
		folder_files = os.listdir(os.getcwd())
		for filename in folder_files:
			if int(os.path.getsize(filename)) > BiggestSize:
				BiggestSize = int(os.path.getsize(filename))
				BiggestFile = filename
		DbFilename = raw_input("Is "+BiggestFile+" your database name ?\nEnter if OK, or write an other name: ")
		if DbFilename=='': DbFilename = BiggestFile
	else : DbFilename = DbFile
	
	#---- If the number of processor is not given, ask for it ----#
	if not procNum:
		procNum = raw_input("Number of processors to use: ")
		procNum = int(procNum)
	
	#---- Count the number of sequences in the query file ----#
	TempFilename = QuerySeq.definition.replace(' ','_')
	TempFilename = TempFilename.replace('|','_')
	QueryFilename = TempFilename + '.query'
	tmpObj = open(QueryFilename, 'w')
	tmpObj.write(QuerySeq.GetInFasta())
	tmpObj.close()
	
	#Running the blast command
	XMLFilename = TempFilename+'.'+BlastProgram+'.xml'
	CmdTorun = BioClasses.BioPATH.blast + ' -i ' + QueryFilename + ' -d ' + DbFilename + ' -p ' + BlastProgram +' -a ' + str(procNum) + ' -v '+ str(NbResults) + ' -e ' +str(eValueLimit) + ' -b '+ str(NbResults) +' -m 7 -o ' + XMLFilename
	if not Filter: CmdTorun+=' -F F'
	os.system(CmdTorun)
		
	os.remove(QueryFilename)
	return XMLFilename
		
def MultiNetBlast(nbRes,evallim='1e-5',inFile=False):
	#---- Asking for query
	if not inFile :	query_file = raw_input("Query file name: ")
	else :	query_file = inFile
	
	libs = ['nr','refseq']
	Choose_lib = raw_input("Choose nr or refseq: ")
	if Choose_lib not in libs:
		print "Error in choice"
		sys.exit()
	
	#---- Blast the seq_to_blast using the NCBIWWW against the nr database
	SeqObject = MyFasta(query_file)
	print '\nQuery file contains ' + str(SeqObject.NbSeq) + ' sequences'
	seq_number = 0
	for seq in SeqObject.SequencesList:
		GoNext = False
		# write a temporary tmp_query.query file containing the query
		QueryFilename = seq.definition + '.query'
		QueryFilename = QueryFilename.replace(' ','_')
		tmpObj = open(QueryFilename, 'w')
		tmpObj.write(seq.GetInFasta())
		tmpObj.close()
		#Running the blast command
		XMLFilename = seq.definition.replace(' ','_')+'.NetBlast.xml'
		CmdTorun = BioClasses.BioPATH.netblast + ' -i '+QueryFilename+' -d '+ query_file +' -p blastp -a 2 -e '+evallim+' -v '+str(nbRes)+ ' -b '+str(nbRes)+ ' -m 7 -o ' + XMLFilename
		while not GoNext:
			try:
				os.system(CmdTorun)
				GoNext = True
			# raise exception in case of network problems     
			except IOError:
				print "\nNetwork down, waiting 10 seconds before continuing"
				time.sleep(10)
			except URLError:
				print "URLError"
				GoNext = True
			except:
				print "Unexpected error:", sys.exc_info()[0]
				GoNext = True
		seq_number += 1
		print str(seq_number) + ' sequences blasted out of ' + str(SeqObject.NbSeq)
		os.remove(QueryFilename)
		
		
def MultiNetBlastX(nbRes,evallim='1e-5',inFile=False):
	#---- Asking for query
	if not inFile :	query_file = raw_input("Query file name: ")
	else :	query_file = inFile
	
	#---- Blast the seq_to_blast using the NCBIWWW against the nr database
	SeqObject = MyFasta(query_file)
	print '\nQuery file contains ' + str(SeqObject.NbSeq) + ' sequences'
	seq_number = 0
	for seq in SeqObject.SequencesList:
		GoNext = False
		# write a temporary tmp_query.query file containing the query
		QueryFilename = seq.definition + '.query'
		QueryFilename = QueryFilename.replace(' ','_')
		tmpObj = open(QueryFilename, 'w')
		tmpObj.write(seq.GetInFasta())
		tmpObj.close()
		#Running the blast command
		XMLFilename = seq.definition.replace(' ','_')+'.NetBlast.xml'
		CmdTorun = BioClasses.BioPATH.netblast + ' -i '+QueryFilename+' -d nr -p blastp -a 2 -e '+evallim+' -v '+str(nbRes)+ ' -b '+str(nbRes)+ ' -m 7 -o ' + XMLFilename
		while not GoNext:
			try:
				os.system(CmdTorun)
				GoNext = True
			# raise exception in case of network problems     
			except IOError:
				print "\nNetwork down, waiting 10 seconds before continuing"
				time.sleep(10)
			except URLError:
				print "URLError"
				GoNext = True
			except:
				print "Unexpected error:", sys.exc_info()[0]
				GoNext = True
		seq_number += 1
		print str(seq_number) + ' sequences blasted out of ' + str(SeqObject.NbSeq)
		os.remove(QueryFilename)
		
def BlastLaunchCommon(BlastType):
	nbRes = raw_input("\nHow many results to store ? ")
	evallim = raw_input("\nEvalue threshold (ex: 1e-10, default : 1e-5)? ")
	askfilter = raw_input("\nFilter sequences (Y/N, Default is Yes) ? ")
	if nbRes.isdigit():
		if evallim!='':
			if askfilter == 'N' or askfilter == 'n':
				MultiLocalBlast(BlastType, nbRes, evallim, Filter=False)
			else:
				MultiLocalBlast(BlastType, nbRes, evallim, Filter=True)
		else:
			if askfilter == 'N' or askfilter == 'n':
				MultiLocalBlast(BlastType, nbRes, Filter=False)
			else:
				MultiLocalBlast(BlastType, nbRes, Filter=True)
			
			
def BlastPlusLaunchCommon(BlastType, OneFile=False):
	nbRes = raw_input("\nHow many results to store ? ")
	evallim = raw_input("\nEvalue threshold (ex: 1e-10, default : 1e-5)? ")
	askfilter = raw_input("\nFilter sequences (Y/N, Default is Yes) ? ")
	if nbRes.isdigit():
		if OneFile:
			if evallim!='':
				if askfilter == 'N' or askfilter == 'n':
					MultiLocalBlastPlusOneFile(BlastType, nbRes, evallim, Filter=False)
				else:
					MultiLocalBlastPlusOneFile(BlastType, nbRes, evallim, Filter=True)
			else:
				if askfilter == 'N' or askfilter == 'n':
					MultiLocalBlastPlusOneFile(BlastType, nbRes, Filter=False)
				else:
					MultiLocalBlastPlusOneFile(BlastType, nbRes, Filter=True)
		else:
			if evallim!='':
				if askfilter == 'N' or askfilter == 'n':
					MultiLocalBlastPlus(BlastType, nbRes, evallim, Filter=False)
				else:
					MultiLocalBlastPlus(BlastType, nbRes, evallim, Filter=True)
			else:
				if askfilter == 'N' or askfilter == 'n':
					MultiLocalBlastPlus(BlastType, nbRes, Filter=False)
				else:
					MultiLocalBlastPlus(BlastType, nbRes, Filter=True)
				
	
def MultiNCBIBlastPlus(nbRes,evallim='1e-5',inFile=False,DbFile=False):
	#---- Asking for query file, if not provided ----#
	if not inFile:	query_file = raw_input("Query file name: ")
	else:	query_file = inFile
	
	if not DbFile:
		libs = ['nr','refseq_protein']
		DbFile = raw_input("Choose nr or refseq_protein: ")
		if DbFile not in libs:
			print "Error in choice"
			sys.exit()
		
	#---- Count the number of sequences in the query file ----#
	SeqObject = MyFasta(query_file)
	print '\nquery file contains ' + str(SeqObject.NbSeq) + ' sequences'
	seq_number = 0
	for seq in SeqObject.SequencesList:
		# write a temporary tmp_query.query file containing the query
		TempFilename = seq.definition.replace(' ','_')
		TempFilename = TempFilename.replace('|','_')
		QueryFilename = TempFilename + '.query'
		
		tmpObj = open(QueryFilename, 'w')
		tmpObj.write(seq.GetInFasta())
		tmpObj.close()
		#Running the blast command
		XMLFilename = TempFilename+'NetBlastP.xml'
		CmdTorun = 'blastp -query ' + QueryFilename + ' -db ' + DbFile +' -remote  -evalue ' +str(evallim) + ' -num_alignments '+ str(nbRes) +' -outfmt 5 -out ' + XMLFilename
		#-max_target_seqs '+ str(nbRes) + '
		print CmdTorun
		os.system(CmdTorun)
		
		seq_number += 1
		print str(seq_number) + ' sequences blasted out of ' + str(SeqObject.NbSeq)
		os.remove(QueryFilename)
	
