#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

def GetXMLBlastData(inFile):
	ifObj = open(inFile,'r')
	XMLdata = ''
	for line in ifObj:
		XMLdata += line.strip('\r\n')
	ifObj.close()
	TempoObj = BlastParser()
	return TempoObj.DoParse(XMLdata)

def GetDataBaseFormatedBlastHit(BlastString):
	BlastData = DatabaseFormatedBlastHit()
	BlastString = BlastString.strip('\r\n')
	data = BlastString.split('|')
	if len(data)!=13:
		print "Bad data format, "+str(13-len(data))+" missing entries"
		return False
	else:
		try:
			BlastData.query=data[0]
			BlastData.Hit_def=data[1]
			BlastData.Hsp_score=int(data[2])
			if BlastData.Hsp_score>32000:		BlastData.Hsp_score=32000
			BlastData.Hsp_bit_score=float(data[3])
			BlastData.Hsp_evalue=float(data[4])
			BlastData.Hsp_identity=int(data[5])
			if BlastData.Hsp_identity>32000:	BlastData.Hsp_identity=32000
			BlastData.Hsp_positive=int(data[6])
			if BlastData.Hsp_positive>32000:	BlastData.Hsp_positive=32000
			BlastData.Hsp_query_from=int(data[7])
			BlastData.Hsp_query_to=int(data[8])
			BlastData.Hsp_hit_from=int(data[9])
			BlastData.Hsp_hit_to=int(data[10])
			BlastData.Hsp_align_len=int(data[11])
			try :
				BlastData.Hsp_qseq = data[12]
			except:
				pass
			return BlastData
		except:
			print "Bad data format"
			return False

class BlastParser(object):
	def __init__(self):
		self.Iteration_query_def_open = False
		self.Iteration_query_len_open = False 
		self.Iteration_query_done = False
		self.query = ''
		self.query_len = '0'
		self.Hit_open = False
		self.Hit_id_open = False
		self.Hit_def_open = False
		self.Hit_len_open = False
		self.Hsp_open = False
		self.Hsp_bit_score_open = False
		self.Hsp_score_open = False
		self.Hsp_evalue_open = False
		self.Hsp_query_from_open = False
		self.Hsp_query_to_open = False
		self.Hsp_hit_from_open = False
		self.Hsp_hit_to_open = False
		self.Hsp_query_frame_open = False
		self.Hsp_hit_frame_open = False
		self.Hsp_identity_open = False
		self.Hsp_positive_open = False
		self.Hsp_align_len_open = False
		self.Hsp_qseq_open = False
		self.Hsp_hseq_open = False
		self.Hsp_midline_open = False
		
		self.curr_hit = Blast_hit()
		self.curr_hsp = False
		self.list_of_hits = []
	
	def DoParse(self, XMLdata):
		open_tag = False
		close_tag = False
		inside_tag = False
		between_tag = False
		current_tag = ''
		current_chars = ''
		for char in XMLdata:
			if char == '<':
				if between_tag :
					#print "Chars = "+current_chars
					self.characters(current_chars)
					current_chars = ''
					between_tag = False
				inside_tag = True
				open_tag = True
			elif char == '>':
				if open_tag:
					#print "Open tag = "+current_tag
					self.startElement(current_tag)
					current_tag = ''
					open_tag = False
					inside_tag = False
					between_tag = True
				if close_tag:
					#print "Close tag = "+current_tag
					self.endElement(current_tag)
					current_tag = ''
					close_tag = False
					inside_tag = False
			elif char == '/':
				if inside_tag:
					close_tag = True
					open_tag = False
			else :
				if inside_tag :
					current_tag += char
				if between_tag :
					current_chars += char
		#Renvoi des lignes de blast
		if len(self.list_of_hits)==0:
			return False, self.query
		else:
			return True, self.list_of_hits
	
	def startElement(self, name):
	#"fonction appelee lorsque le parser rencontre une balise ouvrante"
		if name=='BlastOutput_query-def':self.Iteration_query_def_open = True
		if name=='Iteration_query-len':	self.Iteration_query_len_open = True
		if name=='Hit':					self.Hit_open = True
		if name=='Hit_id':				self.Hit_id_open = True
		if name=='Hit_def':				self.Hit_def_open = True
		if name=='Hit_len':				self.Hit_len_open = True
		if name=='Hsp':
			self.curr_hsp = Hit_hsp()
			self.Hsp_open = True
		if name=='Hsp_bit-score':		self.Hsp_bit_score_open = True
		if name=='Hsp_score':			self.Hsp_score_open = True
		if name=='Hsp_evalue':			self.Hsp_evalue_open = True
		if name=='Hsp_query-from':		self.Hsp_query_from_open = True
		if name=='Hsp_query-to':		self.Hsp_query_to_open = True
		if name=='Hsp_hit-from':		self.Hsp_hit_from_open = True
		if name=='Hsp_hit-to':			self.Hsp_hit_to_open = True
		if name=='Hsp_query-frame':		self.Hsp_query_frame_open = True
		if name=='Hsp_hit-frame':		self.Hsp_hit_frame_open = True
		if name=='Hsp_identity':		self.Hsp_identity_open = True
		if name=='Hsp_positive':		self.Hsp_positive_open = True
		if name=='Hsp_align-len':		self.Hsp_align_len_open = True
		if name=='Hsp_qseq':			self.Hsp_qseq_open = True
		if name=='Hsp_hseq':			self.Hsp_hseq_open = True
		if name=='Hsp_midline':			self.Hsp_midline_open = True
	def characters(self, ch):
	#"fonction appelee lorsque le parser rencontre des donnees dans un element"
		if self.Iteration_query_def_open:
			if not self.Iteration_query_done:
				self.query = ch
				self.curr_hit.query = self.query
		if self.Iteration_query_len_open:
			if not self.Iteration_query_done:
				self.query_len = ch
				self.curr_hit.query_len = int(self.query_len)
				self.Iteration_query_done = True
		if self.Hit_id_open:				self.curr_hit.Hit_id = ch
		if self.Hit_def_open:				self.curr_hit.Hit_def = ch
		if self.Hit_len_open:				self.curr_hit.Hit_len = int(ch)
		if self.Hsp_bit_score_open:			self.curr_hsp.Hsp_bit_score = ch
		if self.Hsp_score_open:				self.curr_hsp.Hsp_score = ch
		if self.Hsp_evalue_open:			self.curr_hsp.Hsp_evalue = ch
		if self.Hsp_query_from_open:		self.curr_hsp.Hsp_query_from = int(ch)
		if self.Hsp_query_to_open:			self.curr_hsp.Hsp_query_to = int(ch)
		if self.Hsp_hit_from_open:			self.curr_hsp.Hsp_hit_from = int(ch)
		if self.Hsp_hit_to_open:			self.curr_hsp.Hsp_hit_to = int(ch)
		if self.Hsp_query_frame_open:		self.curr_hsp.Hsp_query_frame = int(ch)
		if self.Hsp_hit_frame_open:			self.curr_hsp.Hsp_hit_frame = int(ch)
		if self.Hsp_identity_open:			self.curr_hsp.Hsp_identity = int(ch)
		if self.Hsp_positive_open:			self.curr_hsp.Hsp_positive = int(ch)
		if self.Hsp_align_len_open:			self.curr_hsp.Hsp_align_len = int(ch)
		if self.Hsp_qseq_open:				self.curr_hsp.Hsp_qseq = ch
		if self.Hsp_hseq_open:				self.curr_hsp.Hsp_hseq = ch
		if self.Hsp_midline_open:			self.curr_hsp.Hsp_midline = ch
	
	def endElement(self, name):
	#"fonction appelee lorsque le parser rencontre une balise fermante"
		if name=='BlastOutput_query-def':		self.Iteration_query_def_open = False
		if name=='Iteration_query-len':			self.Iteration_query_len_open = False
		if name=='Hit':
			self.Hit_open = False
			self.list_of_hits.append(self.curr_hit)
			self.curr_hit = Blast_hit()
			self.curr_hit.query = self.query
			self.curr_hit.query_len = int(self.query_len)
		if name=='Hit_id':					self.Hit_id_open = False
		if name=='Hit_def':					self.Hit_def_open = False
		if name=='Hit_len':					self.Hit_len_open = False
		if name=='Hsp':
			self.curr_hit.Hsps.append(self.curr_hsp)
			self.curr_hit.Num_hsp += 1
			self.Hsp_open = False
		if name=='Hsp_bit-score':			self.Hsp_bit_score_open = False
		if name=='Hsp_score':				self.Hsp_score_open = False
		if name=='Hsp_evalue':				self.Hsp_evalue_open = False
		if name=='Hsp_query-from':			self.Hsp_query_from_open = False
		if name=='Hsp_query-to':			self.Hsp_query_to_open = False
		if name=='Hsp_hit-from':			self.Hsp_hit_from_open = False
		if name=='Hsp_hit-to':				self.Hsp_hit_to_open = False
		if name=='Hsp_query-frame':			self.Hsp_query_frame_open = False
		if name=='Hsp_hit-frame':			self.Hsp_hit_frame_open = False
		if name=='Hsp_identity':			self.Hsp_identity_open = False
		if name=='Hsp_positive':			self.Hsp_positive_open = False
		if name=='Hsp_align-len':			self.Hsp_align_len_open = False
		if name=='Hsp_qseq':				self.Hsp_qseq_open = False
		if name=='Hsp_hseq':				self.Hsp_hseq_open = False
		if name=='Hsp_midline':				self.Hsp_midline_open = False
	
class Blast_hit(object):
	def __init__(self):
		self.query = ''
		self.query_len = 0
		self.Num_hsp = 0
		self.Hit_id = ''
		self.Hit_def = ''
		self.Hsps = []
		self.Hit_len = 0
		self.best_evalue = 0
	
class Hit_hsp(object):
	def __init__(self):
		self.Hsp_bit_score = ''
		self.Hsp_score = ''
		self.Hsp_evalue = ''
		self.Hsp_query_from = ''
		self.Hsp_query_to = ''
		self.Hsp_hit_from = ''
		self.Hsp_hit_to = ''
		self.Hsp_query_frame = ''
		self.Hsp_hit_frame = ''
		self.Hsp_identity = ''
		self.Hsp_positive = ''
		self.Hsp_gaps = ''
		self.Hsp_align_len = ''
		self.Hsp_qseq = ''
		self.Hsp_hseq = ''
		self.Hsp_midline = ''
	
class DatabaseFormatedBlastHit(object):
	def __init__(self):
		self.query = ''
		self.Hit_def = ''
		self.Hsp_bit_score = ''
		self.Hsp_score = ''
		self.Hsp_evalue = ''
		self.Hsp_query_from = ''
		self.Hsp_query_to = ''
		self.Hsp_hit_from = ''
		self.Hsp_hit_to = ''
		self.Hsp_query_frame = ''
		self.Hsp_hit_frame = ''
		self.Hsp_identity = ''
		self.Hsp_positive = ''
		self.Hsp_gaps = ''
		self.Hsp_align_len = ''
		self.Hsp_qseq = ''
		self.Hsp_hseq = ''
		self.Hsp_midline = ''
