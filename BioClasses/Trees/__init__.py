# BioClasses Package
#Trees Part

from TreeBase import *
from TreeWx import *
from TreeLauncher import *
#from TreeWx import *
from sys import exit

def OpenTreeFile(Filename):
	ifObj = open(Filename,'r')
	Treeline = ifObj.readline()
	ifObj.close()
	return Arbre(Treeline)
	
def OpenMultiTreeFile(Filename):
	ifObj = open(Filename,'r')
	AllData = ""
	TreeList = []
	for line in ifObj:
		AllData += line.strip("\r\n")
	ifObj.close()
	TreeLineList = AllData.split(';')
#	for TreeLine in TreeLineList:
#		TreeList.append(Arbre(TreeLine+';'))
	return TreeLineList
	
def RestoreSeqNamesInTree(TreeFile, SeqNamesFile=False):
	# Getting seqnames file if not provided
	if not SeqNamesFile :
		SeqNamesFile = GetFilePrefix(FastaFile) + ".seqnames"
		AskForSeqNamesFile = raw_input("\nDoes "+seqnamesFile+" contain your sequences names and numbers ?? \n Press Enter if OK or type the correct name : ")
		if AskForSeqNamesFile != '': SeqNamesFile = AskForSeqNamesFile
	# Tryin to open SeqName file
	try : SeqNameFileObj = open(SeqNamesFile,'r')
	except :
		print "Error while opening file, does "+SeqNamesFile+" exists ?"
		exit()
	
	# Getting seqNums 2 seqNames data
	SeqNameFileDict = {}
	for line in SeqNameFileObj:
		sptuple = line.strip('\r\n').split('!')
		SeqNameFileDict[sptuple[0]]=sptuple[1]
	SeqNameFileObj.close()
	
	# Changing names in tree file
	MyTree = OpenTreeFile(TreeFile)
	
	#Modifiing tree
	for Leaf in MyTree.GetAllLeafList():
		try :
			Leaf.LabelData.Label = SeqNameFileDict[Leaf.LabelData.Label]
		except :
			print "\nError while modifiing tree, Missing Seqence Name"
			exit()
	
	#writing final tree
	ofObj = open(TreeFile,'w')
	ofObj.write(MyTree.PrintInNewick())
	ofObj.close()
	
