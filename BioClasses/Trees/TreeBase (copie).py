#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import math

class Node(object):
	def __init__ (self, id, parent, support=0, BranchLen=0):
		#, NodeParts=[]
		self.id = id
		self.parent = parent
		self.support = support
		self.BranchLen = BranchLen
		self.NodeParts = []
		self.x = 0
		self.y = 0
		self.Region = False
		
	def IsNode(self):
		return True
	def IsLeaf(self):
		return False
	def NumberOfParts(self):
		return len(self.NodeParts)
		
class Leaf(object):
	def __init__ (self, id, parent, LabelData, BranchLen, local=False, color=False, Active=True):
		self.id = id
		self.parent = parent
		self.BranchLen = BranchLen
		
		if isinstance(LabelData, LabelDataObj): self.LabelData = LabelData
		else : self.LabelData = LabelDataObj(LabelData) 
		
		self.local = local
		self.Color = color
		
		self.Active = Active
		self.x = 0
		self.y = 0
		self.Region = False
		
	def IsNode(self):
		return False
	def IsLeaf(self):
		return True
	def IsActive(self):
		return self.Active

class LabelDataObj(object):
	def __init__ (self, Label):
		# Original Label
		self.Label = Label
		# Color Data
		# self.Color = Color
		# Database Data
		self.LocalID = ''
		self.GI = ''
		self.Accession = ''
		self.Definition = ''
		self.Groups = []
		# Label to write on screen
		self.LabelToWrite = Label
		
class GroupDataObj(object):
	def __init__(self, GroupID, GroupName):
		self.GroupName = GroupName
		self.GroupID = GroupID
		
class DrawSegment(object):
	def __init__ (self, Part, xfrom, yfrom, xto, yto, vertical, horizontal):
		self.Part = Part
		self.xfrom = xfrom
		self.yfrom = yfrom
		self.xto = xto
		self.yto = yto
		self.vertical = vertical
		self.horizontal = horizontal

class Arbre(object):
	def __init__(self, TreeString, Configurator=False):
	#Options
		self.showlog = False
	#Variables for structures
		self.TreeStructures = []
		self.TreeParts = []
		self.TempoTreeParts = []
	#Variables for drawing
		self.Configurator = Configurator
		self.PartsLeft = []
		self.CurrentLine = 0
		self.Elem2Ligne = []
		self.segments = []
		self.NodeWasJustClosed = False
		self.LongestSegment = (0,False)
	# Start parsing tree
		self.ParseNewickString(TreeString)
	# Get ratio 1/(num of line)
		self.RatioY = 1.0/self.NumberOfLeaf()
	# Store the first structure
		self.TreeStructures.append(self.TreeParts)
	# If it's needed to get info from db
		if self.Configurator : 
			self.ApplyConfigurator()
			self.UpdateLeafConfiguration()
	# Make the Draw sructure
		self.RecursiveDrawLoop()
	
	def ParseNewickString(self, NewickString):
		tokens =["(",")",",",";"]
		CurrentText = ''
		Nodes_NbOpenOnNbClosed = 0
		NodeWasJustClosed = False
		CurrentNodeNumber = 0
		for chars in NewickString:
			if chars in tokens:
				if chars == '(':
					Nodes_NbOpenOnNbClosed += 1
					CurrentNodeNumber = self.OpenNode(CurrentNodeNumber)
					if self.showlog: print "Opening node "+str(CurrentNodeNumber)
				if chars == ',' and CurrentText != '':
					part1 = CurrentText.split(':')[0]
					part2 = CurrentText.split(':')[1]
					if NodeWasJustClosed:
						if self.showlog: print "Closing node "+str(CurrentNodeNumber)+" data: "+part1+", "+part2
						if part1 == '' : part1 = 0
						else : part1 = float(part1)
						if part2 == '' : part2 = 0
						else : part2 = float(part2)
						CurrentNodeNumber = self.CloseNode(CurrentNodeNumber, part1, part2)
						NodeWasJustClosed = False
					else :
						part1 = part1.strip(' ')
						part2 = float(part2)
						if self.showlog: print "Adding leaf "+str(part1)+" _ "+str(part2)
						self.AddLeaf(CurrentNodeNumber, part1, part2)
					CurrentText = ''
					if self.showlog: print "Current Node "+str(CurrentNodeNumber)
				
				if chars == ')':
					Nodes_NbOpenOnNbClosed -= 1
					part1 = CurrentText.split(':')[0]
					part2 = CurrentText.split(':')[1]
					if Nodes_NbOpenOnNbClosed == 0:
						if NodeWasJustClosed:
							if self.showlog: print "Close parent : End of tree - "+part1+" _ "+part2
							if part1 == '' : part1 = 0
							else : part1 = float(part1)
							if part2 == '' : part2 = 0
							else : part2 = float(part2)
							CurrentNodeNumber = self.CloseNode(CurrentNodeNumber, part1, part2)
						else :
							part1 = part1.strip(' ')
							part2 = float(part2)
							if self.showlog: print "Adding leaf "+str(part1)+" _ "+str(part2)
							self.AddLeaf(CurrentNodeNumber, part1, part2)
					elif NodeWasJustClosed:
						if self.showlog: print "Ferme le noeud "+str(CurrentNodeNumber)+" data: "+part1+", "+part2
						if part1 == '' : part1 = 0
						else : part1 = float(part1)
						if part2 == '' : part2 = 0
						else : part2 = float(part2)
						CurrentNodeNumber = self.CloseNode(CurrentNodeNumber, part1, part2)
					else :
						part1 = part1.strip(' ')
						part2 = float(part2)
						if self.showlog: print "Adding leaf "+str(part1)+" _ "+str(part2)
						self.AddLeaf(CurrentNodeNumber, part1, part2)
						
					NodeWasJustClosed = True
					CurrentText = ''
					if self.showlog: print "Noeud courant "+str(CurrentNodeNumber)
			else :
				CurrentText += str(chars)
	def NumberOfLeaf(self):
		total = 0
		for TreePart in self.TreeParts:
			if TreePart.IsLeaf(): total +=1
		return total
	
	def NumberOfParts(self):
		return len(self.TreeParts)
	
	def OpenNode(self, CurrentNodeNumber):
		if self.NumberOfParts() == 0 : 
			self.TreeParts.append(Node(CurrentNodeNumber, False))
		else:
			PreviousNodeNumber = CurrentNodeNumber
			CurrentNodeNumber = self.NumberOfParts()
			self.TreeParts.append(Node(CurrentNodeNumber, self.TreeParts[PreviousNodeNumber]))
			self.TreeParts[PreviousNodeNumber].NodeParts.append(self.TreeParts[CurrentNodeNumber])
		return CurrentNodeNumber
	
	def AddLeaf(self, CurrentNodeNumber, label, BranchLen):
		CurrentPartNumber = self.NumberOfParts()
		self.TreeParts.append(Leaf(CurrentPartNumber, self.TreeParts[CurrentNodeNumber], label, BranchLen))
		self.TreeParts[CurrentNodeNumber].NodeParts.append(self.TreeParts[CurrentPartNumber])
		
	def CloseNode(self, CurrentNodeNumber, support, BranchLen):
		self.TreeParts[CurrentNodeNumber].support = support
		self.TreeParts[CurrentNodeNumber].BranchLen = BranchLen
		return self.TreeParts[CurrentNodeNumber].parent.id
	def GetAllPartsOfNode(self, Part, StartList):
		AllPartsList = StartList
		for NodePart in Part.NodeParts:
			AllPartsList.append(NodePart)
			if NodePart.IsNode(): self.GetAllPartsOfNode(NodePart, AllPartsList)
		return AllPartsList
	def GetAllLeafsOfNode(self, Node):
		LeafList=[]
		for Part in self.GetAllPartsOfNode(Node, []):
			if Part.IsLeaf(): LeafList.append(Part)
		return LeafList
	def GetAllLeafList(self):
		LeafList=[]
		for TreePart in self.TreeParts:
			if TreePart.IsLeaf(): LeafList.append(TreePart)
		return LeafList
	def GetActiveLeafs(self):
		ActiveLeafs=[]
		for Leaf in self.GetAllLeafList():
			if Leaf.IsActive(): ActiveLeafs.append(Leaf)
		return ActiveLeafs
	def tell_structure(self):
		print "structure"
		for Part in self.TreeParts:
			if Part.IsNode():
				print "Node ", Part.id
				for subPart in  Part.NodeParts :
					if subPart.IsNode():
						print "	Node : "+str(subPart.id)+" - "+str(subPart.NumberOfParts())+ " Nodeparts"
					elif subPart.IsLeaf():
						print "	Leaf : "+str(subPart.id)+" - "+str(subPart.LabelData.Label)
	# Finding methods
	def FindALeafFullName(self, SearchString):
		LeafList=[]
		for leaf in self.GetAllLeafList():
			if leaf.LabelData.Label==SearchString : LeafList.append(leaf)
		return LeafList
	def FindAPartByID(self, SearchID):
		SearchID = int(SearchID)
		found = False
		for part in self.TreeParts:
			if part.id == SearchID : found = part
		return found
	# Editing methods
	def Reroot(self, CurrentPart, CurrentNewNode = False):
		if not CurrentNewNode: # Sarting the Process
			# Only start the process if the chosen part is not alreay the outgroup
			if CurrentPart != self.TreeParts[1]:
				# Re initialize Tempo Tree Parts
				self.TempoTreeParts = []
				self.PartsLeft = []
				for Part in self.TreeParts:
					self.PartsLeft.append(Part.id)
				if CurrentPart.IsNode():#Starting from Node
					if self.showlog: print "Starting from Node"
					# Create a New root
					if self.showlog: print "Creating New Root"
					self.TempoTreeParts.append(Node(0, False))
					# Put back the Outgroup Group
					if self.showlog: print "Registering new outgroup Node and removing it from todo list"
					NewPart = Node(1,self.TempoTreeParts[0], CurrentPart.support, CurrentPart.BranchLen*0.5)
					self.TempoTreeParts.append(NewPart)
					# And register it to the root
					self.TempoTreeParts[0].NodeParts.append(self.TempoTreeParts[1])# And register it to root parts
					self.PartsLeft.remove(CurrentPart.id) # Remove Outgroup Node from the list todo
					CurrentNewNode = NewPart
					# Register all parts for this Current Part Parent to New parts
					if self.showlog: print "---Launching  ReRoot_DeclareParts using Part:",CurrentPart.id," CurrNewNode:", CurrentNewNode.id
					self.ReRoot_DeclareParts(CurrentPart, CurrentNewNode)
					# Add the rest of the Outgroup Len to create a new node
					if self.showlog: print "Creating Rest of the outgroup node"
					NewPart = Node(len(self.TempoTreeParts),self.TempoTreeParts[0], CurrentPart.support, CurrentPart.BranchLen*0.5)
					self.TempoTreeParts.append(NewPart)
					CurrentNewNode = NewPart
					# And register it to the root
					self.TempoTreeParts[0].NodeParts.append(NewPart)
					#if CurrentPart.parent:
					if self.showlog: print "Re-Launching  Reroot using Node:",CurrentPart.parent.id," CurrNewNode:", CurrentNewNode.id
					self.Reroot(CurrentPart.parent, CurrentNewNode)
					
				elif CurrentPart.IsLeaf():#Starting from Leaf
					if self.showlog: print "Starting from Leaf"
					# Create a New root
					if self.showlog: print "Creating New Root"
					self.TempoTreeParts.append(Node(0, False))
					# Put back the Outgroup Leaf
					if self.showlog: print "Registering new outgroup leaf and removing it from todo list"
					self.TempoTreeParts.append(Leaf(1, self.TempoTreeParts[0], CurrentPart.LabelData.Label, CurrentPart.BranchLen*0.7, CurrentPart.local, CurrentPart.Color, CurrentPart.Active))
					self.TempoTreeParts[0].NodeParts.append(self.TempoTreeParts[1])# And register it to root parts
					self.PartsLeft.remove(CurrentPart.id) # Remove Outgroup Leaf from the list todo
					# Add the rest of the Outgroup Len to create a new node
					if self.showlog: print "Creating Node number 2"
					self.TempoTreeParts.append(Node(2, self.TempoTreeParts[0], 0, CurrentPart.BranchLen*0.3))
					CurrentNewNode = self.TempoTreeParts[2]
					# And register it to the root
					self.TempoTreeParts[0].NodeParts.append(self.TempoTreeParts[2])
					#if CurrentPart.parent:
					if self.showlog: print "Re-Launching  Reroot using Node:",CurrentPart.parent.id," CurrNewNode:", CurrentNewNode.id
					self.Reroot(CurrentPart.parent, CurrentNewNode)
				
		elif CurrentPart.parent:
			# Register all parts for this Current Part Parent to New parts
			if self.showlog: print "---Launching  ReRoot_DeclareParts using Part:",CurrentPart.id," CurrNewNode:", CurrentNewNode.id
			self.ReRoot_DeclareParts(CurrentPart, CurrentNewNode)
			# Add the calling Node to the New Tree part
			if self.showlog: print "Creating New Node number", len(self.TempoTreeParts)
			NewPart = Node(len(self.TempoTreeParts),CurrentNewNode, CurrentPart.support, CurrentPart.BranchLen)
			self.TempoTreeParts.append(NewPart)
			# Add the calling Node to it's New parent Node
			CurrentNewNode.NodeParts.append(NewPart)
			# Remove it from the ToDo list
			self.PartsLeft.remove(CurrentPart.id)
			# Shifting it as the New Current Node
			CurrentNewNode = NewPart
			# And relauch Reroot using the parent of the curent part
			if self.showlog: print "Re-Launching  Reroot using Part.parent:",CurrentPart.parent.id," CurrNewNode:", CurrentNewNode.id, "\nParts Left:", self.PartsLeft
			self.Reroot(CurrentPart.parent, CurrentNewNode)
		else:# We are at root node
			if self.showlog: print "We are at root"
			# Look if Root is a trifurcation or a bifurcation
			if len(CurrentPart.NodeParts) == 3:
				if self.showlog: print "Root is a trifurcation"
				# Register all parts for this Current Part Parent to New parts
				if self.showlog: print "---Launching  ReRoot_DeclareParts using Part:",CurrentPart.id," CurrNewNode:", CurrentNewNode.id
				self.ReRoot_DeclareParts(CurrentPart, CurrentNewNode)
			else:
				if self.showlog: print "Root is a bifurcation, Deleting the last added Node and adding it's branch len to the next part"
				ToAddForNext = CurrentNewNode.BranchLen
				PreviousNewNode = CurrentNewNode.parent
				self.TempoTreeParts.remove(CurrentNewNode)
				PreviousNewNode.NodeParts.remove(CurrentNewNode)
				if self.showlog: print "---Launching  ReRoot_DeclareParts using Part:",CurrentPart.id," PreviousNewNode:", PreviousNewNode.id
				self.ReRoot_DeclareParts(CurrentPart, PreviousNewNode, ToAddForNext)
			
			# Store the New structure
			if self.showlog: print "storing Structure"
			self.TreeStructures.append(self.TempoTreeParts)
			# Replace the Old structure by the new One
			self.TreeParts = self.TempoTreeParts
			if self.showlog: print "ReRoot Done"
			self.RecursiveDrawLoop()
			if self.showlog: print "ReDraw Done"
	def ReRoot_DeclareParts(self, CurrentNode, CurrentNewNode, Adding = 0):
		for Part in CurrentNode.NodeParts:
			if self.showlog: print "------Considering part ",Part.id
			if Part.id in self.PartsLeft:
				if self.showlog: print "------Not Done Yet"
				if Part.IsNode() : 
					# Register Node to new Tree Parts
					NewPart = Node(len(self.TempoTreeParts), CurrentNewNode, Part.support, Part.BranchLen+Adding)
					self.TempoTreeParts.append(NewPart)
					# Register Node to it's parent Node
					CurrentNewNode.NodeParts.append(NewPart)
					self.PartsLeft.remove(Part.id) # Remove element from the list todo
					# Lauch recursively the function using the new node
					self.ReRoot_DeclareParts(Part, NewPart)
				if Part.IsLeaf() : 
					# Register Leaf to new Tree Parts
					NewPart = Leaf(len(self.TempoTreeParts), CurrentNewNode, Part.LabelData, Part.BranchLen+Adding, Part.local, Part.Color, Part.Active)
					self.TempoTreeParts.append(NewPart)
					# Register Leaf to it's parent Node
					CurrentNewNode.NodeParts.append(NewPart)
					self.PartsLeft.remove(Part.id) # Remove element from the list todo
	def SwapNode(self, ChosenNode, Recursive = False):
		if not Recursive:
			if ChosenNode.IsNode():#Check if the chosen part for swap is a Node
				if len(ChosenNode.NodeParts)!=3 :#Check if the chosen Node is not a trifurcation
					# Go throught the whole Current structure, copy only while getting to the node to swap 
					self.TempoTreeParts = []
					self.PartsLeft = []
					for Part in self.TreeParts:
						self.PartsLeft.append(Part.id)
					if self.showlog: print "Creating New Root"
					self.TempoTreeParts.append(Node(0, False))
					CurrentNewNode = self.TempoTreeParts[0]
					# Copying the structure
					self.ReRoot_DeclareParts(self.TreeParts[0], CurrentNewNode)
					# Replace the Old structure by the new One
					self.TreeParts = self.TempoTreeParts
					
					self.TempoTreeParts = []
					PartToSwap = self.FindAPartByID(ChosenNode.id)
					if PartToSwap:
						#Inverting the node
						PartToSwap.NodeParts.reverse()
						# Placing the Root and Launching the Recurssive loop
						self.TempoTreeParts.append(self.TreeParts[0])
						self.SwapNode(self.TreeParts[0], True)
						# Store the New structure
						if self.showlog: print "storing Structure"
						self.TreeStructures.append(self.TempoTreeParts)
						# Replace the Old structure by the new One
						self.TreeParts = self.TempoTreeParts
						# Renumbering the Ids
						for i, part in enumerate(self.TreeParts):
							part.id = i
						if self.showlog: print "Swap Done"
						#Redrawing
						self.RecursiveDrawLoop()
						if self.showlog: print "ReDraw Done"
					else:
						# Restore the new Structure
						if self.showlog: print "Id not Found"
						pass
		else:# Recurssive part of the Function
			for Part in ChosenNode.NodeParts:
				if Part.IsNode():
					self.TempoTreeParts.append(Part)
					self.SwapNode(Part, True)
				if Part.IsLeaf():
					self.TempoTreeParts.append(Part)
	# Drawing method
	def RecursiveDrawLoop(self, CurrentPart=False):
		if not CurrentPart : 
			self.CurrentLine = self.RatioY/2
			self.Elem2Ligne = range(self.NumberOfParts())
			self.segments = []
			CurrentPart = self.TreeParts[0]
			
			self.PartsLeft = []
			for Part in self.TreeParts:
				self.PartsLeft.append(Part.id)
			
		if self.showlog: print '-----------open node '+str(CurrentPart.id)
		for Part in CurrentPart.NodeParts:
			if Part.IsLeaf(): # If Part is a leaf
				if self.showlog: print "Part is a Leaf ",Part.id, Part.LabelData.Label
				self.PartsLeft.remove(Part.id) # Remove element from the list todo
				self.Elem2Ligne[Part.id]=self.CurrentLine # Link the current element to it's line
				# Draw the segment
				new_segment = DrawSegment( Part, 0, self.CurrentLine, Part.BranchLen, self.CurrentLine, False, True)
				self.segments.append(new_segment)
				# Set x and y to the Part
				Part.x = Part.BranchLen
				Part.y = self.CurrentLine
				
				if self.showlog: print 'self.CurrentLine: ', self.CurrentLine
				self.CurrentLine += self.RatioY # On deplace la ligne courante
				
			if Part.IsNode(): # If Part is a Node
				self.RecursiveDrawLoop(Part)
			
			#Should we close a Node ???
			ClosingTest = True
			for SubPart in Part.parent.NodeParts:
				if SubPart.id in self.PartsLeft : ClosingTest = False
			if ClosingTest and len(self.PartsLeft)>0: # If all element in node were drawn -> Closing node
				if self.showlog: print '-----------Closing node ', Part.parent.id
				self.PartsLeft.remove(Part.parent.id)
				
				TopLine = 1
				BtmLine = 0
				for SubPart in Part.parent.NodeParts: # Review all subparts from parent node to get TopLine and BtmLine
					if self.Elem2Ligne[SubPart.id]<TopLine: TopLine = self.Elem2Ligne[SubPart.id]
					if self.Elem2Ligne[SubPart.id]>BtmLine: BtmLine = self.Elem2Ligne[SubPart.id]
						
				LigneForNode = (TopLine+BtmLine)/2 # Calcule la ligne ou placer le noeud
				
				TopLimit = 1
				BtmLimit = 0
				for SubPart in self.GetAllPartsOfNode(Part.parent,[]): # Review all sibblings from parent node to get TopLimit and BtmLimit
					if self.Elem2Ligne[SubPart.id]<TopLimit: TopLimit = self.Elem2Ligne[SubPart.id]
					if self.Elem2Ligne[SubPart.id]>BtmLimit: BtmLimit = self.Elem2Ligne[SubPart.id]
				
				# Write vertical segment
				new_segment = DrawSegment(Part, 0, TopLine, 0, BtmLine, True, False)
				self.segments.append(new_segment)
				
				# Write Node Label (support)
				# if str(Part.parent.support).find('.')>0 and int(Part.parent.support)<2: support = str(int(Part.parent.support*100))
				# else: support = str(int(Part.parent.support))
				#Set 
				Part.parent.x = 0
				Part.parent.y = LigneForNode
				
				# Shift segments coordinates
				for segment in self.segments :
					if segment.yfrom >= TopLimit and segment.yfrom <= BtmLimit :
						segment.xfrom += Part.parent.BranchLen
						segment.xto += Part.parent.BranchLen
				
				# Shift Labels coordinates
				for label in self.TreeParts :
					if label.y >= TopLimit and label.y <= BtmLimit :
						label.x += Part.parent.BranchLen
				
				self.Elem2Ligne[Part.parent.id]=LigneForNode # Creer la correspondence de elem to ligne
				
				# Write horizontal segment
				new_segment = DrawSegment(Part, 0, LigneForNode, Part.parent.BranchLen, LigneForNode, False, True)
				self.segments.append(new_segment)
		
		if len(self.PartsLeft) == 0:
			self.LongestSegment = self.UpdateLongestSegment()
			
	# Text Based Output methods
	def DrawInText(self, LenX=40, LenY=2):
		# Create the string array for lines
		LinesArray = []
		RatioX = float(LenX)/self.LongestSegment[0]
		i = self.NumberOfLeaf()*LenY
		while i > 0:  
			LinesArray.append([])
			i-=1
		for lines in LinesArray:
			for x in range(LenX+1):
				lines.append(' ')
		# start with horizontal segments
		for segment in self.segments:
			if segment.horizontal:# Is it an H segment
				for Xpos in range(int(RatioX*segment.xfrom),int(RatioX*segment.xto)+1):
					LinesArray[int(segment.yfrom*self.NumberOfLeaf()*LenY)][Xpos]='-'
		# Continue with vertical segments
		for segment in self.segments:
			if segment.vertical:# Is it an V segment
				xPos = int(RatioX*segment.xfrom)
				linefrom = int(segment.yfrom*self.NumberOfLeaf()*LenY)
				# upCorner :
				LinesArray[linefrom][xPos]='+'
				linefrom += 1
				# InBetween:
				while linefrom < int(segment.yto*self.NumberOfLeaf()*LenY):
					LinesArray[linefrom][xPos]='|'
					linefrom += 1
				# btmCorner :
				LinesArray[linefrom][xPos]='+'
		
		for label in self.TreeParts:
			if label.IsNode():
				for char in range(len(str(label.support))):
					LinesArray[int(label.y*self.NumberOfLeaf()*LenY)][int(RatioX*label.x)+char]=str(label.support)[char]
		
		for line in LinesArray:
			xPos = len(line)-1
			while line[xPos]==' ' and xPos>0:
				line[xPos]=''
				xPos -= 1
		for label in self.TreeParts:
			if label.IsLeaf():LinesArray[int(label.y*self.NumberOfLeaf()*LenY)].append(str(label.LabelData.Label))
		
		Output = ''
		for line in LinesArray:
			Output += ''.join(line) +"\n"
		return Output
	def DrawInHTML(self, LenX=40, LenY=2):
		# Create the string array for lines
		LinesArray = []
		RatioX = float(LenX)/self.GetLongestSegment()[0]
		i = self.NumberOfLeaf()*LenY
		while i > 0:  
			LinesArray.append([])
			i-=1
		for lines in LinesArray:
			for x in range(LenX+1):
				lines.append(' ')
		# start with horizontal segments
		for segment in self.segments:
			if segment.horizontal:# Is it an H segment
				for Xpos in range(int(RatioX*segment.xfrom),int(RatioX*segment.xto)+1):
					LinesArray[int(segment.yfrom*self.NumberOfLeaf()*LenY)][Xpos]='-'
		# Continue with vertical segments
		for segment in self.segments:
			if segment.vertical:# Is it an V segment
				xPos = int(RatioX*segment.xfrom)
				linefrom = int(segment.yfrom*self.NumberOfLeaf()*LenY)
				# upCorner :
				LinesArray[linefrom][xPos]='£'
				linefrom += 1
				# InBetween:
				while linefrom < int(segment.yto*self.NumberOfLeaf()*LenY):
					LinesArray[linefrom][xPos]='|'
					linefrom += 1
				# btmCorner :
				LinesArray[linefrom][xPos]='µ'
		
		for Part in self.TreeParts:
			if Part.IsNode():
				for char in range(len(str(Part.support))):
					LinesArray[int(Part.y*self.NumberOfLeaf()*LenY)][int(RatioX*Part.x)+char]=str(Part.support)[char]
		
		for line in LinesArray:
			xPos = len(line)-1
			while line[xPos]==' ' and xPos>0:
				line[xPos]=''
				xPos -= 1
				
		for Part in self.TreeParts:
			if Part.IsLeaf():LinesArray[int(Part.y*self.NumberOfLeaf()*LenY)].append(str(Part.LabelData.Label))
		
		Output = '<HTML>\n<BODY STYLE="font-size:12px;font-family:Courier New;line-height:14px;">\n'
		for line in LinesArray:
			currLine = ''.join(line)
			currLine = currLine.replace(' ','&nbsp;') # espaces
			currLine = currLine.replace('|','&#9474;') # vertical bar
			currLine = currLine.replace('£','&#9484;') # coin haut
			currLine = currLine.replace('µ','&#9492;') # coin bas
			currLine = currLine.replace('-','&#9472;') # honrizontal bar
			Output += currLine+"<br>\n"
		Output += "</BODY>\n</HTML>"
		return Output
	
	def PrintInNewick(self, CurrentPart=False):
		# Recursive loop to get a Newick string from the tree Object
		if not CurrentPart : 
			CurrentPart = self.TreeParts[0]
			self.PartsLeft = []
			for Part in self.TreeParts:
				self.PartsLeft.append(Part.id)
		self.NodeWasJustClosed = False
		GrowingText = '('
		if self.showlog: print 'open node '+str(CurrentPart.id)+" - just-close="+str(self.NodeWasJustClosed)
		for Part in CurrentPart.NodeParts:
			# If part is a Leaf
			if Part.IsLeaf():
				self.PartsLeft.remove(Part.id)
				if self.NodeWasJustClosed: GrowingText +=','
				GrowingText += str(Part.LabelData.Label)+":"+str(Part.BranchLen)
			
			# If part is a Node
			if Part.IsNode():
				if self.NodeWasJustClosed: GrowingText +=','
				GrowingText += self.PrintInNewick(Part)
			
			# Shoul we close a node ???
			ClosingTest = True
			for subPart in Part.parent.NodeParts:
				if subPart.id in self.PartsLeft : ClosingTest = False
			if ClosingTest and len(self.PartsLeft)>0:
				if self.showlog: print 'close node '+str(CurrentPart.id)+" - just-close="+str(self.NodeWasJustClosed)
				self.PartsLeft.remove(Part.parent.id)
				GrowingText +=')'+str(Part.parent.support)+":"+str(Part.parent.BranchLen)
				self.NodeWasJustClosed = True
			if not ClosingTest and Part.IsLeaf():
				GrowingText +=','
			if len(self.PartsLeft)==0:
				GrowingText +=';'
		return GrowingText.replace(',,',',')
	
	def ActivateOrDeactivatePart(self, Part):
		are_on = 0
		are_off = 0
		ModifiedLeafs = []
		#Part is a Leaf
		if Part.IsLeaf():
			if Part.IsActive(): Part.Active = False
			else: Part.Active = True
			ModifiedLeafs.append(Part)
		#Part is a Node
		if Part.IsNode():
			for Leaf in self.GetAllLeafsOfNode(Part):
				if Leaf.IsActive(): are_on += 1
				else: are_off += 1
				ModifiedLeafs.append(Leaf)
			if are_off == 0:
				for Leaf in self.GetAllLeafsOfNode(Part): Leaf.Active = False
			elif are_on == 0:
				for Leaf in self.GetAllLeafsOfNode(Part): Leaf.Active = True
			else :
				if are_on > are_off :
					for Leaf in self.GetAllLeafsOfNode(Part): Leaf.Active = False
				if are_off >= are_on :
					for Leaf in self.GetAllLeafsOfNode(Part): Leaf.Active = True
		return ModifiedLeafs
				
	def UpdateLongestSegment(self, dc=False, width=1000):
		if not dc:
			LongestSegmentSize = 0
			LongestSegmentPart = ''
			LongestFullSegment = 0
			for leaf in self.GetAllLeafList():
				if leaf.x*1000 + len(leaf.LabelData.LabelToWrite)*10 > LongestFullSegment:
					LongestSegmentSize = leaf.x
					LongestSegmentPart = leaf
					LongestFullSegment = leaf.x*1000 + len(leaf.LabelData.LabelToWrite)*10
			return (LongestSegmentSize, LongestSegmentPart)
		else:
			LongestSegmentSize = 0
			LongestSegmentPart = ''
			LongestFullSegment = 0
			for leaf in self.GetAllLeafList():
				if leaf.x*width + dc.GetTextExtent(leaf.LabelData.LabelToWrite)[0] > LongestFullSegment:
					LongestSegmentSize = leaf.x
					LongestSegmentPart = leaf
					LongestFullSegment = leaf.x*width + dc.GetTextExtent(leaf.LabelData.LabelToWrite)[0]
			print LongestSegmentPart.LabelData.LabelToWrite
			return (LongestSegmentSize, LongestSegmentPart)
			
	
	# Apply Configartion modification on tree
	def ApplyConfigurator(self):
		for Leaf in self.GetAllLeafList():
			LocalID = False
			# Getting Data from DB
			if self.Configurator.UseDB:
				try :
					LocalID = Leaf.LabelData.Label.split('@')[1].split('|')[0]
					if LocalID:
						# Get everything, Draw data and update will cope with display options
						try : 
							QueryLine = "SELECT gi, ref, manualdef, def " 
							QueryLine+= "FROM aaseqs WHERE aaseqs.localid="+str(LocalID)
							query = self.Configurator.db.query(QueryLine)
							if query.ntuples()>0:
								data = query.dictresult()[0]
								if len(str(data['manualdef']))>5 : Leaf.LabelData.Definition = data['manualdef'][:50]
								else : Leaf.LabelData.Definition = str(data['def'])[:50]
								Leaf.LabelData.GI = str(data['gi'])
								Leaf.LabelData.Accession = str(data['ref'])
								Leaf.LabelData.LocalID = LocalID
								Leaf.local = True
						except :
								print "can't get data"+str(Leaf.LabelData.Label)
								print "-"+str(LocalID)+"-"
								print QueryLine
								pass
						try :
							QueryLine = "SELECT usergroups.groupid, usergroups.groupname FROM usergroups "
							QueryLine+= "LEFT JOIN seq2usergroups ON seq2usergroups.groupid=usergroups.groupid "
							QueryLine+= "WHERE seq2usergroups.seqid="+str(LocalID)
							query = self.Configurator.db.query(QueryLine)
							if query.ntuples()>0:
								for data in query.dictresult():
									group = GroupDataObj(data['groupid'], data['groupname'])
									Leaf.LabelData.Groups.append(group)
						except :
							print "can't get groups"+str(Leaf.LabelData.Label)
							pass
				except :
					print "can't get local id"+str(Leaf.LabelData.Label)
					pass
			# Getting Colors
			if self.Configurator.UseColors:
				SplitedTaxon = Leaf.LabelData.Label.replace('_',' ').split('@')[0].split(' ')
				try : tax_id = self.Configurator.Names[' '.join(SplitedTaxon)]
				except: tax_id = False
				if not tax_id:
					try : tax_id = self.Configurator.Names[SplitedTaxon[:2]]
					except: tax_id = False
				if not tax_id:
					try : tax_id = self.Configurator.Names[SplitedTaxon[:1]]
					except: tax_id = False
				if not tax_id:
					try : tax_id = self.Configurator.Names[SplitedTaxon[0]]
					except: tax_id = False
				if not tax_id:
					try : tax_id = self.Configurator.Names[SplitedTaxon[1:2]]
					except: tax_id = False
				if tax_id:
					while tax_id not in self.Configurator.MyColors: tax_id = self.Configurator.Nodes[tax_id]
					Leaf.Color = self.Configurator.MyColors[tax_id][1]
				## A enlever si pas pour Evoldeep 
				else :
					if SplitedTaxon[0] == "Evoldeep":
						Leaf.Color = self.Configurator.MyColors['0'][1]
				
	def UpdateLeafConfiguration(self):
		for Leaf in self.GetAllLeafList():
			if self.Configurator.UseDB:
				Leaf.LabelData.LabelToWrite = Leaf.LabelData.Label 
				if self.Configurator.ShowDefinitions: 
					Leaf.LabelData.LabelToWrite += " | "+Leaf.LabelData.Definition
				if self.Configurator.ShowGI:
					Leaf.LabelData.LabelToWrite += " | "+Leaf.LabelData.GI
				if self.Configurator.ShowRef:
					Leaf.LabelData.LabelToWrite += " | "+Leaf.LabelData.Accession
				if self.Configurator.ShowGroups:
					for group in Leaf.LabelData.Groups:
						Leaf.LabelData.LabelToWrite += ", "+group.GroupName
				
	
	
