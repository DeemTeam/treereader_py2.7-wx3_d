#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import wx, os, math, DbClass
import BioClasses.Trees as Trees #import Trees
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4 as A4size
from reportlab.lib.colors import HexColor

class MyTreeFrame(wx.Frame):
	def __init__(self, parent, config_dict, TreeFile, TreeFileName):
		wx.Frame.__init__(self, parent, -1, TreeFile ,wx.DefaultPosition, (1000,700), wx.DEFAULT_FRAME_STYLE | wx.CLIP_CHILDREN | wx.FRAME_FLOAT_ON_PARENT)
		self.SetMinSize(wx.Size(1000,700))
		
		# Opening tree File
		ifObj = open(TreeFile,'r')
		all_text = ''
		for line in ifObj: 
			if len(line.strip(' \r\n'))!=0 : all_text += line.strip(' \r\n').replace(';',';�')
		ifObj.close()
		self.TreesList = all_text.split('�')
		if self.TreesList.count('') : self.TreesList.remove('')
		
		# Variable for operation
		self.config_dict = config_dict
		self.TreeFileName = TreeFileName
		# Tree Interaction
		
		# Tree Browsing
		self.CurrentTreeNum = 1
		
		self.CurrentTreeObj = Trees.Arbre(self.TreesList[self.CurrentTreeNum-1],self.config_dict)
		
		# Drawing the frame
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		self.LeftPanel = TunningPanel(self)
		self.RightPanel = MyTreeWindow(self)
		
		H_Sizer.Add(self.LeftPanel, 0, wx.EXPAND)
		H_Sizer.Add(self.RightPanel, 1, wx.EXPAND|wx.ALL, 0)
		
		self.SetSizer(H_Sizer)
	def SwitchTree(self, Action):
		ExTreeNum = self.CurrentTreeNum
		if Action == "First":
			self.CurrentTreeNum = 1
		elif Action == "Back":
			if self.CurrentTreeNum - 1 >= 1 : self.CurrentTreeNum -= 1
		elif Action == "Fwd":
			if self.CurrentTreeNum + 1 <= len(self.TreesList) : self.CurrentTreeNum += 1
		elif Action == "Last":
			self.CurrentTreeNum = len(self.TreesList)
		else:
			if  1 < int(Action) < len(self.TreesList) : self.CurrentTreeNum = int(Action)
			else : 
				Errordlg = wx.MessageDialog(self, 'Number is out of range','Error',wx.OK | wx.ICON_ERROR)
				Errordlg.ShowModal()
				Errordlg.Destroy()
				self.LeftPanel.CurrentTreeNumber.SetValue(str(self.CurrentTreeNum))
		if ExTreeNum != self.CurrentTreeNum:
			self.ReloadTree()
			self.RefreshTreePanel()
	def ReloadTree(self):
		self.CurrentTreeObj = Trees.Arbre(self.TreesList[self.CurrentTreeNum-1],self.config_dict)
	def RefreshTreePanel(self):
		self.LeftPanel.CurrentTreeNumber.SetValue(str(self.CurrentTreeNum))
		self.RightPanel.DrawTree()
		self.RightPanel.SetFocus()

class TunningPanel(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent, -1, wx.DefaultPosition, (150,700))
		# IDs Generation
		ID_SPIN_LEAVES_FONT = wx.NewId()
		ID_SPIN_NODES_FONT = wx.NewId()
		ID_FirstBtn = wx.NewId()
		ID_BackBtn = wx.NewId()
		ID_FwdBtn = wx.NewId()
		ID_LastBtn = wx.NewId()
		ID_LeftClickAction = wx.NewId()
		ID_RefreshBtn = wx.NewId()
		ID_SaveFastaBtn = wx.NewId()
		ID_CreaGroupBtn = wx.NewId()
		ID_LinkGroupBtn = wx.NewId()
		ID_CreaPhyloEvtBtn = wx.NewId()
		ID_LinkPhyloEvtBtn = wx.NewId()
		ID_SaveNewickBtn = wx.NewId()
		ID_SavePDFBtn = wx.NewId()
		
		# Variable for operation
		self.RefreshNeeded = False
		
		V_Sizer = wx.BoxSizer(wx.VERTICAL)
		TitleText = wx.StaticText(self, label = "Browse Trees", style = wx.ALIGN_CENTRE)
		TitleText.SetFont(wx.Font(12, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
		V_Sizer.Add(TitleText, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		V_Sizer.Add(wx.StaticLine(self, -1, (-1,-1), (140,1), wx.HORIZONTAL), 0, wx.ALIGN_CENTER)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		FirstBtn = wx.BitmapButton(self, ID_FirstBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "FirstBtn.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		BackBtn = wx.BitmapButton(self, ID_BackBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "BackBtn.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		FwdBtn = wx.BitmapButton(self, ID_FwdBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "FwdBtn.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		LastBtn = wx.BitmapButton(self, ID_LastBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "LastBtn.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		H_Sizer.Add(FirstBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(BackBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(FwdBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(LastBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		self.CurrentTreeNumber = wx.TextCtrl(self, -1, str(self.GetParent().CurrentTreeNum), size=(50, -1), style=wx.TE_RIGHT|wx.TE_PROCESS_ENTER)
		TotalTreeNumber = wx.StaticText(self, label = " on "+str(len(self.GetParent().TreesList)))
		H_Sizer.Add(self.CurrentTreeNumber, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(TotalTreeNumber, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		V_Sizer.AddSpacer(10)
		
		TitleText = wx.StaticText(self, label = "Fonts", style = wx.ALIGN_CENTRE)
		TitleText.SetFont(wx.Font(12, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
		V_Sizer.Add(TitleText, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		V_Sizer.Add(wx.StaticLine(self, -1, (-1,-1), (140,1), wx.HORIZONTAL), 0, wx.ALIGN_CENTER)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		FontTitleText = wx.StaticText(self, label = "Leaves Font Size")
		FontTitleText.SetFont(wx.Font(10, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
		H_Sizer.Add(FontTitleText, 0, wx.ALIGN_LEFT|wx.ALL, 1)
		self.LeafFontSpin = wx.SpinCtrl(self, ID_SPIN_LEAVES_FONT, size=(45,18))
		self.LeafFontSpin.SetRange(6,16)
		self.LeafFontSpin.SetValue(self.GetParent().config_dict['LeafFont'].GetPointSize())
		H_Sizer.Add(self.LeafFontSpin, 0, wx.ALIGN_RIGHT|wx.ALL, 1)
		V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		FontTitleText = wx.StaticText(self, label = "Nodes Font Size")
		FontTitleText.SetFont(wx.Font(10, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
		H_Sizer.Add(FontTitleText, 0, wx.ALIGN_LEFT|wx.ALL, 1)
		self.NodesFontSpin = wx.SpinCtrl(self, ID_SPIN_NODES_FONT, size=(45,18))
		self.NodesFontSpin.SetRange(6,14)
		self.NodesFontSpin.SetValue(self.GetParent().config_dict['NodeFont'].GetPointSize())
		H_Sizer.Add(self.NodesFontSpin, 0, wx.ALIGN_RIGHT|wx.ALL, 1)
		V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		V_Sizer.AddSpacer(10)
		
		#------ Saving and printing part
		TitleText = wx.StaticText(self, label = "Save and Print", style = wx.ALIGN_CENTRE)
		TitleText.SetFont(wx.Font(12, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
		V_Sizer.Add(TitleText, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		V_Sizer.Add(wx.StaticLine(self, -1, (-1,-1), (140,1), wx.HORIZONTAL), 0, wx.ALIGN_CENTER)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		SaveNewickBtn = wx.BitmapButton(self, ID_SaveNewickBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "SaveNewick.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		SavePDFBtn = wx.BitmapButton(self, ID_SavePDFBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "SavePDF.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		
		H_Sizer.Add(SaveNewickBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(SavePDFBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 0)
		
		
		if self.GetParent().config_dict['UseDB']:
			V_Sizer.AddSpacer(10)
			
			TitleText = wx.StaticText(self, label = "Database", style = wx.ALIGN_CENTRE)
			TitleText.SetFont(wx.Font(12, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
			V_Sizer.Add(TitleText, 0, wx.ALIGN_CENTER|wx.ALL, 1)
			V_Sizer.Add(wx.StaticLine(self, -1, (-1,-1), (140,1), wx.HORIZONTAL), 0, wx.ALIGN_CENTER)
			
			self.DefinitionsCB = wx.CheckBox(self, -1, "Show Definitions")
			self.GroupsCB = wx.CheckBox(self, -1, "Show Groups")
			
			V_Sizer.AddSpacer(5)
			V_Sizer.Add(self.DefinitionsCB, 0, wx.ALIGN_LEFT|wx.LEFT, 15)
			V_Sizer.AddSpacer(5)
			V_Sizer.Add(self.GroupsCB, 0, wx.ALIGN_LEFT|wx.LEFT, 15)
			
			H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
			RefreshBtn = wx.BitmapButton(self, ID_RefreshBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "Refresh.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
			SaveFastaBtn = wx.BitmapButton(self, ID_SaveFastaBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "SaveFasta.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
			
			H_Sizer.Add(RefreshBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
			H_Sizer.Add(SaveFastaBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
			
			V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 0)
			
			H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
			CreaGroupBtn = wx.BitmapButton(self, ID_CreaGroupBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "database_process.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
			LinkGroupBtn = wx.BitmapButton(self, ID_LinkGroupBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "database_add.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
			H_Sizer.Add(CreaGroupBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
			H_Sizer.Add(LinkGroupBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
			
			#CreaPhyloEvtBtn = wx.BitmapButton(self, ID_CreaPhyloEvtBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "database_process_evt.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
			LinkPhyloEvtBtn = wx.BitmapButton(self, ID_LinkPhyloEvtBtn, wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "database_add_evt.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
			
			#H_Sizer.Add(CreaPhyloEvtBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
			H_Sizer.Add(LinkPhyloEvtBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
			
			V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 0)
			
			# Event Bindings For DB interaction 
			self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.DefinitionsCB)
			self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.GroupsCB)
			self.Bind(wx.EVT_BUTTON, self.OnRefreshBtn, RefreshBtn)
			self.Bind(wx.EVT_BUTTON, self.OnSaveFastaBtn, SaveFastaBtn)
			
			self.Bind(wx.EVT_BUTTON, self.OnCreaGroupBtn, CreaGroupBtn)
			self.Bind(wx.EVT_BUTTON, self.OnLinkGroupBtn, LinkGroupBtn)
			
			#self.Bind(wx.EVT_BUTTON, self.OnCreaPhyloEvtBtn, CreaPhyloEvtBtn)
			self.Bind(wx.EVT_BUTTON, self.OnLinkPhyloEvtBtn, LinkPhyloEvtBtn)
			
		self.SetSizer(V_Sizer)
		
		#Event Bindings
		self.Bind(wx.EVT_TEXT_ENTER, self.OnCurrentTreeNumberEnter, self.CurrentTreeNumber)
		
		self.Bind(wx.EVT_BUTTON, self.OnFirstBtn, id=ID_FirstBtn)
		self.Bind(wx.EVT_BUTTON, self.OnBackBtn, id=ID_BackBtn)
		self.Bind(wx.EVT_BUTTON, self.OnFwdBtn, id=ID_FwdBtn)
		self.Bind(wx.EVT_BUTTON, self.OnLastBtn, id=ID_LastBtn)
		
		self.Bind(wx.EVT_SPINCTRL, self.OnNodesFontSpin, id=ID_SPIN_NODES_FONT)
		self.Bind(wx.EVT_SPINCTRL, self.OnLeafFontSpin, id=ID_SPIN_LEAVES_FONT)
		
		self.Bind(wx.EVT_BUTTON, self.OnSaveNewickBtn, SaveNewickBtn)
		self.Bind(wx.EVT_BUTTON, self.OnSavePDFBtn, SavePDFBtn)
		
	# Saving and printing
	def OnSaveNewickBtn(self, event):
		dlg = wx.FileDialog(self, "Save Tree in Newick Format", wildcard = "ph Phylogenetic Tree (*.ph)|*.ph|tre Phylogenetic Tree (*.tre)|*.tre", style = wx.SAVE|wx.OVERWRITE_PROMPT)
		dlg.SetFilename(self.GetParent().TreeFileName.split('.')[0])
		DlgResponse = dlg.ShowModal()
		FilePath = dlg.GetPath()
		FileName = dlg.GetFilename()
		dlg.Destroy()
		if DlgResponse == wx.ID_OK and FileName != "": 
			try :
				NewickString = self.GetParent().CurrentTreeObj.PrintInNewick()
				ofObj = open(FilePath,"w")
				ofObj.write(NewickString)
				ofObj.close()
			except:
				Errordlg = wx.MessageDialog(self, 'Unable to save file','Error',wx.OK | wx.ICON_ERROR)
				Errordlg.ShowModal()
				Errordlg.Destroy()
		
	def OnSavePDFBtn(self, event):
		dlg = wx.FileDialog(self, "Save Tree in PDF", wildcard = "PDF file (*.pdf)|*.pdf", style = wx.SAVE|wx.OVERWRITE_PROMPT)
		dlg.SetFilename(self.GetParent().TreeFileName.split('.')[0])
		DlgResponse = dlg.ShowModal()
		FilePath = dlg.GetPath()
		FileName = dlg.GetFilename()
		dlg.Destroy()
		if DlgResponse == wx.ID_OK and FileName != "": 
			#try :
			NodeFontHeight = 8
			LeafFontHeight = 10
			LeafSeparation = 1
			Margin = 20
			SepFromLine = 2
			PdfCanvas = canvas.Canvas(FilePath, pagesize=A4size)
			Width, Height = A4size
			PdfCanvas.setFont("Helvetica", LeafFontHeight)
			RatioX = (Width - Margin*2 - SepFromLine - PdfCanvas.stringWidth(self.GetParent().CurrentTreeObj.LongestSegment[1].label)) / self.GetParent().CurrentTreeObj.LongestSegment[0]
			ShiftYLeaf = LeafFontHeight/3.0
			ShiftYNode = NodeFontHeight/3.0
			
			NbPDFPages = math.ceil(self.GetParent().CurrentTreeObj.NumberOfLeaf() * (LeafFontHeight + LeafSeparation) / (Height - Margin*2))
			CurrentPDFPage = 0
			PageUpLimit = 1.0/NbPDFPages
			PageDownLimit = 0
			while CurrentPDFPage < NbPDFPages:
				UppestLeafY = 0.0
				#Get the modified value of the last leaf to draw
				for label in self.GetParent().CurrentTreeObj.TreeParts:
					if label.IsLeaf() and PageDownLimit <= label.y <= PageUpLimit:
						if label.y - PageDownLimit > UppestLeafY : UppestLeafY = label.y - PageDownLimit
				
				for segment in self.GetParent().CurrentTreeObj.segments:
					if PageDownLimit <= segment.yfrom <= PageUpLimit or PageDownLimit <= segment.yto <= PageUpLimit: 
						PdfCanvas.line	(
										segment.xfrom * RatioX + Margin,
										(1 - ((segment.yfrom - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin,
										segment.xto * RatioX + Margin,
										(1 - ((segment.yto - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin
										)
					elif PageDownLimit >= segment.yfrom and segment.yto >= PageUpLimit:
						PdfCanvas.line	(
										segment.xfrom * RatioX + Margin,
										(1 - ((segment.yfrom - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin,
										segment.xto * RatioX + Margin,
										(1 - ((segment.yto - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin
										)
				for label in self.GetParent().CurrentTreeObj.TreeParts:
					if label.IsLeaf():
						if PageDownLimit <= label.y <= PageUpLimit : 
							PdfCanvas.setFont("Helvetica", LeafFontHeight)
							if label.color:
								PdfCanvas.setFillColor(HexColor(label.color[0].GetAsString(wx.C2S_HTML_SYNTAX)))
								PdfCanvas.rect(
												label.x * RatioX + Margin + SepFromLine,
												(1 - ((label.y - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin - 2*ShiftYLeaf,
												PdfCanvas.stringWidth(str(label.label)),
												LeafFontHeight + ShiftYLeaf,
												stroke=0, fill=1
												)
								PdfCanvas.setFillColor(HexColor(label.color[1].GetAsString(wx.C2S_HTML_SYNTAX)))
							else : PdfCanvas.setFillColorRGB(0,0,0)
							PdfCanvas.drawString(
												label.x * RatioX + Margin + SepFromLine,
												(1 - ((label.y - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin - ShiftYLeaf,
												str(label.label)
												)
				for label in self.GetParent().CurrentTreeObj.TreeParts:
					if label.IsNode():
						if PageDownLimit <= label.y <= PageUpLimit : 
							PdfCanvas.setFont("Helvetica", NodeFontHeight)
							PdfCanvas.setFillColorRGB(0,0,0)
							PdfCanvas.drawString(
												label.x * RatioX + Margin + SepFromLine,
												(1 - ((label.y - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin - ShiftYNode,
												str(label.support)
												)
				PdfCanvas.showPage()
				CurrentPDFPage += 1
				PageUpLimit += 1.0/NbPDFPages
				PageDownLimit += 1.0/NbPDFPages
				
			PdfCanvas.save()
				
			# except:
				# Errordlg = wx.MessageDialog(self, 'Unable to save file','Error',wx.OK | wx.ICON_ERROR)
				# Errordlg.ShowModal()
				# Errordlg.Destroy()
		
	def EvtCheckBox(self, event):
		if self.DefinitionsCB.GetValue(): self.GetParent().config_dict['ShowDefinitions']=True
		else : self.GetParent().config_dict['ShowDefinitions']=False
		if self.GroupsCB.GetValue(): self.GetParent().config_dict['ShowGroups']=True
		else : self.GetParent().config_dict['ShowGroups']=False
		self.RefreshNeeded = True
		
	def OnRefreshBtn(self, event):
		if self.RefreshNeeded:
			self.GetParent().ReloadTree()
			self.GetParent().RefreshTreePanel()
			self.RefreshNeeded = False
		
	def OnSaveFastaBtn(self, event):
		dlg = wx.FileDialog(self, "Save Selected sequences in Fasta", wildcard = "Fasta files (*.fas)|*.fas", style = wx.SAVE|wx.OVERWRITE_PROMPT)
		dlg.SetFilename(self.GetParent().TreeFileName.split('.')[0])
		DlgResponse = dlg.ShowModal()
		FilePath = dlg.GetPath()
		FileName = dlg.GetFilename()
		dlg.Destroy()
		if DlgResponse == wx.ID_OK and FileName != "": 
			try :
				ofObj = open(FilePath,"w")
				for Leaf in self.GetParent().CurrentTreeObj.GetActiveLeafs():
					SeqId = Leaf.split('@')[1].split('|')[0].split(' ')[0]
					query = self.GetParent().config_dict['db'].query("SELECT aaseqs.localid,aaseqs.aa,sp.spname FROM aaseqs LEFT JOIN sp ON sp.spid=aaseqs.idsp WHERE aaseqs.localid="+str(SeqId))
					data = query.dictresult()[0]
					ofObj.write(">"+str(data['spname'])+"@"+str(data['localid'])+"\n"+str(data['aa'])+"\n")
				ofObj.close()
			except:
				Errordlg = wx.MessageDialog(self, 'Unable to save file','Error',wx.OK | wx.ICON_ERROR)
				Errordlg.ShowModal()
				Errordlg.Destroy()
		
	def OnCreaGroupBtn(self, event):
		dlg = DbClass.DbNewGroupDialog(self, self.GetParent().config_dict['db'])
		if dlg.ShowModal() == wx.ID_OK:
			query = "SELECT groupid FROM usergroups WHERE groupname='"+dlg.ChosenName.GetValue()+"' AND parent='"+str(dlg.ChosenDir)+"'"
			GetExistingGpname = self.GetParent().config_dict['db'].query(query)
			if GetExistingGpname.ntuples()!=0:
				Errordlg = wx.MessageDialog(self, 'This name already exists','Error',wx.OK | wx.ICON_ERROR)
				Errordlg.ShowModal()
				Errordlg.Destroy()
			else:
				#print dlg.ChosenDir, dlg.ChosenName.GetValue() , dlg.ChosenNotes.GetValue()
				if dlg.ChosenDir != 0 and dlg.ChosenName.GetValue() != "":
					query = "INSERT INTO usergroups VALUES(DEFAULT,'"+dlg.ChosenName.GetValue()+"',1,'"+dlg.ChosenNotes.GetValue()+"','"+str(dlg.ChosenDir)+"')"
					self.GetParent().config_dict['db'].query(query)
		dlg.Destroy()
		
	def OnLinkGroupBtn(self, event):
		dlg = DbClass.DbGroupingDialog(self, self.GetParent().config_dict['db'])
		if dlg.ShowModal() == wx.ID_OK:
			if dlg.ChosenGroup != 0:
				for Leaf in self.GetParent().CurrentTreeObj.GetActiveLeafs():
					SeqId = Leaf.split('@')[1].split('|')[0].split(' ')[0]
					GetRegistered = self.GetParent().config_dict['db'].query("SELECT seq2usergroupsid FROM seq2usergroups WHERE seqid="+str(SeqId)+" AND groupid="+str(dlg.ChosenGroup))
					if GetRegistered.ntuples()==0:
						query = "INSERT INTO seq2usergroups VALUES(DEFAULT,'"+str(SeqId)+"','"+str(dlg.ChosenGroup)+"')"
						self.GetParent().config_dict['db'].query(query)
						self.RefreshNeeded = True
		dlg.Destroy()
		
	# def OnCreaPhyloEvtBtn(self, event):
		# pass
	def OnLinkPhyloEvtBtn(self, event):
		dlg = DbClass.DbNewPhyloEvtDialog(self, self.GetParent().config_dict['db'])
		if dlg.ShowModal() == wx.ID_OK:
			GroupsToActOn = []
			for GroupsChoice in dlg.ExistingGroupsChoice:
				if GroupsChoice.IsChecked() : GroupsToActOn.append(dlg.ExistingGroupsList[GroupsChoice.GetLabel()])
			if len(GroupsToActOn)>1:
				#print dlg.ChosenNotes.GetValue(), dlg.ChosenEvent
				query = "INSERT INTO phyloevents VALUES(DEFAULT,'"+str(dlg.ChosenEvent)+"','"+str(dlg.ChosenNotes.GetValue())+"') RETURNING evtid"
				insert_evt = self.GetParent().config_dict['db'].query(query)
				inserted_evt_id = insert_evt.dictresult()[0]['evtid']
				for group in GroupsToActOn:
					query = "INSERT INTO phyloevent2groups VALUES(DEFAULT,'"+str(inserted_evt_id)+"','"+str(group)+"')" 
					self.GetParent().config_dict['db'].query(query)
		dlg.Destroy()
		
	def OnFirstBtn(self, event):
		self.GetParent().SwitchTree("First")
	def OnBackBtn(self, event):
		self.GetParent().SwitchTree("Back")
	def OnFwdBtn(self, event):
		self.GetParent().SwitchTree("Fwd")
	def OnLastBtn(self, event):
		self.GetParent().SwitchTree("Last")
	def OnCurrentTreeNumberEnter(self, event):
		self.GetParent().SwitchTree(self.CurrentTreeNumber.GetValue())
	def OnNodesFontSpin(self, event):
		self.GetParent().config_dict['NodeFont'].SetPointSize(int(self.NodesFontSpin.GetValue()))
		self.GetParent().RefreshTreePanel()
	def OnLeafFontSpin(self, event):
		self.GetParent().config_dict['LeafFont'].SetPointSize(int(self.LeafFontSpin.GetValue()))
		self.GetParent().RefreshTreePanel()
		
		
class MyTreeWindow(wx.ScrolledWindow):
	def __init__(self, parent):
		wx.ScrolledWindow.__init__(self, parent, -1, wx.DefaultPosition, (850,700))
		
		#self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)
		self.SetScrollRate(20,20)
		self.SetBackgroundColour("WHITE")
		
		# Variables
		self.FirstOpen = False
		self.resized = False
		self.NodeBitmap = wx.Bitmap(os.path.join(self.GetParent().config_dict['ImgDir'], "NodeImg.png"), wx.BITMAP_TYPE_PNG)
		
		self.BlackColor = wx.Colour(0, 0, 0, wx.ALPHA_OPAQUE)
		self.WhiteColor = wx.Colour(255, 255, 255, wx.ALPHA_OPAQUE)
		self.RedColor = wx.Colour(255, 0, 0, wx.ALPHA_OPAQUE)
		
		self.Margin = 10
		self.SepFromLine = 5
		self.Height = 20
		self.Width = 20
		self.RatioX = 0
		self.ShiftYNode = 0
		self.ShiftYLeaf = 0
		self.buffer = wx.EmptyBitmap(self.Width,self.Height)
		
		# Events Binding
		self.Bind(wx.EVT_SIZE , self.OnSize)
		self.Bind(wx.EVT_PAINT, self.OnPaint)
		self.Bind(wx.EVT_ENTER_WINDOW, self.OnEnter)
		self.Bind(wx.EVT_LEFT_UP, self.OnLeftClick)
		self.Bind(wx.EVT_RIGHT_UP, self.OnRightClick)
		
	def OnPaint(self, event):
		dc = wx.BufferedPaintDC(self, self.buffer, wx.BUFFER_VIRTUAL_AREA)
		event.Skip()
	def OnSize(self, event):
		self.resized = True
		event.Skip()
	def OnEnter(self, event):
		if self.resized :
			self.DrawTree()
			self.resized = False
	
	def OnLeftClick(self, event):
		ModifiedLeafs = False
		ClickCoords = self.CalcUnscrolledPosition(event.GetX(), event.GetY())
		for part in self.GetParent().CurrentTreeObj.TreeParts: 
			if part.region.ContainsPoint(ClickCoords) == wx.InRegion: 
				ModifiedLeafs = self.GetParent().CurrentTreeObj.ActivateOrDeactivatePart(part)
		if ModifiedLeafs: self.UpdateLeafs(ModifiedLeafs)
	def OnRightClick(self, event):
		ModifiedLeafs = False
		ClickCoords = self.CalcUnscrolledPosition(event.GetX(), event.GetY())
		for part in self.GetParent().CurrentTreeObj.TreeParts: 
			if part.region.ContainsPoint(ClickCoords) == wx.InRegion: 
				TempPopUpMenu = DefaultTreePopUpMenu(self, part)
				self.PopupMenu(TempPopUpMenu)
				TempPopUpMenu.Destroy()
	def DrawTree(self):
		self.Refresh()
		
		self.buffer = wx.EmptyBitmap(self.Width,self.Height)
		dc = wx.BufferedDC(None, self.buffer)
		dc.SetFont(self.GetParent().config_dict['LeafFont'])
		
		# Width is always deduced from window
		self.Width = self.GetSizeTuple()[0]-20
		# Height is from window*Expansion Config
		self.Height = self.GetParent().config_dict['Expansion']*self.GetParent().CurrentTreeObj.NumberOfLeaf()*dc.GetTextExtent("A")[1] + (self.Margin * 2)
		
		self.buffer = wx.EmptyBitmap(self.Width,self.Height)
		# try: dc = wx.GCDC(wx.BufferedDC(None, self.buffer))
		# except: dc = wx.BufferedDC(None, self.buffer)
		
		dc = wx.BufferedDC(None, self.buffer)
		
		dc.SetFont(self.GetParent().config_dict['NodeFont'])
		self.ShiftYNode = dc.GetTextExtent("A")[1]/2
		dc.SetFont(self.GetParent().config_dict['LeafFont'])
		self.ShiftYLeaf = dc.GetTextExtent("A")[1]/2
		self.RatioX = (self.Width - self.Margin*2 - self.SepFromLine - dc.GetTextExtent(self.GetParent().CurrentTreeObj.LongestSegment[1].label)[0]) / self.GetParent().CurrentTreeObj.LongestSegment[0]
		
		dc.SetPen(wx.Pen(self.BlackColor))
		dc.SetBackground(wx.Brush(self.WhiteColor))
		dc.SetBackgroundMode(wx.SOLID)
		
		dc.Clear()
		#dc.StartDrawing()
		# start with horizontal segments
		for segment in self.GetParent().CurrentTreeObj.segments:
			dc.DrawLine(
						segment.xfrom * self.RatioX + self.Margin, 
						segment.yfrom * (self.Height-(self.Margin*2)) + self.Margin, 
						segment.xto * self.RatioX + self.Margin, 
						segment.yto * (self.Height-(self.Margin*2)) + self.Margin
						)
			
		for label in self.GetParent().CurrentTreeObj.TreeParts:
			if label.IsNode():
				dc.SetTextBackground(self.WhiteColor)
				dc.SetTextForeground(self.BlackColor)
				dc.SetFont(self.GetParent().config_dict['NodeFont'])
				dc.DrawText(
							str(label.support),
							label.x * self.RatioX + self.Margin + self.SepFromLine,
							label.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYNode
							)
				dc.DrawBitmap(
							self.NodeBitmap,
							label.x * self.RatioX + self.Margin - 2,
							label.y * (self.Height-(self.Margin*2)) + self.Margin -2
							)
				label.region = wx.Region(
											label.x * self.RatioX + self.Margin - 3,
											label.y * (self.Height-(self.Margin*2)) + self.Margin - 3, 
											7, 
											7
										)
				
			if label.IsLeaf():
				dc.SetFont(self.GetParent().config_dict['LeafFont'])
				if label.color:
					dc.SetTextBackground(label.color[0])
					dc.SetTextForeground(label.color[1])
				else :
					dc.SetTextBackground(self.WhiteColor)
					dc.SetTextForeground(self.BlackColor)
				if not label.IsActive():
					dc.SetTextBackground(self.WhiteColor)
					dc.SetTextForeground(self.RedColor)
				dc.DrawText(
							str(label.label),
							label.x * self.RatioX + self.Margin + self.SepFromLine,
							label.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYLeaf
							)
				label.region = wx.Region(
											label.x * self.RatioX + self.Margin + self.SepFromLine, 
											label.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYNode, 
											dc.GetTextExtent(label.label)[0], 
											dc.GetTextExtent(label.label)[1]
										)
		self.SetVirtualSize((self.Width, self.Height))
	def UpdateLeafs(self, ModifiedLeafs):
		self.Refresh()
		# try: dc = wx.GCDC(wx.BufferedDC(None, self.buffer))
		# except: dc = wx.BufferedDC(None, self.buffer)
		
		dc = wx.BufferedDC(None, self.buffer)
		
		dc.SetFont(self.GetParent().config_dict['LeafFont'])
		dc.SetPen(wx.Pen(self.BlackColor))
		dc.SetBackground(wx.Brush(self.WhiteColor))
		dc.SetBackgroundMode(wx.SOLID)
		
		for label in ModifiedLeafs:
			if label.color:
				dc.SetTextBackground(label.color[0])
				dc.SetTextForeground(label.color[1])
			else :
				dc.SetTextBackground(self.WhiteColor)
				dc.SetTextForeground(self.BlackColor)
			if not label.IsActive():
				dc.SetTextBackground(self.WhiteColor)
				dc.SetTextForeground(self.RedColor)
			dc.DrawText(
						str(label.label),
						label.x * self.RatioX + self.Margin + self.SepFromLine,
						label.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYLeaf
						)

		
class DefaultTreePopUpMenu(wx.Menu):
	def __init__(self, parent, ClickedPart):
		wx.Menu.__init__(self, "Actions")
		
		self.ClickedPart = ClickedPart
		ItemList =	[
					[wx.NewId(),"Reroot from here",0]
					]
		
		if ClickedPart.IsNode() : ItemList.append([wx.NewId(),"Flip this node",1])
		self.ActionList = {}
		self.parent = parent
		for MenuItem in ItemList:
			self.Append(MenuItem[0], MenuItem[1])
			wx.EVT_MENU(self, MenuItem[0], self.OnPopMenuAction)
			self.ActionList[str(MenuItem[0])] = MenuItem[2]
			
	def OnPopMenuAction(self, event):
		if self.ActionList[str(event.GetId())] == 0:
			self.parent.GetParent().CurrentTreeObj.Reroot(self.ClickedPart)
			self.parent.DrawTree()
		elif self.ActionList[str(event.GetId())] == 1:
			self.parent.GetParent().CurrentTreeObj.SwapNode(self.ClickedPart)
			self.parent.DrawTree()
		

