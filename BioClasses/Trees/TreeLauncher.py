#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import BioClasses, os
from os import system, remove
from sys import exit

def RunTree(Filename, Software='TreeFinder', MakeSeqNames=False, SubstitionModel=False, KeepTFfiles=False, nBproc=1):
	if Software in ['TreeFinder','FastTree','mrBayes','FastTree_Nt','mrBayes_MPI']:
		# Make seq naes if required
		if MakeSeqNames:
			SeqNameFile = MakeSeqNamesList(Filename)
		# Lauch the work
		if Software == 'TreeFinder' : AlignedFile = RunTreeFinder(Filename,SubstitionModel, KeepTFfiles)
		if Software == 'FastTree' : AlignedFile = RunFastTree(Filename)
		if Software == 'FastTree_Nt' : AlignedFile = RunFastTreeNT(Filename)
		if Software == 'mrBayes' : AlignedFile = RunMrBayes(Filename)
		if Software == 'mrBayes_MPI' : AlignedFile = RunMrBayes_MPI(nBproc, Filename)
		
		# Restore seqnames if required
		if MakeSeqNames:
			RestoreSeqNamesInAlignment(Filename, SeqNameFile)
			RestoreSeqNamesInAlignment(AlignedFile, SeqNameFile)
			
		return AlignedFile
	else :
		raise Exception('Software option incorrect')
	
def RunTreeFinder(Filename,SubstitionModel,KeepTFfiles=False) :
	# Preparing file names
	RepportFile = BioClasses.GetFilePrefix(Filename) + ".tlrep"
	TreeFinderScriptFile =  BioClasses.GetFilePrefix(Filename) + ".tfscr"
	TreeFile = BioClasses.GetFilePrefix(Filename) + ".tre"
	
	# Launching reconstruction
	WorkingDir = BioClasses.GetWorkingDirectoryForTreeFinder()
	TreeFinderScript = open(TreeFinderScriptFile,'w')
	TreeFinderScript.write('SaveReport[ReconstructPhylogeny[\"'+WorkingDir+Filename+'",SubstitutionModel->'+SubstitionModel+'],"'+WorkingDir+RepportFile+'",Format->"TL"],\nQuit\n')
	TreeFinderScript.close()
	print "Launching Reconstruction"
	TreeFinderCommand = BioClasses.BioPATH.treefinder +'<'+ TreeFinderScriptFile
	try :
		system(TreeFinderCommand)
	except :
		print "_______________ Error during tree reconstruction _______________"
		exit()
	
	# ReLaunching TreeF‫inder to save tree data
	ReportFileObject = open(RepportFile,'r')
	ReportFileData = ReportFileObject.read()
	ReportFileData = ReportFileData.strip('\r\n')
	ReportFileData = ReportFileData.replace('\n','')
	ReportFileData = ReportFileData.replace('\r','')
	ReportFileData = ReportFileData.replace(' ','')
	ReportFileData = ReportFileData.replace(',()','')
	ReportFileObject.close()
	
	TreeFinderScript = open(TreeFinderScriptFile,'w')
	TreeFinderScript.write('SaveTreeList[\'PlotTree['+ReportFileData+',Header->"'+WorkingDir+RepportFile+'"],"'+WorkingDir+TreeFile+'",Format->"NEWICK"],\nQuit\n')
	TreeFinderScript.close()
	
	TreeFinderCommand = BioClasses.BioPATH.treefinder +'<'+ TreeFinderScriptFile
	
	print "Extracting Tree Data"
	try :
		system(TreeFinderCommand)
	except :
		print "_______________ Error While Saving Tree _______________"
	
	# Removing template files
	if not KeepTFfiles : 
		remove(TreeFinderScriptFile)
		remove(RepportFile)
	
	return TreeFile
	
def RunFastTree(Filename) :
	TreeFile = BioClasses.GetFilePrefix(Filename) + ".ph"
	FastTreeCommand = BioClasses.BioPATH.fasttree + ' ' + Filename + ' > ' + TreeFile
	system(FastTreeCommand)
	return TreeFile

def RunFastTreeNT(Filename) :
	TreeFile = BioClasses.GetFilePrefix(Filename) + ".ph"
	FastTreeCommand = BioClasses.BioPATH.fasttree + ' -nt ' + Filename + ' > ' + TreeFile
	system(FastTreeCommand)
	return TreeFile
	
def RunMrBayes(Filename) :
	mbCommands="""
begin mrbayes;
	prset aamodelpr=mixed;

	lset 
		rates=invgamma
		ngammacat=8;

	set autoclose=yes;

	mcmcp
		ngen=1000000
		printfreq=10000
		samplefreq=100
		nchains=1
		savebrlens=yes
		filename="""+BioClasses.GetFilePrefix(Filename)+""";
	mcmc;
	
	quit;
end;"""
	
	#Generating temporary nexus
	AlnFasta = BioClasses.Sequences.MyFasta(Filename)
	TempName = BioClasses.GetFilePrefix(Filename)+".temp"
	TemporaryObj = open(TempName,'w')
	TemporaryObj.write(AlnFasta.FullOutputInNexus())
	TemporaryObj.write(mbCommands)
	TemporaryObj.close()
	
	MrBayesCommand = BioClasses.BioPATH.mrBayes + ' -i ' + TempName
	print "Running mrBayes on "+TempName
	system(MrBayesCommand)
	os.remove(TempName)
	#Extracting tree from con
	MbOfObj = open(BioClasses.GetFilePrefix(Filename)+'.con','r')
	for line in MbOfObj:
		if line.find("tree con_all_compat")>=0 :
			TreeData = line.split(' = ')[1]
			break
	MbOfObj.close()
	TreeFile = BioClasses.GetFilePrefix(Filename)+'.tre'
	tfObj = open(TreeFile,'w')
	tfObj.write(TreeData)
	tfObj.close()
	return TreeFile

def RunMrBayes_MPI(nbProc, Filename) :
	mbCommands="""
begin mrbayes;
	prset aamodelpr=mixed;

	lset 
		rates=invgamma
		ngammacat=8;

	set autoclose=yes;

	mcmcp
		ngen=1000000
		printfreq=10000
		samplefreq=100
		nchains=1
		savebrlens=yes
		filename="""+BioClasses.GetFilePrefix(Filename)+""";
	mcmc;
	
	quit;
end;"""
	
	#Generating temporary nexus
	AlnFasta = BioClasses.Sequences.MyFasta(Filename)
	TempName = BioClasses.GetFilePrefix(Filename)+".temp"
	TemporaryObj = open(TempName,'w')
	TemporaryObj.write(AlnFasta.FullOutputInNexus())
	TemporaryObj.write(mbCommands)
	TemporaryObj.close()
	
	MrBayesCommand = "mpirun -np "+ nbProc +" " + BioClasses.BioPATH.mrBayes + ' -i ' + TempName
	print "Running mrBayes on "+TempName
	system(MrBayesCommand)
	os.remove(TempName)
	#Extracting tree from con
	MbOfObj = open(BioClasses.GetFilePrefix(Filename)+'.con','r')
	for line in MbOfObj:
		if line.find("tree con_all_compat")>=0 :
			TreeData = line.split(' = ')[1]
			break
	MbOfObj.close()
	TreeFile = BioClasses.GetFilePrefix(Filename)+'.tre'
	tfObj = open(TreeFile,'w')
	tfObj.write(TreeData)
	tfObj.close()
	return TreeFile


#	sumt 
#		filename="""+BioClasses.GetFilePrefix(Filename)+"""
#		burnin=1000
#		contype=allcompat;
	
	
def DataBaseTreeMaker(pgServerIP) :
	seq_list_file = raw_input("\nName of the file containing the seq_id list to treat?? : ")
	limEval = 1e-5
	asklimEval = raw_input("\nPlease enter an evalue threshold (default=1e-5): ")
	if asklimEval != '':
		limEval = float(asklimEval)
	Number = 50
	askNumber = raw_input("\nPlease enter how many blast res. to use (default=50): ")
	if askNumber != '':
		Number = int(askNumber)
	db_len = coreFuncs.tell_db_len()
	model2use = coreFuncs.choose_subst_model()
	#Openning file
	ifObj = open(seq_list_file,'r')
	db = pg.connect('Genomes',pgServerIP,5432,None,None,'pguser','Genomes')
	for seq_id in ifObj:
		Auj = datetime.datetime.now()
		Auj.timetuple()
		seq_id = seq_id.strip('\r\n')
		#Look if a tree using this seq was already generated
		get_if_done = db.query("SELECT tree_id FROM trees WHERE seed_seq_id="+seq_id)
		if get_if_done.ntuples() == 0:
		#-------If not, register the tree----
			get_tree_id = db.query("INSERT INTO trees VALUES(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, "+str(seq_id)+", '"+str(Auj)+"', DEFAULT, '"+str(model2use)+"') RETURNING tree_id")
			tree_id = get_tree_id.getresult()[0][0]
		#-------Get all blasts hits from db--------
			allblastid = []
			allblastdef = []
			get_blastdef = db.query("SELECT blastid,seq2,bitscore,seqlen,spname,aa FROM blast LEFT JOIN aaseqs ON aaseqs.localid=blast.seq2 LEFT JOIN sp ON sp.spid=aaseqs.idsp WHERE blast.seq1="+str(seq_id))
			for blastdef in get_blastdef.dictresult():
				if int(blastdef['blastid']) not in allblastid:
					if blastdef['bitscore'] > 1000:
						eValue = 0
					else :
						eValue = (blastdef['seqlen']*db_len)/(pow(2, blastdef['bitscore']))
					newblastdef = [eValue,blastdef['seq2'],blastdef['spname'],blastdef['aa']]
					# Eliminate seq with evalue too high or too long
					if eValue < limEval and blastdef['seqlen'] < 5000:
						allblastdef.append(newblastdef)
						allblastid.append(int(blastdef['blastid']))
			get_blastdef = db.query("SELECT blastid,seq1,bitscore,seqlen,spname,aa FROM blast LEFT JOIN aaseqs ON aaseqs.localid=blast.seq1 LEFT JOIN sp ON sp.spid=aaseqs.idsp WHERE blast.seq2="+str(seq_id))
			for blastdef in get_blastdef.dictresult():
				if int(blastdef['blastid']) not in allblastid:
					if blastdef['bitscore'] > 1000:
						eValue = 0
					else :
						eValue = (blastdef['seqlen']*db_len)/(pow(2, blastdef['bitscore']))
					newblastdef = [eValue,blastdef['seq1'],blastdef['spname'],blastdef['aa']]
					# Eliminate seq with evalue too high or too long
					if eValue < limEval and blastdef['seqlen'] < 5000:
						allblastdef.append(newblastdef)
						allblastid.append(int(blastdef['blastid']))
		#-------Sort all blasts results collected--------
			allblastdef.sort()
		#-------if at least 3 results in blast------#
			if len(allblastdef) > 2:
		#-------Generate template fas file------#
				tempName = seq_id+'.fas'
				tempObj = open(tempName,'w')
				for blastdef in allblastdef[:Number]:
					tempObj.write(">"+str(blastdef[2])+"@"+str(blastdef[1])+"\n"+str(blastdef[3])+"\n")
				tempObj.close()
		#-------running Muscle on template fas file------#
				print "Running muscle And editing alignement"
				try:
					seqsFuncs.Run_Muscle(tempName)
					#Trying to open alignement
					AlnLen = seqsFuncs.BMGE(seq_id+'.aln')
					seqsFuncs.tag_partial_seq(seq_id+'.gap')
				except:
					print "\n-----------------Error during aligment------------------\n"
					db.query("DELETE from trees WHERE tree_id="+str(tree_id))
					sys.exit()
				# launch TF phylogeny
				print "Launching tree"
				rep_work = coreFuncs.get_working_dir()
				dest_file=seq_id+'.tlrep'
				TFscrObj = open(seq_id+'.tfscr','w')
				TFscrObj.write('SaveReport[ReconstructPhylogeny[\"'+rep_work+seq_id+'.gap'+'",SubstitutionModel->'+model2use+'],"'+rep_work+dest_file+'",Format->"TL"],\nQuit\n')
				TFscrObj.close()
				TF_cmd = TF_path +'<'+ seq_id+'.tfscr'
				try :
					system(TF_cmd)
					#Trying to open tree
					tlrepObj = open(dest_file,'r')
				except :
					print "\n-----------------Error while reconstructing phylogeny------------------\n"
					db.query("DELETE from trees WHERE tree_id="+str(tree_id))
					sys.exit()
		#-------launch TF to get tree
				print "Getting tree"
				TFscrObj = open(seq_id+'.tfscr','w')
				file_content=tlrepObj.read()
				file_content=file_content.strip('\r\n')
				file_content=file_content.replace('\n','')
				file_content=file_content.replace('\r','')
				file_content=file_content.replace(' ','')
				file_content=file_content.replace(',()','')
				tlrepObj.close()
				tree_file = seq_id+'.tre'
				TFscrObj.write('SaveTreeList[\'PlotTree['+file_content+',Header->"'+rep_work+dest_file+'"],"'+rep_work+tree_file+'",Format->"NEWICK"],\nQuit\n')
				TFscrObj.close()
				TF_cmd = TF_path +'<'+ seq_id+'.tfscr'
				try :
					system(TF_cmd)
				except :
					print "\n-----------------Error while saving tree------------------\n"
					db.query("DELETE from trees WHERE tree_id="+str(tree_id))
					sys.exit()
		#-------Record tree in tree db
				seqsFuncs.Replace_in_tree(tree_file, False)
				treeFileObj = open(tree_file,'r')
				file_content=treeFileObj.read()
				file_content=file_content.strip('\r\n')
				treeObj = MyObjects.parse_tree(file_content)
				db.query("UPDATE trees SET tree_ph='"+file_content+"',nbsites="+AlnLen+" WHERE tree_id ="+str(tree_id))
				treeFileObj.close()
		#-------Record alignment in tree db
				seqsFuncs.Replace_in_aligment(seq_id+'.gap', False)
				alnObj = open(seq_id+'.gap','r')
				file_content=alnObj.read()
				db.query("UPDATE trees SET alignement='"+file_content+"' WHERE tree_id ="+str(tree_id))
				alnObj.close()
		# Registering tree for all sequences
				for feuille in treeObj.liste_feuilles():
					localid = treeObj.elements[feuille].label.split('@')[1]
					get_seq2tree = db.query("SELECT * FROM seq2trees WHERE tree_id="+str(tree_id)+" AND seq_id="+str(localid))
					if get_seq2tree.ntuples()==0:
						db.query("INSERT INTO seq2trees VALUES(DEFAULT, '"+str(localid)+"','"+str(tree_id)+"')")
		#-------Removing template files
				remove(seq_id+'.fas')
				remove(seq_id+'.aln')
				remove(seq_id+'.gap')
				remove(seq_id+'.seqnames')
				remove(seq_id+'.tfscr')
				remove(seq_id+'.tlrep')
				remove(seq_id+'.tre')
			else :
				print "Not enough blast result"
		else :
			print "tree is already computed"
	db.close()


