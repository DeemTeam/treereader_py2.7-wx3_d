#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import wx
from os import path
import BioClasses.Trees

class MyTreeFrame(wx.Frame):
	def __init__(self, parent, Configurator, TreeFile, TreeFileName):
		wx.Frame.__init__(self, parent, -1, TreeFile ,wx.DefaultPosition, (1000,700), wx.DEFAULT_FRAME_STYLE | wx.CLIP_CHILDREN | wx.FRAME_FLOAT_ON_PARENT)
		self.SetMinSize(wx.Size(1000,700))
		
		# Opening tree File
		ifObj = open(TreeFile,'r')
		all_text = ''
		for line in ifObj: 
			if len(line.strip(' \r\n'))!=0 : all_text += line.strip(' \r\n').replace(';',';�')
		ifObj.close()
		self.TreesList = all_text.split('�')
		if self.TreesList.count('') : self.TreesList.remove('')
		
		# Variable for operation
		self.Configurator = Configurator
		self.TreeFileName = TreeFileName
		# Tree Interaction
		
		# Tree Browsing
		self.CurrentTreeNum = 1
		
		self.CurrentTreeObj = BioClasses.Trees.Arbre(self.TreesList[self.CurrentTreeNum-1],self.Configurator)
		
		# Drawing the frame
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		self.LeftPanel = TunningPanel(self)
		self.RightPanel = MyTreeWindow(self)
		
		H_Sizer.Add(self.LeftPanel, 0, wx.EXPAND)
		H_Sizer.Add(self.RightPanel, 1, wx.EXPAND|wx.ALL, 0)
		
		self.SetSizer(H_Sizer)
	def SwitchTree(self, Action):
		ExTreeNum = self.CurrentTreeNum
		if Action == "First":
			self.CurrentTreeNum = 1
		elif Action == "Back":
			if self.CurrentTreeNum - 1 >= 1 : self.CurrentTreeNum -= 1
		elif Action == "Fwd":
			if self.CurrentTreeNum + 1 <= len(self.TreesList) : self.CurrentTreeNum += 1
		elif Action == "Last":
			self.CurrentTreeNum = len(self.TreesList)
		else:
			if  1 < int(Action) < len(self.TreesList) : self.CurrentTreeNum = int(Action)
			else : 
				Errordlg = wx.MessageDialog(self, 'Number is out of range','Error',wx.OK | wx.ICON_ERROR)
				Errordlg.ShowModal()
				Errordlg.Destroy()
				self.LeftPanel.CurrentTreeNumber.SetValue(str(self.CurrentTreeNum))
		if ExTreeNum != self.CurrentTreeNum:
			self.ReloadTree()
			self.RefreshTreePanel()
	def ReloadTree(self):
		self.CurrentTreeObj = BioClasses.Trees.Arbre(self.TreesList[self.CurrentTreeNum-1],self.Configurator)
	def RefreshTreePanel(self):
		self.LeftPanel.CurrentTreeNumber.SetValue(str(self.CurrentTreeNum))
		self.RightPanel.DrawTree()
		self.RightPanel.SetFocus()


class MyTreeWindow(wx.ScrolledWindow):
	def __init__(self, parent):
		wx.ScrolledWindow.__init__(self, parent, -1, wx.DefaultPosition, wx.DefaultSize)
		
		#self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)
		self.SetScrollRate(20,20)
		self.SetBackgroundColour("WHITE")
		
		# Variables
		self.FirstOpen = False
		self.resized = False
		self.NodeBitmap = wx.Bitmap(path.join(self.GetParent().Configurator.ImgDir, "NodeImg.png"), wx.BITMAP_TYPE_PNG)
		
		self.BlackColor = wx.Colour(0, 0, 0, wx.ALPHA_OPAQUE)
		self.WhiteColor = wx.Colour(255, 255, 255, wx.ALPHA_OPAQUE)
		self.RedColor = wx.Colour(255, 0, 0, wx.ALPHA_OPAQUE)
		
		self.Margin = 10
		self.SepFromLine = 5
		self.Height = 20
		self.Width = 20
		self.RatioX = 0
		self.ShiftYNode = 0
		self.ShiftYLeaf = 0
		self.buffer = wx.EmptyBitmap(self.Width,self.Height)
		
		# Events Binding
		self.Bind(wx.EVT_SIZE , self.OnSize)
		self.Bind(wx.EVT_PAINT, self.OnPaint)
		self.Bind(wx.EVT_ENTER_WINDOW, self.OnEnter)
		self.Bind(wx.EVT_LEFT_UP, self.OnLeftClick)
		self.Bind(wx.EVT_RIGHT_UP, self.OnRightClick)
		
	def OnPaint(self, event):
		dc = wx.BufferedPaintDC(self, self.buffer, wx.BUFFER_VIRTUAL_AREA)
		#event.Skip()
	def OnSize(self, event):
		self.resized = True
		#event.Skip()
	def OnEnter(self, event):
		if self.resized :
			self.DrawTree()
			self.resized = False
	
	def OnLeftClick(self, event):
		ModifiedLeafs = False
		ClickCoords = self.CalcUnscrolledPosition(event.GetX(), event.GetY())
		for part in self.GetParent().CurrentTreeObj.TreeParts: 
			if part.region.ContainsPoint(ClickCoords) == wx.InRegion: 
				ModifiedLeafs = self.GetParent().CurrentTreeObj.ActivateOrDeactivatePart(part)
		if ModifiedLeafs: self.UpdateLeafs(ModifiedLeafs)
	def OnRightClick(self, event):
		ModifiedLeafs = False
		ClickCoords = self.CalcUnscrolledPosition(event.GetX(), event.GetY())
		for part in self.GetParent().CurrentTreeObj.TreeParts: 
			if part.region.ContainsPoint(ClickCoords) == wx.InRegion: 
				TempPopUpMenu = DefaultTreePopUpMenu(self, part)
				self.PopupMenu(TempPopUpMenu)
				TempPopUpMenu.Destroy()
	def DrawTree(self):
		self.Refresh()
		
		self.buffer = wx.EmptyBitmap(self.Width,self.Height)
		dc = wx.BufferedDC(None, self.buffer)
		dc.SetFont(self.GetParent().Configurator.LeafFont)
		
		# Width is always deduced from window
		self.Width = self.GetSizeTuple()[0]-20
		# Height is from window*Expansion Config
		self.Height = self.GetParent().Configurator.Expansion*self.GetParent().CurrentTreeObj.NumberOfLeaf()*dc.GetTextExtent("A")[1] + (self.Margin * 2)
		
		self.buffer = wx.EmptyBitmap(self.Width,self.Height)
		# try: dc = wx.GCDC(wx.BufferedDC(None, self.buffer))
		# except: dc = wx.BufferedDC(None, self.buffer)
		
		dc = wx.BufferedDC(None, self.buffer)
		
		dc.SetFont(self.GetParent().Configurator.NodeFont)
		self.ShiftYNode = dc.GetTextExtent("A")[1]/2
		dc.SetFont(self.GetParent().Configurator.LeafFont)
		self.ShiftYLeaf = dc.GetTextExtent("A")[1]/2
		self.RatioX = (self.Width - self.Margin*2 - self.SepFromLine - dc.GetTextExtent(self.GetParent().CurrentTreeObj.LongestSegment[1].LabelData.Label)[0]) / self.GetParent().CurrentTreeObj.LongestSegment[0]
		
		dc.SetPen(wx.Pen(self.BlackColor))
		dc.SetBackground(wx.Brush(self.WhiteColor))
		dc.SetBackgroundMode(wx.SOLID)
		
		dc.Clear()
		#dc.StartDrawing()
		# start with horizontal segments
		for segment in self.GetParent().CurrentTreeObj.segments:
			dc.DrawLine(
						segment.xfrom * self.RatioX + self.Margin, 
						segment.yfrom * (self.Height-(self.Margin*2)) + self.Margin, 
						segment.xto * self.RatioX + self.Margin, 
						segment.yto * (self.Height-(self.Margin*2)) + self.Margin
						)
			
		for label in self.GetParent().CurrentTreeObj.TreeParts:
			if label.IsNode():
				dc.SetTextBackground(self.WhiteColor)
				dc.SetTextForeground(self.BlackColor)
				dc.SetFont(self.GetParent().Configurator.NodeFont)
				dc.DrawText(
							str(label.support),
							label.x * self.RatioX + self.Margin + self.SepFromLine,
							label.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYNode
							)
				dc.DrawBitmap(
							self.NodeBitmap,
							label.x * self.RatioX + self.Margin - 2,
							label.y * (self.Height-(self.Margin*2)) + self.Margin -2
							)
				label.region = wx.Region(
											label.x * self.RatioX + self.Margin - 3,
											label.y * (self.Height-(self.Margin*2)) + self.Margin - 3, 
											7, 
											7
										)
				
			if label.IsLeaf():
				dc.SetFont(self.GetParent().Configurator.LeafFont)
				if label.Color:
					dc.SetTextBackground(label.Color[0])
					dc.SetTextForeground(label.Color[1])
				else :
					dc.SetTextBackground(self.WhiteColor)
					dc.SetTextForeground(self.BlackColor)
				if not label.IsActive():
					dc.SetTextBackground(self.WhiteColor)
					dc.SetTextForeground(self.RedColor)
				dc.DrawText(
							str(label.LabelData.Label),
							label.x * self.RatioX + self.Margin + self.SepFromLine,
							label.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYLeaf
							)
				label.region = wx.Region(
											label.x * self.RatioX + self.Margin + self.SepFromLine, 
											label.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYNode, 
											dc.GetTextExtent(label.LabelData.Label)[0], 
											dc.GetTextExtent(label.LabelData.Label)[1]
										)
		self.SetVirtualSize((self.Width, self.Height))
	def UpdateLeafs(self, ModifiedLeafs):
		self.Refresh()
		# try: dc = wx.GCDC(wx.BufferedDC(None, self.buffer))
		# except: dc = wx.BufferedDC(None, self.buffer)
		
		dc = wx.BufferedDC(None, self.buffer)
		
		dc.SetFont(self.GetParent().Configurator.LeafFont)
		dc.SetPen(wx.Pen(self.BlackColor))
		dc.SetBackground(wx.Brush(self.WhiteColor))
		dc.SetBackgroundMode(wx.SOLID)
		
		for label in ModifiedLeafs:
			if label.Color:
				dc.SetTextBackground(label.Color[0])
				dc.SetTextForeground(label.Color[1])
			else :
				dc.SetTextBackground(self.WhiteColor)
				dc.SetTextForeground(self.BlackColor)
			if not label.IsActive():
				dc.SetTextBackground(self.WhiteColor)
				dc.SetTextForeground(self.RedColor)
			dc.DrawText(
						str(label.LabelData.Label),
						label.x * self.RatioX + self.Margin + self.SepFromLine,
						label.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYLeaf
						)

		
class DefaultTreePopUpMenu(wx.Menu):
	def __init__(self, parent, ClickedPart):
		wx.Menu.__init__(self, "Actions")
		
		self.ClickedPart = ClickedPart
		ItemList =	[
					[wx.NewId(),"Reroot from here",0]
					]
		
		if ClickedPart.IsNode() : ItemList.append([wx.NewId(),"Flip this node",1])
		self.ActionList = {}
		self.parent = parent
		for MenuItem in ItemList:
			self.Append(MenuItem[0], MenuItem[1])
			wx.EVT_MENU(self, MenuItem[0], self.OnPopMenuAction)
			self.ActionList[str(MenuItem[0])] = MenuItem[2]
			
	def OnPopMenuAction(self, event):
		if self.ActionList[str(event.GetId())] == 0:
			self.parent.GetParent().CurrentTreeObj.Reroot(self.ClickedPart)
			self.parent.DrawTree()
		elif self.ActionList[str(event.GetId())] == 1:
			self.parent.GetParent().CurrentTreeObj.SwapNode(self.ClickedPart)
			self.parent.DrawTree()
		

