import wx, os, sys
import lib.TreeVClass as TreeVClass
import lib.SetterClass as SetterClass
import lib.AlignVClass as AlignVClass
from lib import taxDATA, NumLinesInFile
import pickle

class mainFrame(wx.Frame):
	def __init__(self, parent, title,
				Configurator,
				pos=wx.DefaultPosition, size=(400,150),
				style=wx.DEFAULT_FRAME_STYLE):
		wx.Frame.__init__(self, parent, -1, title, pos, size, style)
		
		self.Configurator = Configurator
		
		# Window styles
		self.SetMinSize(wx.Size(400,150))
		self.SetMaxSize(wx.Size(400,150))
		self.SetIcon(wx.Icon(os.path.join(Configurator.ImgDir, "xrced.ico"), wx.BITMAP_TYPE_ICO, 16, 16))
		
		# IDs
		ID_OPEN_TREE_FILE = wx.NewId()
		ID_OPEN_PROJECT_FILE = wx.NewId()
		ID_OPEN_SEQ_FILE = wx.NewId()
		ID_SETTINGS = wx.NewId()
		
		# Create Menu Bar
		mb = wx.MenuBar()
		file_menu = wx.Menu()
		file_menu.Append(ID_OPEN_TREE_FILE, "&Open a tree file", "Open a tree file")
		file_menu.Append(ID_OPEN_SEQ_FILE, "&Test Sequence Frame", "Test Sequence frame")
		file_menu.Append(wx.ID_EXIT, "Exit")
		mb.Append(file_menu, "File")
		settings_menu = wx.Menu()
		settings_menu.Append(ID_SETTINGS, "&Define Taxon Colors", "Chose Colors")
		mb.Append(settings_menu, "Tools")
		self.SetMenuBar(mb)
			
		# Create Button Bar
		BtnPanel = wx.Panel(self, -1, style=wx.WANTS_CHARS)
		V_Sizer = wx.BoxSizer(wx.VERTICAL)
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		OpenProjectBtn = wx.BitmapButton(BtnPanel, -1, wx.Bitmap(os.path.join(Configurator.ImgDir, "OpenProjectIcon.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, (65,60) , style = wx.BU_AUTODRAW)
		OpenTreeBtn = wx.BitmapButton(BtnPanel, -1, wx.Bitmap(os.path.join(Configurator.ImgDir, "OpenTreeIcon.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, (65,60) , style = wx.BU_AUTODRAW)
		SettingsBtn = wx.BitmapButton(BtnPanel, -1, wx.Bitmap(os.path.join(Configurator.ImgDir, "Settings.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, (65,60) , style = wx.BU_AUTODRAW)
		
		H_Sizer.Add(OpenProjectBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(OpenTreeBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(SettingsBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		V_Sizer.Add(H_Sizer, 1, wx.ALIGN_CENTER|wx.ALL, 0)
		BtnPanel.SetSizer(V_Sizer)
		#H_Sizer.Fit(self)
		
		self.Bind(wx.EVT_BUTTON, self.OnOpenTree, OpenTreeBtn)
		self.Bind(wx.EVT_BUTTON, self.OnSettings, SettingsBtn)
		
		# Create Status bar
		self.statusbar = self.CreateStatusBar(2, wx.ST_SIZEGRIP)
		self.statusbar.SetStatusWidths([-2, -3])
		self.statusbar.SetStatusText("My Phylogenetic Tools", 0)
		
		self.Bind(wx.EVT_MENU, self.OnExit, id=wx.ID_EXIT)
		self.Bind(wx.EVT_MENU, self.OnOpenTree, id=ID_OPEN_TREE_FILE)
		self.Bind(wx.EVT_MENU, self.OnOpenSequence, id=ID_OPEN_SEQ_FILE)
		self.Bind(wx.EVT_MENU, self.OnSettings, id=ID_SETTINGS)
		#self.Bind(wx.EVT_TIMER, self.OnTimerCall)
		#Settings Frame (See function bottom page)
		self.SettingsDialog = False
		
		
		
	def OnExit(self, event):
		self.Close()
	
	def OnSettings(self, event):
		self.SettingsDialog.Show(True)
			
	def OnOpenTree(self, evt):
		dlg = wx.FileDialog(self, "Open a Tree File", wildcard = "Phylogenetic Trees (*.ph;*.tre)|*.ph;*.tre|All files (*.*)|*.*", style = wx.OPEN|wx.FD_CHANGE_DIR)
		DlgResponse = dlg.ShowModal()
		FilePath = dlg.GetPath()
		FileName = dlg.GetFilename()
		dlg.Destroy()
		if DlgResponse == wx.ID_OK and FileName != "": 
			self.OpenTree(FilePath, FileName) 
		
	def OpenTree(self, FilePath, FileName):
		TreeFrame = TreeVClass.MyTreeFrame(self, self.Configurator, FilePath, FileName)
		TreeFrame.Show(True)
		
	def OnOpenSequence(self, evt):
		self.SeqFrame = AlignVClass.MyAlnFrame(self)
		self.SeqFrame.Show(True)
		
	def LoadColorFiles(self):
		#try : # Test if files are in directory
			NodesFile = os.path.join(self.Configurator.DbData, "nodes.dat")
			NamesFile = os.path.join(self.Configurator.DbData, "names.dat")
			MyColorsFile = os.path.join(self.Configurator.DbData, "mycolors.lst")

			dlg = wx.ProgressDialog("Loading Colour Files",
						"Loading Colour Files :\n Loading Names",
						100,
						self,wx.PD_APP_MODAL)

			# ---------- Loading Names from pickle out --------------
			with open(NamesFile, 'rb') as ifObj :
				self.Configurator.Names = pickle.load(ifObj)

			DlgState = 50
			
			# ---------- Loading Nodes from pickle out --------------
			dlg.Update(DlgState, "Loading Colour Files :\n Loading Nodes")
			with open(NodesFile, 'rb') as ifObj :
				self.Configurator.Nodes = pickle.load(ifObj)

			DlgState = 70

			# ---------- Loading MyColors from pickle out --------------
			dlg.Update(DlgState, "Loading Colour Files :\n Loading My Colors")
			ifObj = open(MyColorsFile,'r')
			for line in ifObj:
				if not line.startswith('#'):
					print(line)
					data = line.strip("\r\n").split('|')
					tax_id = int(data[0])
					TextBackground = wx.Colour(int(data[2].split(':')[0]), int(data[2].split(':')[1]), int(data[2].split(':')[2]))
					TextForeground = wx.Colour(int(data[3].split(':')[0]), int(data[3].split(':')[1]), int(data[3].split(':')[2]))
					self.Configurator.MyColors[tax_id]=[data[1],(TextBackground, TextForeground)]
			

			ifObj.close()
			dlg.Destroy()
		# except:
		# 	Errordlg = wx.MessageDialog(self, 'Error While Loading Colour Configuration files','Error',wx.OK | wx.ICON_ERROR)
		# 	Errordlg.ShowModal()
		# 	Errordlg.Destroy()
		# 	self.Configurator.UseColors = False
		# 	dlg.Destroy()
			
	def LoadSettingFrame(self):
		self.SettingsDialog = SetterClass.SettingsDialog(self)
		
#	def OnTimerCall(self, event):
#		print "Timer!"
		
