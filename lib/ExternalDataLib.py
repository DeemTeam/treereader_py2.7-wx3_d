# -*- coding: iso-8859-1 -*-

import wx, os


class ExtGroup():
	def __init__(self, parent):
		self.GpName = ''
		self.Members = []
		self.Annotations = []
		self.parent = parent

	def AddMember(self, LocalID):
		if LocalID not in self.Members : 
			self.Members.append(LocalID)
			self.parent.AddToAllMembers(LocalID)

	def RemoveMember(self, LocalID):
		if LocalID in self.Members : 
			self.Members.remove(LocalID)
			self.parent.RemoveFromAllMembers(LocalID)

	def UpdateAnnotations(self, CommaSepAnnotations):
		CommaSepAnnotations = CommaSepAnnotations.strip('\r\n,')
		CommaSepAnnotations = CommaSepAnnotations.replace('\r\n<>','')
		self.Annotations = []
		NewAnnotationsList = CommaSepAnnotations.split(',')
		for NewAnnotation in NewAnnotationsList :
			self.Annotations.append(NewAnnotation)

class ExtDataStruct(list):
	def __init__(self, inFile):
		# Data Stored in the object
		self.ListOfGpNames = []
		self.CompleteListOfMembers = []

		# Single sequences Annotations
		self.SingleAnnotations = {}
		self.AnnotatedLocalIDs = []

		# For parsing purposes
		self.ExtGpList_open = False
		self.ExtGp_open = False
		self.ExtGpName_open = False
		self.ExtGpMember_open = False
		self.ExtGpAnnotation_open = False

		self.SingleAnnotationsList_open = False
		self.SingleAnnotation_open = False
		self.LocalID_open = False

		self.CurrentLocalID = False
		self.CurrentGp = False

		print "Starting XML Parser"
		ifObj = open(inFile,'r')
		XMLdata = ''
		for line in ifObj:
			XMLdata += line.strip('\r\n')
		ifObj.close()
		open_tag = False
		close_tag = False
		inside_tag = False
		between_tag = False
		current_tag = ''
		current_chars = ''
		for char in XMLdata:
			if char == '<':
				if between_tag :
					#print "Chars = "+current_chars
					self.characters(current_chars)
					current_chars = ''
					between_tag = False
				inside_tag = True
				open_tag = True
			elif char == '>':
				if open_tag:
					#print "Open tag = "+current_tag
					self.startElement(current_tag)
					current_tag = ''
					open_tag = False
					inside_tag = False
					between_tag = True
				if close_tag:
					#print "Close tag = "+current_tag
					self.endElement(current_tag)
					current_tag = ''
					close_tag = False
					inside_tag = False
			elif char == '/':
				if inside_tag:
					close_tag = True
					open_tag = False
			else :
				if inside_tag :
					current_tag += char
				if between_tag :
					current_chars += char
		#Renvoi des lignes de blast

		print "Done"
	
	def startElement(self, name):
	#"fonction appelee lorsque le parser rencontre une balise ouvrante"
		if name=='ExtGpList':			self.ExtGpList_open = True
		if name=='ExtGp':				
			self.ExtGp_open = True
			self.CurrentGp = ExtGroup(self)
		if name=='GpName':				self.ExtGpName_open = True
		if name=='Member':				self.ExtGpMember_open = True
		if name=='Annotation':			self.ExtGpAnnotation_open = True
		if name=='SingleAnnotationsList':		self.SingleAnnotationsList_open = True
		if name=='SingleAnnotation':			self.SingleAnnotation_open = True
		if name=='LocalID':						self.LocalID_open = True

	def characters(self, ch):
	#"fonction appelee lorsque le parser rencontre des donnees dans un element"
		if self.ExtGpName_open:				
			self.CurrentGp.GpName = ch
			self.ListOfGpNames.append(ch)
		if self.ExtGpMember_open: 			
			self.CurrentGp.Members.append(ch)
			self.CompleteListOfMembers.append(ch)
		if self.ExtGpAnnotation_open:		
			if self.ExtGp_open:
				self.CurrentGp.Annotations.append(ch)
			elif self.SingleAnnotation_open:
				self.SingleAnnotations[self.CurrentLocalID].append(ch)
		if self.SingleAnnotationsList_open:
			if self.LocalID_open:
				if ch not in self.AnnotatedLocalIDs:
					self.SingleAnnotations[ch] = []
					self.AnnotatedLocalIDs.append(ch)
				self.CurrentLocalID = ch

	def endElement(self, name):
	#"fonction appelee lorsque le parser rencontre une balise fermante"
		if name=='ExtGpList':			self.ExtGpList_open = False
		if name=='ExtGp':				
			self.ExtGp_open = False
			self.append(self.CurrentGp)
		if name=='GpName':				self.ExtGpName_open = False
		if name=='Member':				self.ExtGpMember_open = False
		if name=='Annotation':			self.ExtGpAnnotation_open = False
		if name=='SingleAnnotationsList':	self.SingleAnnotationsList_open = False
		if name=='SingleAnnotation':		self.SingleAnnotation_open = False
		if name=='LocalID':					self.LocalID_open = False

	def ExtractXMLdata(self, FileHandler):
		# Converts the object into XML string to save to file
		print "Outputing XML"
		FileHandler.write("<SingleAnnotationsList>\n")
		for LocalID, AnnotationList in self.SingleAnnotations.iteritems():
			FileHandler.write("\t<SingleAnnotation>\n")
			# Write LocalID
			FileHandler.write("\t\t<LocalID>"+LocalID+"</LocalID>\n")
			# Write Annotations
			for Annotation in AnnotationList:
				FileHandler.write("\t\t<Annotation>"+Annotation+"</Annotation>\n")
			FileHandler.write("\t</SingleAnnotation>\n")
		FileHandler.write("</SingleAnnotationsList>\n")

		FileHandler.write("<ExtGpList>\n")
		for Group in self:
			if len(Group.Members)>0:
				FileHandler.write("\t<ExtGp>\n")
				# Write Name
				FileHandler.write("\t\t<GpName>"+Group.GpName+"</GpName>\n")
				# Write Members
				for Member in Group.Members:
					FileHandler.write("\t\t<Member>"+Member+"</Member>\n")
				# Write Annotations
				for Annotation in Group.Annotations:
					FileHandler.write("\t\t<Annotation>"+Annotation+"</Annotation>\n")
				FileHandler.write("\t</ExtGp>\n")
		FileHandler.write("</ExtGpList>\n")
		print "Done"

	def AddGroup(self, GpName=False):
		NewGroup = ExtGroup(self)
		if GpName : NewGroup.GpName = GpName
		self.append(NewGroup)
		return NewGroup
	
	def GetSingleAnnotations(self, ListofLocalID):
		ListOfSingleAnnotations = []
		LocalIDsAlreadyAnnotated = self.SingleAnnotations.keys()
		for LocalID in ListofLocalID:
			if str(LocalID) in LocalIDsAlreadyAnnotated:
				for Annotation in self.SingleAnnotations[LocalID]:
					if Annotation not in ListOfSingleAnnotations:
						ListOfSingleAnnotations.append(Annotation)
		return ListOfSingleAnnotations

	def AddSingleAnnotations(self, ListofLocalID, CommaSepAnnotations):
		CommaSepAnnotations = CommaSepAnnotations.strip('\r\n,')
		CommaSepAnnotations = CommaSepAnnotations.replace('\r\n<>','')
		NewAnnotationsList = CommaSepAnnotations.split(',')
		
		LocalIDsAlreadyAnnotated = self.SingleAnnotations.keys()

		for LocalID in ListofLocalID:
			if str(LocalID) not in LocalIDsAlreadyAnnotated:
				self.SingleAnnotations[str(LocalID)] = []
			for NewAnnotation in NewAnnotationsList:
				if NewAnnotation not in self.SingleAnnotations[str(LocalID)]:
					self.SingleAnnotations[str(LocalID)].append(NewAnnotation)

	def RemoveSingleAnnotations(self, ListofLocalID, Annotation):
		LocalIDsAlreadyAnnotated = self.SingleAnnotations.keys()
		for LocalID in ListofLocalID:
			if str(LocalID) in LocalIDsAlreadyAnnotated:
				if Annotation in self.SingleAnnotations[str(LocalID)]:
					self.SingleAnnotations[str(LocalID)].remove(Annotation)
			# Check if dict is empty and erase it
				if len(self.SingleAnnotations[str(LocalID)])==0:
					del self.SingleAnnotations[str(LocalID)]

	def AddToAllMembers(self, LocalID):
		if LocalID not in self.CompleteListOfMembers : 
			self.CompleteListOfMembers.append(LocalID)

	def RemoveFromAllMembers(self, LocalID):
		# Test First if this Sequences is indeed absent from any groups before deleting it
		DoRemoveFromAllMembers = True
		for Group in self:
			if LocalID in Group.Members : DoRemoveFromAllMembers = False
		if DoRemoveFromAllMembers:
			if LocalID in self.CompleteListOfMembers : 
				self.CompleteListOfMembers.remove(LocalID)


class LocalGpPanel(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent, -1, wx.DefaultPosition, wx.DefaultSize)
		
		ID_LoadExtGpFile = wx.NewId()
		ID_LoadAnnoDict = wx.NewId()
		ID_SaveExtGpFile = wx.NewId()

		self.ParentalTreeFrame = parent.GetParent()
		
		V_Sizer = wx.BoxSizer(wx.VERTICAL)
		
		TitleText = wx.StaticText(self, label = "Use External Data", style = wx.ALIGN_CENTRE)
		TitleText.SetFont(wx.Font(12, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
		V_Sizer.Add(TitleText, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		V_Sizer.Add(wx.StaticLine(self, -1, (-1,-1), (140,1), wx.HORIZONTAL), 0, wx.ALIGN_CENTER)
	
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		LoadExtGpFileBtn = wx.BitmapButton(self, ID_LoadExtGpFile, wx.Bitmap(os.path.join(self.ParentalTreeFrame.Configurator.ImgDir, "OpenIcon30.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		SaveExtGpFileBtn = wx.BitmapButton(self, ID_SaveExtGpFile, wx.Bitmap(os.path.join(self.ParentalTreeFrame.Configurator.ImgDir, "SaveGpFile.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		LoadAnnoDictBtn = wx.BitmapButton(self, ID_LoadAnnoDict, wx.Bitmap(os.path.join(self.ParentalTreeFrame.Configurator.ImgDir, "CheckList.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		
		H_Sizer.Add(LoadExtGpFileBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(SaveExtGpFileBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(LoadAnnoDictBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 0)
		
		self.ShowExternalDataCB = wx.CheckBox(self, -1, "Show ExternalData")
		
		V_Sizer.AddSpacer(3)
		V_Sizer.Add(self.ShowExternalDataCB, 0, wx.ALIGN_LEFT|wx.LEFT, 10)
		V_Sizer.AddSpacer(3)

		self.SetSizer(V_Sizer)
		
		# Event Bindings
		self.Bind(wx.EVT_BUTTON, self.OnLoadExtGpFileBtn, LoadExtGpFileBtn)
		self.Bind(wx.EVT_BUTTON, self.OnSaveExtGpFileBtn, SaveExtGpFileBtn)
		self.Bind(wx.EVT_BUTTON, self.OnLoadAnnoDictBtn, LoadAnnoDictBtn)

		self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.ShowExternalDataCB)
		
	def OnLoadExtGpFileBtn(self, event):
		dlg = wx.FileDialog(self, "Open a Local Group/Annotation file", wildcard = "xml files (*.xml)|*.xml|All files (*.*)|*.*", style = wx.OPEN|wx.FD_CHANGE_DIR)
		DlgResponse = dlg.ShowModal()
		FilePath = dlg.GetPath()
		FileName = dlg.GetFilename()
		dlg.Destroy()
		if DlgResponse == wx.ID_OK and FileName != "": 
			try :
				self.ParentalTreeFrame.ExternalData = ExtDataStruct(FileName)
				self.ParentalTreeFrame.CurrentTreeObj.AddExternalData(self.ParentalTreeFrame.ExternalData)
			except:
				Errordlg = wx.MessageDialog(self, 'Unable to Load file','Error',wx.OK | wx.ICON_ERROR)
				self.ParentalTreeFrame.ExternalData = False
				Errordlg.ShowModal()
				Errordlg.Destroy()

	def OnSaveExtGpFileBtn(self, event):
		dlg = wx.FileDialog(self, "Save Local Group/Annotation file", wildcard = "xml files (*.xml)|*.xml|All files (*.*)|*.*", style = wx.SAVE|wx.OVERWRITE_PROMPT|wx.FD_CHANGE_DIR)
		dlg.SetFilename(self.ParentalTreeFrame.TreeFileName.split('.')[0])
		DlgResponse = dlg.ShowModal()
		FilePath = dlg.GetPath()
		FileName = dlg.GetFilename()
		dlg.Destroy()
		if DlgResponse == wx.ID_OK and FileName != "": 
			try :
				ofObj = open(FilePath,"w")
				self.ParentalTreeFrame.ExternalData.ExtractXMLdata(ofObj)
				ofObj.close()
			except:
				Errordlg = wx.MessageDialog(self, 'Unable to save file','Error',wx.OK | wx.ICON_ERROR)
				Errordlg.ShowModal()
				Errordlg.Destroy()

	def OnLoadAnnoDictBtn(self, event):
		dlg = wx.FileDialog(self, "Open Annotation Dictionary file", wildcard = "All files (*.*)|*.*", style = wx.OPEN|wx.FD_CHANGE_DIR)
		DlgResponse = dlg.ShowModal()
		FilePath = dlg.GetPath()
		FileName = dlg.GetFilename()
		dlg.Destroy()
		if DlgResponse == wx.ID_OK and FileName != "": 
			try :
				self.ParentalTreeFrame.AnnotationDict = []
				ifObj = open(FileName,'r')
				for line in ifObj:
					self.ParentalTreeFrame.AnnotationDict.append(line.strip('\r\n'))
				ifObj.close()
			
			except:
				Errordlg = wx.MessageDialog(self, 'Unable to Load file','Error',wx.OK | wx.ICON_ERROR)
				self.ParentalTreeFrame.ExternalData = False
				Errordlg.ShowModal()
				Errordlg.Destroy()
	
	def EvtCheckBox(self, event):
		self.ParentalTreeFrame.Configurator.ShowExternalData = self.ShowExternalDataCB.GetValue()
		print "ExternalDataLib:199 - Refreshing"
		self.ParentalTreeFrame.RefreshTreePanel()


	
class ExternalDataPopUpMenu(wx.Menu):
	def __init__(self, ClickedPart, CurrentTreeObj, ExternalData):
		wx.Menu.__init__(self)
		
		self.ClickedPart = ClickedPart
		self.ExternalData = ExternalData
		self.EventIDtoGroupObj = {}

		self.SelectedLeafsList =	[]
		self.PresentGroupList = []
		# Listing All Selected Leafs 
		if self.ClickedPart.IsNode():
			for Leaf in CurrentTreeObj.GetAllLeafsOfNode(self.ClickedPart):
				if Leaf not in self.SelectedLeafsList and Leaf.IsActive(): 
					self.SelectedLeafsList.append(Leaf)
		elif self.ClickedPart.IsLeaf():
			self.SelectedLeafsList.append(self.ClickedPart)
		
		# Listing all Groups present in tree
		for Leaf in CurrentTreeObj.GetAllLeafList():
			for Group in Leaf.LabelData.ExternalGroups:
				if Group not in self.PresentGroupList : self.PresentGroupList.append(Group)

		# Generating Group Option Menu 
		NewID = wx.NewId()
		self.Append(NewID, "Add Annotations")
		wx.EVT_MENU(self, NewID, self.AddSingleAnnotations)
		
		ListofLocalID = []
		for Leaf in self.SelectedLeafsList:
			if Leaf.LabelData.LocalID : ListofLocalID.append(str(Leaf.LabelData.LocalID))
		
		SingleAnnotationsList = self.ExternalData.GetSingleAnnotations(ListofLocalID)
		if len(SingleAnnotationsList) > 0:
			SubMenuTitle = "Remove Annotations"
			SubMenu = wx.Menu()
			for Annotation in SingleAnnotationsList:
				NewID = wx.NewId()
				SubMenu.Append(NewID, Annotation)
				wx.EVT_MENU(SubMenu, NewID, self.RemoveSingleAnnotations)
				self.EventIDtoGroupObj[str(NewID)] = Annotation

			self.AppendMenu(wx.NewId(), SubMenuTitle, SubMenu)

		SubMenuTitle = "Add this to"
		if len(self.SelectedLeafsList)>1: SubMenuTitle = "Add All to"
		SubMenu = wx.Menu()
		for Group in self.PresentGroupList:
			NewID = wx.NewId()
			SubMenu.Append(NewID, Group.GpName)
			wx.EVT_MENU(SubMenu, NewID, self.AddMemberToGroup)
			self.EventIDtoGroupObj[str(NewID)] = Group

		NewID = wx.NewId()
		SubMenu.Append(NewID, "an other Group")
		wx.EVT_MENU(SubMenu, NewID, self.AddMemberToGroup)
		self.EventIDtoGroupObj[str(NewID)] = False

		self.AppendMenu(wx.NewId(), SubMenuTitle, SubMenu)
		
		SubMenuTitle = "Remove this from"
		if len(self.SelectedLeafsList)>1: SubMenuTitle = "Remove All from"
		SubMenu = wx.Menu()
		for Group in self.PresentGroupList:
			NewID = wx.NewId()
			SubMenu.Append(NewID, Group.GpName)
			wx.EVT_MENU(SubMenu, NewID, self.RemoveMemberFromGroup)
			self.EventIDtoGroupObj[str(NewID)] = Group
		self.AppendMenu(wx.NewId(), SubMenuTitle, SubMenu)
		
		SubMenuTitle = "Edit Group Annotations"
		SubMenu = wx.Menu()
		for Group in self.PresentGroupList:
			NewID = wx.NewId()
			SubMenu.Append(NewID, Group.GpName)
			wx.EVT_MENU(SubMenu, NewID, self.EditGroup)
			self.EventIDtoGroupObj[str(NewID)] = Group
		self.AppendMenu(wx.NewId(), SubMenuTitle, SubMenu)

	def AddMemberToGroup(self, event):
		print "AddMemberToGroup"
		# If the method is called with Group set as False, create a new group
		if self.EventIDtoGroupObj[str(event.GetId())]:
			for Leaf in self.SelectedLeafsList:
				if Leaf.LabelData.LocalID:
					self.EventIDtoGroupObj[str(event.GetId())].AddMember(Leaf.LabelData.LocalID)
				else:
					print "Leaf doesn't have LocalID"
			self.GetParent().parent.GetParent().UpdateDataInTreeObj()
			self.GetParent().parent.GetParent().RefreshTreePanel()
		else :
			dlg = ExternalNewGroupDialog(self.GetParent().parent.GetParent())
			if dlg.ShowModal() == wx.ID_OK:
				GpNameAlreadyUsed = False
				for Group in self.ExternalData:
					if Group.GpName == dlg.ChosenName.GetValue() : GpNameAlreadyUsed = True
				if GpNameAlreadyUsed:
					Errordlg = wx.MessageDialog(self, 'This name already exists','Error',wx.OK | wx.ICON_ERROR)
					Errordlg.ShowModal()
					Errordlg.Destroy()

				elif dlg.ChosenName.GetValue() == '':
					Errordlg = wx.MessageDialog(self, 'Empty names are forbidden','Error',wx.OK | wx.ICON_ERROR)
					Errordlg.ShowModal()
					Errordlg.Destroy()
				
				else:
					NewGroup = self.ExternalData.AddGroup(dlg.ChosenName.GetValue())
					for Leaf in self.SelectedLeafsList:
						if Leaf.LabelData.LocalID:
							NewGroup.AddMember(Leaf.LabelData.LocalID)
					self.GetParent().parent.GetParent().UpdateDataInTreeObj()
					self.GetParent().parent.GetParent().RefreshTreePanel()
				
			dlg.Destroy()
		
	def RemoveMemberFromGroup(self, event):
		print "RemoveMemberFromGroup"
		for Leaf in self.SelectedLeafsList:
			if Leaf.LabelData.LocalID:
				self.EventIDtoGroupObj[str(event.GetId())].RemoveMember(Leaf.LabelData.LocalID)
		self.GetParent().parent.GetParent().UpdateDataInTreeObj()
		self.GetParent().parent.GetParent().RefreshTreePanel()


	def EditGroup(self, event):
		dlg = ModifExternalGroupAnnotations(self.GetParent().parent.GetParent(), self.EventIDtoGroupObj[str(event.GetId())])
		if dlg.ShowModal() == wx.ID_OK:
			self.EventIDtoGroupObj[str(event.GetId())].UpdateAnnotations(dlg.AnnotationsList.GetValue())
		dlg.Destroy()

		self.GetParent().parent.GetParent().UpdateDataInTreeObj()
		self.GetParent().parent.GetParent().RefreshTreePanel()

	def AddSingleAnnotations(self, event):
		dlg = AddSingleAnnotationDialog(self.GetParent().parent.GetParent())
		if dlg.ShowModal() == wx.ID_OK:
			ListofLocalID = []
			for Leaf in self.SelectedLeafsList:
				if Leaf.LabelData.LocalID : ListofLocalID.append(str(Leaf.LabelData.LocalID))
			
			self.ExternalData.AddSingleAnnotations(ListofLocalID, dlg.AnnotationsList.GetValue())	

			dlg.Destroy()

		self.GetParent().parent.GetParent().UpdateDataInTreeObj()
		self.GetParent().parent.GetParent().RefreshTreePanel()

	def RemoveSingleAnnotations(self, event):
		ListofLocalID = []
		for Leaf in self.SelectedLeafsList:
			if Leaf.LabelData.LocalID: ListofLocalID.append(str(Leaf.LabelData.LocalID))
				
		self.ExternalData.RemoveSingleAnnotations(ListofLocalID, self.EventIDtoGroupObj[str(event.GetId())])
		
		self.GetParent().parent.GetParent().UpdateDataInTreeObj()
		self.GetParent().parent.GetParent().RefreshTreePanel()		

class ExternalNewGroupDialog(wx.Dialog):
	def __init__(self, parent):
		wx.Dialog.__init__(self, parent, -1, "Create a new external group", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		# Variables
		GlobalSizer = wx.BoxSizer(wx.VERTICAL)
		
		GlobalSizer.Add(wx.StaticText(self, -1, "Name of the group"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
		self.ChosenName = wx.TextCtrl(self, -1, "", size=(100, -1))
		GlobalSizer.Add(self.ChosenName, 0, wx.ALIGN_CENTER|wx.ALL, 2)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		btn = wx.Button(self, wx.ID_CANCEL)
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		
		GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		self.SetSizer(GlobalSizer)
		GlobalSizer.Fit(self)

		self.ChosenName.SetFocus()

				
class ModifExternalGroupAnnotations(wx.Dialog):
	def __init__(self, parent, Group):
		wx.Dialog.__init__(self, parent, -1, "Modify Annotations of a group", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		# Variables
		GlobalSizer = wx.BoxSizer(wx.VERTICAL)
		
		GlobalSizer.Add(wx.StaticText(self, -1, "Annotations list (comma sep)"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
				
		AnnotationsListGenerated = ''
		for Annotation in Group.Annotations:
			AnnotationsListGenerated += Annotation+","
		AnnotationsListGenerated = AnnotationsListGenerated.strip(',')
		
		self.AnnotationsList = wx.TextCtrl(self, -1, AnnotationsListGenerated, size=(300, -1))
		GlobalSizer.Add(self.AnnotationsList, 0, wx.ALIGN_CENTER|wx.ALL, 2)
		
		if parent.AnnotationDict:
			GlobalSizer.Add(wx.StaticText(self, -1, "Use Dictionnary"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
			AnnotationChoice = wx.Choice(self, -1, choices = parent.AnnotationDict)
			self.Bind(wx.EVT_CHOICE, self.AnnotationChoiceAction, AnnotationChoice)
			GlobalSizer.Add(AnnotationChoice, 0, wx.ALIGN_CENTER|wx.ALL, 5)
			
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		btn = wx.Button(self, wx.ID_CANCEL)
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		
		GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		self.SetSizer(GlobalSizer)
		GlobalSizer.Fit(self)

		self.AnnotationsList.SetFocus()

	def AnnotationChoiceAction(self, event):
		if self.AnnotationsList.GetValue() == '': 
			self.AnnotationsList.SetValue(event.GetString())
		elif self.AnnotationsList.GetValue()[-1] != ',' :
			self.AnnotationsList.SetValue(self.AnnotationsList.GetValue()+','+event.GetString())
		else:
			self.AnnotationsList.SetValue(self.AnnotationsList.GetValue()+event.GetString())

class AddSingleAnnotationDialog(wx.Dialog):
	def __init__(self, parent):
		wx.Dialog.__init__(self, parent, -1, "Add annotation(s) on sequence(s)", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		# Variables
		GlobalSizer = wx.BoxSizer(wx.VERTICAL)
		
		self.AnnotationsList = wx.TextCtrl(self, -1, "", size=(200, -1))
		GlobalSizer.Add(self.AnnotationsList, 0, wx.ALIGN_CENTER|wx.ALL, 2)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)		
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		btn = wx.Button(self, wx.ID_CANCEL)
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		
		GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		self.SetSizer(GlobalSizer)
		GlobalSizer.Fit(self)

		self.AnnotationsList.SetFocus()