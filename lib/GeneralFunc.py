# -*- coding: iso-8859-1 -*-

from wx import Timer
from os.path import split

class FileTimer(Timer):
	def __init__(self, SwapObj, MainFrame):
		Timer.__init__(self)
		self.SwapObj = SwapObj
		self.MainFrame = MainFrame
		
	def Notify(self):
		data = self.SwapObj.readline()
		if data.find('OPEN:')==0: 
			FileToOpen = data.split('OPEN:')[1].strip('\0')
			FileToOpen = FileToOpen.strip(' ')
			self.SwapObj.seek(0)
			self.SwapObj.write("DONE")
			#print FileToOpen
			#print split(FileToOpen)[1]
			self.MainFrame.OpenTree(FileToOpen, split(FileToOpen)[1])
			
		elif data.find('RUNNING')==0: 
			#print "I am running"
			self.SwapObj.seek(0)
			self.SwapObj.write("YES")
			
		self.SwapObj.seek(0)
		#if data != 'Done' and data != 'Wait':
		#	try :
		#		self.GetParent
		
