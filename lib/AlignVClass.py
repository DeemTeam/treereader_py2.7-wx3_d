#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import wx, os, math, DbClass
#from BioClasses.Trees import TreeBase
import TreeBase as Trees
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4 as A4size
from reportlab.lib.colors import HexColor

#class SequenceEntry(object):
#	
#	
#	


class MyAlnFrame(wx.Frame):
	def __init__(self, parent):
		wx.Frame.__init__(self, parent, -1, "Multi Align", wx.DefaultPosition, (1000,700), wx.DEFAULT_FRAME_STYLE | wx.CLIP_CHILDREN | wx.FRAME_FLOAT_ON_PARENT)
		self.SetMinSize(wx.Size(1000,700))
		
		splitter = wx.SplitterWindow(self, -1, style = wx.SP_LIVE_UPDATE)
		
		#sty = wx.BORDER_NONE
		#sty = wx.BORDER_SIMPLE
		sty = wx.BORDER_SUNKEN
		
		p1 = wx.Window(splitter, style=sty)
		p1.SetBackgroundColour("pink")
		wx.StaticText(p1, -1, "Panel One", (5,5))

		p2 = wx.Window(splitter, style=sty)
		p2.SetBackgroundColour("sky blue")
		wx.StaticText(p2, -1, "Panel Two", (5,5))

		splitter.SetMinimumPaneSize(20)
		splitter.SplitVertically(p1, p2, -100)
		
		
		


#class MySplitter(wx.SplitterWindow):
#    def __init__(self, parent, ID, log):
#        wx.SplitterWindow.__init__(self, parent, ID,
#                                   style = wx.SP_LIVE_UPDATE
#                                   )
#        self.log = log
#        
#        self.Bind(wx.EVT_SPLITTER_SASH_POS_CHANGED, self.OnSashChanged)
#        self.Bind(wx.EVT_SPLITTER_SASH_POS_CHANGING, self.OnSashChanging)

#    def OnSashChanged(self, evt):
#        self.log.WriteText("sash changed to %s\n" % str(evt.GetSashPosition()))

#    def OnSashChanging(self, evt):
#        self.log.WriteText("sash changing to %s\n" % str(evt.GetSashPosition()))
#        # uncomment this to not allow the change
#        #evt.SetSashPosition(-1)


##---------------------------------------------------------------------------

#def runTest(frame, nb, log):
#    splitter = MySplitter(nb, -1, log)

#    #sty = wx.BORDER_NONE
#    #sty = wx.BORDER_SIMPLE
#    sty = wx.BORDER_SUNKEN
#    
#    p1 = wx.Window(splitter, style=sty)
#    p1.SetBackgroundColour("pink")
#    wx.StaticText(p1, -1, "Panel One", (5,5))

#    p2 = wx.Window(splitter, style=sty)
#    p2.SetBackgroundColour("sky blue")
#    wx.StaticText(p2, -1, "Panel Two", (5,5))

#    splitter.SetMinimumPaneSize(20)
#    splitter.SplitVertically(p1, p2, -100)

#    return splitter

