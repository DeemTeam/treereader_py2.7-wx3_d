#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

# lib files for tree reader
# Root __init__.py file
__version__ = "0.1.0"

import os
from sys import exit

def ChooseOneOrMultiFile():
	One_or_multi = raw_input("Apply on one file or several files?? (O/M): ")
	if One_or_multi == "O" :
		file_name = raw_input("Enter the source file: ")
		files_to_treat = [file_name]
		return files_to_treat
	elif One_or_multi == "M" :
		files_extension = raw_input("Enter the file extension to use : ")
		# retrieve a list folder_dir of all files in the current folder
		CurrentFolder = os.getcwd()
		folder_files = os.listdir(CurrentFolder)
		folder_files = sorted(folder_files)
		# retrieve only the files with the correct extension
		files_to_treat = []
		for filename in folder_files:
			if filename.endswith(files_extension):
				files_to_treat.append(filename)
		print '\n', len(files_to_treat), 'files to treat'
		return files_to_treat
	else :
		print "Choix impossible - End of script"
		exit()
		
def GetFilePrefix(filename):
	return '.'.join(filename.split('.')[:-1])

class Codon(object):
	def __init__(self, AAname, AAshort, AAletter, codon):
		self.AAname = AAname
		self.AAshort = AAshort
		self.AAletter = AAletter
		self.codon = codon

class GeneticTable(dict):
	def __init__(self, type=1, direction='Codon2letter'):
		# Variables
		if type == 1 :
			# List to use to build oblect
			RawList = [
			['Alanine','Ala','A',['GCT','GCC','GCA','GCG']],
			['Arginine','Arg','R',['CGT','CGC','CGA','CGG','AGA','AGG']],
			['Asparagine','Asn','N',['AAT','AAC']],
			['Aspartic acid','Asp','D',['GAT','GAC']],
			['Cysteine','Cys','C',['TGT','TGC']],
			['Glutamine','Gln','Q',['CAA','CAG']],
			['Glutamic acid','Glu','E',['GAA','GAG']],
			['Glycine','Gly','G',['GGT','GGC','GGA','GGG']],
			['Histidine','His','H',['CAT','CAC']],
			['Isoleucine','Ile','I',['ATT','ATC','ATA']],
			['Leucine','Leu','L',['TTA','TTG','CTT','CTC','CTA','CTG']],
			['Lysine','Lys','K',['AAA','AAG']],
			['Méthionine','Met','M',['ATG']],
			['Phenylalanine','Phe','F',['TTT','TTC']],
			['Proline','Pro','P',['CCT','CCC','CCA','CCG']],
			['Serine','Ser','S',['TCT','TCC','TCA','TCG','AGT','AGC']],
			['Threonine','Thr','T',['ACT','ACC','ACA','ACG']],
			['Tryptophane','Trp','W',['TGG']],
			['Tyrosine','Tyr','Y',['TAT','TAC']],
			['Valine','Val','V',['GTT','GTC','GTA','GTG']],
			['Ambre','Stp','*',['TAG']],
			['Ocre','Stp','*',['TAA']],
			['Opale','Stp','*',['TGA']],]

		if direction == 'Codon2letter':
			for AAdata in RawList:
				Current_AAname = AAdata[0]
				Current_AAshort = AAdata[1]
				Current_AAletter = AAdata[2]
				for AAcodon in AAdata[3]:
					self[AAcodon] = Codon(Current_AAname, Current_AAshort, Current_AAletter, AAcodon)
		
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def NumLinesInFile(fname):
	with open(fname) as f:
		for i, l in enumerate(f):
			pass
	return i + 1
        
class taxDATA(object):
	def __init__(self, taxID=0):
		self.taxID = taxID
		self.ChildNames = []
		self.ChildObjs = []

	def getChildObjFromName(self, queryName):
		try : return self.ChildObjs[self.ChildNames.index(queryName)]
		except : return False

	def AddChild(self, ChildName, ChildObj):
		self.ChildNames.append(ChildName)
		self.ChildObjs.append(ChildObj)
		return ChildObj
