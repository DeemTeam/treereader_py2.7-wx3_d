# -*- coding: iso-8859-1 -*-

import wx, os, math, DbClass, ExternalDataLib
#import BioClasses.Trees as Trees #import Trees
import TreeBase as Trees
from BioClasses.Sequences import MyFasta
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4 as A4size
from reportlab.lib.colors import Color as rpColor, HexColor


class MyTreeFrame(wx.Frame):
    def __init__(self, parent, Configurator, TreeFile, TreeFileName):
        wx.Frame.__init__(self, parent, -1, TreeFile ,wx.DefaultPosition, (1000,700), wx.DEFAULT_FRAME_STYLE | wx.CLIP_CHILDREN | wx.FRAME_FLOAT_ON_PARENT)
        self.SetMinSize(wx.Size(1000,700))

        self.Bind(wx.EVT_KEY_UP, self.OnKeyUp)

        # Opening tree File
        ifObj = open(TreeFile,'r')
        all_text = ''
        for line in ifObj:
            if len(line.strip(' \r\n'))!=0 : all_text += line.strip(' \r\n').replace(';',';µ')
        ifObj.close()
        self.TreesList = all_text.split('µ')
        if self.TreesList.count('') : self.TreesList.remove('')

        # Variable for operation
        self.Configurator = Configurator
        self.TreeFileName = TreeFileName
        self.ReferenceFasta = False
        self.ExternalData = False
        self.AnnotationDict = False

        # Tree Browsing
        self.CurrentTreeNum = 1

        # Instanciating the current tree
        self.CurrentTreeObj = Trees.Arbre(self.TreesList[self.CurrentTreeNum-1])
        # Adding data to the tre and update its display content
        self.UpdateDataInTreeObj()
        self.CurrentTreeObj.UpdateLeafConfiguration(self.Configurator)

        # Drawing the frame
        H_Sizer = wx.BoxSizer(wx.HORIZONTAL)

        self.LeftPanel = TunningPanel(self)
        self.RightPanel = MyTreeWindow(self)

        H_Sizer.Add(self.LeftPanel, 0, wx.EXPAND)
        H_Sizer.Add(self.RightPanel, 1, wx.EXPAND|wx.ALL , 0)

        self.SetSizer(H_Sizer)
        H_Sizer.Fit(self)

    def SwitchTree(self, Action):
        ExTreeNum = self.CurrentTreeNum
        if Action == "First":
            self.CurrentTreeNum = 1
        elif Action == "Back":
            if self.CurrentTreeNum - 1 >= 1 : self.CurrentTreeNum -= 1
        elif Action == "Fwd":
            if self.CurrentTreeNum + 1 <= len(self.TreesList) : self.CurrentTreeNum += 1
        elif Action == "Last":
            self.CurrentTreeNum = len(self.TreesList)
        else:
            if  1 < int(Action) < len(self.TreesList) : self.CurrentTreeNum = int(Action)
            else :
                Errordlg = wx.MessageDialog(self, 'Number is out of range','Error',wx.OK | wx.ICON_ERROR)
                Errordlg.ShowModal()
                Errordlg.Destroy()
                self.LeftPanel.CurrentTreeNumber.SetValue(str(self.CurrentTreeNum))
        if ExTreeNum != self.CurrentTreeNum:
            self.LeftPanel.CurrentTreeNumber.SetValue(str(self.CurrentTreeNum))

            # Instanciating the current tree
            self.CurrentTreeObj = Trees.Arbre(self.TreesList[self.CurrentTreeNum-1])
            # Add Colors if enabled
            # Adding data to the tree
            self.UpdateDataInTreeObj()
            # update its display content by refreshing the panel
            self.RefreshTreePanel()

    def OnKeyUp(self, evt):
        code = evt.GetKeyCode()
        if code == wx.WXK_LEFT:
            self.SwitchTree("Back")
        elif code == wx.WXK_RIGHT:
            self.SwitchTree("Fwd")
        else:
            evt.Skip()

    def RefreshTreePanel(self):
        self.CurrentTreeObj.UpdateLeafConfiguration(self.Configurator)
        self.RightPanel.DrawTree()
        self.RightPanel.SetFocus()

    def UpdateDataInTreeObj(self):
        # Add Colors if enabled
        if self.Configurator.UseColors:
            self.CurrentTreeObj.AddColors(self.Configurator)
        # Add DB data if enabled
        if self.Configurator.UseDB:
            self.CurrentTreeObj.AddDatabaseInfo(self.Configurator.db, self.Configurator.UserID)
        # Add External Data if enabled
        if self.ExternalData:
            self.CurrentTreeObj.AddExternalData(self.ExternalData)

    def OrderNodes(self):
        #print self.CurrentTreeObj.DrawInText()
        self.CurrentTreeObj.OrderNodes()
        #print self.CurrentTreeObj.DrawInText()
        self.CurrentTreeObj.RecursiveDrawLoop()
        self.RefreshTreePanel()

class TunningPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, wx.DefaultPosition, (150,700))
        # IDs Generation
        ID_SPIN_LEAVES_FONT = wx.NewId()
        ID_SPIN_NODES_FONT = wx.NewId()
        ID_FirstBtn = wx.NewId()
        ID_BackBtn = wx.NewId()
        ID_FwdBtn = wx.NewId()
        ID_LastBtn = wx.NewId()

        ID_OrderNodes = wx.NewId()

        ID_SaveNewickBtn = wx.NewId()
        ID_SavePDFBtn = wx.NewId()
        ID_SaveMultiPDFBtn = wx.NewId()

        ID_LoadExtFasta = wx.NewId()
        ID_SaveSubFasta = wx.NewId()

        # Variable for operation
        self.RefreshNeeded = False

        V_Sizer = wx.BoxSizer(wx.VERTICAL)
        TitleText = wx.StaticText(self, label = "Browse Trees", style = wx.ALIGN_CENTRE)
        TitleText.SetFont(wx.Font(12, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
        V_Sizer.Add(TitleText, 0, wx.ALIGN_CENTER|wx.ALL, 1)
        V_Sizer.Add(wx.StaticLine(self, -1, (-1,-1), (140,1), wx.HORIZONTAL), 0, wx.ALIGN_CENTER)

        H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
        FirstBtn = wx.BitmapButton(self, ID_FirstBtn, wx.Bitmap(os.path.join(self.GetParent().Configurator.ImgDir, "FirstBtn.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
        BackBtn = wx.BitmapButton(self, ID_BackBtn, wx.Bitmap(os.path.join(self.GetParent().Configurator.ImgDir, "BackBtn.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
        FwdBtn = wx.BitmapButton(self, ID_FwdBtn, wx.Bitmap(os.path.join(self.GetParent().Configurator.ImgDir, "FwdBtn.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
        LastBtn = wx.BitmapButton(self, ID_LastBtn, wx.Bitmap(os.path.join(self.GetParent().Configurator.ImgDir, "LastBtn.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
        H_Sizer.Add(FirstBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
        H_Sizer.Add(BackBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
        H_Sizer.Add(FwdBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
        H_Sizer.Add(LastBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
        V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 1)

        H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.CurrentTreeNumber = wx.TextCtrl(self, -1, str(self.GetParent().CurrentTreeNum), size=(50, -1), style=wx.TE_RIGHT | wx.TE_PROCESS_ENTER)
        TotalTreeNumber = wx.StaticText(self, label = " on " + str(len(self.GetParent().TreesList)))
        H_Sizer.Add(self.CurrentTreeNumber, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        H_Sizer.Add(TotalTreeNumber, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER | wx.ALL, 1)

        V_Sizer.AddSpacer(10)

        TitleText = wx.StaticText(self, label = "Fonts", style = wx.ALIGN_CENTRE)
        TitleText.SetFont(wx.Font(12, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
        V_Sizer.Add(TitleText, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        V_Sizer.Add(wx.StaticLine(self, -1, (-1,-1), (140,1), wx.HORIZONTAL), 0, wx.ALIGN_CENTER)

        H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
        FontTitleText = wx.StaticText(self, label = "Leaves Font Size")
        FontTitleText.SetFont(wx.Font(10, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
        H_Sizer.Add(FontTitleText, 0, wx.ALIGN_LEFT | wx.ALL, 1)
        self.LeafFontSpin = wx.SpinCtrl(self, ID_SPIN_LEAVES_FONT, size=(45,18))
        self.LeafFontSpin.SetRange(6,16)
        self.LeafFontSpin.SetValue(self.GetParent().Configurator.LeafFont.GetPointSize())
        H_Sizer.Add(self.LeafFontSpin, 0, wx.ALIGN_RIGHT | wx.ALL, 1)
        V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER | wx.ALL, 1)

        H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
        FontTitleText = wx.StaticText(self, label = "Nodes Font Size")
        FontTitleText.SetFont(wx.Font(10, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
        H_Sizer.Add(FontTitleText, 0, wx.ALIGN_LEFT | wx.ALL, 1)
        self.NodesFontSpin = wx.SpinCtrl(self, ID_SPIN_NODES_FONT, size=(45,18))
        self.NodesFontSpin.SetRange(6,14)
        self.NodesFontSpin.SetValue(self.GetParent().Configurator.NodeFont.GetPointSize())
        H_Sizer.Add(self.NodesFontSpin, 0, wx.ALIGN_RIGHT | wx.ALL, 1)
        V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER | wx.ALL, 1)

        # ------------ Reorder button --------
        H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
        OrderNodesBtn = wx.Button(self, ID_OrderNodes, "Order Nodes",wx.DefaultPosition, wx.DefaultSize)
        H_Sizer.Add(OrderNodesBtn, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER | wx.ALL, 0)

        V_Sizer.AddSpacer(10)

        #------ Saving and printing part
        TitleText = wx.StaticText(self, label = "Save and Print", style = wx.ALIGN_CENTRE)
        TitleText.SetFont(wx.Font(12, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
        V_Sizer.Add(TitleText, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        V_Sizer.Add(wx.StaticLine(self, -1, (-1,-1), (140,1), wx.HORIZONTAL), 0, wx.ALIGN_CENTER)

        H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
        SaveNewickBtn = wx.BitmapButton(self, ID_SaveNewickBtn, wx.Bitmap(os.path.join(self.GetParent().Configurator.ImgDir, "SaveNewick.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
        SavePDFBtn = wx.BitmapButton(self, ID_SavePDFBtn, wx.Bitmap(os.path.join(self.GetParent().Configurator.ImgDir, "SavePDF.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
        SaveMultiPDFBtn = wx.BitmapButton(self, ID_SaveMultiPDFBtn, wx.Bitmap(os.path.join(self.GetParent().Configurator.ImgDir, "SavePDFmultiP.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)


        H_Sizer.Add(SaveNewickBtn, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        H_Sizer.Add(SavePDFBtn, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        H_Sizer.Add(SaveMultiPDFBtn, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER | wx.ALL, 0)

        #------ Use External Fasta Part
        TitleText = wx.StaticText(self, label = "Use ext. Fasta", style = wx.ALIGN_CENTRE)
        TitleText.SetFont(wx.Font(12, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
        V_Sizer.Add(TitleText, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        V_Sizer.Add(wx.StaticLine(self, -1, (-1,-1), (140,1), wx.HORIZONTAL), 0, wx.ALIGN_CENTER)

        H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
        LoadExtFastaBtn = wx.BitmapButton(self, ID_LoadExtFasta, wx.Bitmap(os.path.join(self.GetParent().Configurator.ImgDir, "OpenIcon30.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
        SaveSubFastaBtn = wx.BitmapButton(self, ID_SaveSubFasta, wx.Bitmap(os.path.join(self.GetParent().Configurator.ImgDir, "SaveFasta.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)

        H_Sizer.Add(LoadExtFastaBtn, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        H_Sizer.Add(SaveSubFastaBtn, 0, wx.ALIGN_CENTER | wx.ALL, 1)
        V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER | wx.ALL, 0)

        #------ Use External GP Part
        V_Sizer.AddSpacer(10)
        V_Sizer.Add(ExternalDataLib.LocalGpPanel(self), 0, wx.ALIGN_CENTER | wx.ALL, 0)

        #------ Use DB Data Part
        if self.GetParent().Configurator.UseDB:
            V_Sizer.AddSpacer(10)
            V_Sizer.Add(DbClass.DbPanel(self), 0, wx.ALIGN_CENTER | wx.ALL, 0)

        self.SetSizer(V_Sizer)

        #Event Bindings
        self.Bind(wx.EVT_TEXT_ENTER, self.OnCurrentTreeNumberEnter, self.CurrentTreeNumber)

        self.Bind(wx.EVT_BUTTON, self.OnFirstBtn, id=ID_FirstBtn)
        self.Bind(wx.EVT_BUTTON, self.OnBackBtn, id=ID_BackBtn)
        self.Bind(wx.EVT_BUTTON, self.OnFwdBtn, id=ID_FwdBtn)
        self.Bind(wx.EVT_BUTTON, self.OnLastBtn, id=ID_LastBtn)

        self.Bind(wx.EVT_SPINCTRL, self.OnNodesFontSpin, id=ID_SPIN_NODES_FONT)
        self.Bind(wx.EVT_SPINCTRL, self.OnLeafFontSpin, id=ID_SPIN_LEAVES_FONT)

        self.Bind(wx.EVT_BUTTON, self.OnOrderNodes, id=ID_OrderNodes)

        self.Bind(wx.EVT_BUTTON, self.OnSaveNewickBtn, SaveNewickBtn)
        self.Bind(wx.EVT_BUTTON, self.OnSavePDFBtn, SavePDFBtn)
        self.Bind(wx.EVT_BUTTON, self.OnSaveMultiPDFBtn, SaveMultiPDFBtn)

        self.Bind(wx.EVT_BUTTON, self.OnLoadExtFasta, LoadExtFastaBtn)
        self.Bind(wx.EVT_BUTTON, self.OnSubFastaBtn, SaveSubFastaBtn)

    # Saving and printing
    def OnSaveNewickBtn(self, event):
        dlg = wx.FileDialog(self, "Save Tree in Newick Format", wildcard = "ph Phylogenetic Tree (*.ph)|*.ph|tre Phylogenetic Tree (*.tre)|*.tre", style = wx.SAVE|wx.OVERWRITE_PROMPT)
        dlg.SetFilename(self.GetParent().TreeFileName.split('.')[0])
        DlgResponse = dlg.ShowModal()
        FilePath = dlg.GetPath()
        FileName = dlg.GetFilename()
        dlg.Destroy()
        if DlgResponse == wx.ID_OK and FileName != "":
            try :
                NewickString = self.GetParent().CurrentTreeObj.PrintInNewick()
                ofObj = open(FilePath,"w")
                ofObj.write(NewickString)
                ofObj.close()
            except:
                Errordlg = wx.MessageDialog(self, 'Unable to save file','Error',wx.OK | wx.ICON_ERROR)
                Errordlg.ShowModal()
                Errordlg.Destroy()

    def OnSavePDFBtn(self, event):
        dlg = wx.FileDialog(self, "Save Tree in PDF", wildcard = "PDF file (*.pdf)|*.pdf", style = wx.SAVE|wx.OVERWRITE_PROMPT)
        dlg.SetFilename(self.GetParent().TreeFileName.split('.')[0])
        DlgResponse = dlg.ShowModal()
        FilePath = dlg.GetPath()
        FileName = dlg.GetFilename()
        dlg.Destroy()
        if DlgResponse == wx.ID_OK and FileName != "":

            ActiveTree = self.GetParent().CurrentTreeObj

            #try :
            NodeFontHeight = self.GetParent().Configurator.NodeFont.GetPointSize()
            LeafFontHeight = self.GetParent().Configurator.LeafFont.GetPointSize()
            LeafSeparation = 1
            Margin = 20
            SepFromLine = 2

            # For width use A4
            Width = A4size[0]
            # For height, calculate the point size using num of leaves()
            Height = ActiveTree.NumberOfLeaf() * (LeafFontHeight + LeafSeparation) + Margin*2
            # Create Canvas So we can use it to calculate string sizes in points
            PdfCanvas = canvas.Canvas(FilePath, pagesize=(Width, Height))
            # Setting font for leaves and calculate the size ratio for branch lenghts
            PdfCanvas.setFont("Helvetica", LeafFontHeight)
            RatioX = (Width - Margin*2 - SepFromLine - PdfCanvas.stringWidth(ActiveTree.LongestSegment[1].LabelData.LabelToWrite)) / ActiveTree.LongestSegment[0]
            # Calculate shift values, don't remember what for....
            ShiftYLeaf = LeafFontHeight / 2.0
            ShiftYNode = NodeFontHeight / 2.0

            # Rolling over Segments for drawing
            for segment in ActiveTree.segments:
                PdfCanvas.setLineWidth(segment.thickness)
                PdfCanvas.line  (
                                                segment.xfrom * RatioX + Margin,
                                                (1 - segment.yfrom) * (Height-(Margin*2)) + Margin,
                                                segment.xto * RatioX + Margin,
                                                (1 - segment.yto) * (Height-(Margin*2)) + Margin
                                                )
                PdfCanvas.setLineWidth(1)

            # Rolling over Labels for drawing
            for label in ActiveTree.TreeParts:
                if label.IsLeaf():
                    PdfCanvas.setFont("Helvetica", LeafFontHeight)
                    if label.Color:
                        PdfCanvas.setFillColorRGB(label.Color[0].Red()/255.0, label.Color[0].Green()/255.0, label.Color[0].Blue()/255.0)
                        PdfCanvas.rect(
                                        label.x * RatioX + Margin + SepFromLine,
                                        (1 - label.y) * (Height-(Margin*2)) + Margin - ShiftYLeaf,
                                        PdfCanvas.stringWidth(str(label.LabelData.LabelToWrite)),
                                        LeafFontHeight, #+ ShiftYLeaf,
                                        stroke=0, fill=1
                                        )
                        PdfCanvas.setFillColorRGB(label.Color[1].Red()/255.0, label.Color[1].Green()/255.0, label.Color[1].Blue()/255.0)
                    else : PdfCanvas.setFillColorRGB(0,0,0)

                    PdfCanvas.drawString(
                                                            label.x * RatioX + Margin + SepFromLine,
                                                            (1 - label.y) * (Height-(Margin*2)) + Margin - ShiftYLeaf/2,
                                                            str(label.LabelData.LabelToWrite)
                                                            )

            # for label in ActiveTree.TreeParts:
                elif label.IsNode():
                    PdfCanvas.setFont("Helvetica", NodeFontHeight)
                    PdfCanvas.setFillColorRGB(0,0,0)

                    CurrSupport = (int(round(label.support)) if (label.support>1) else int(round(label.support)*100))
                    # Put label at branch
                    
                    NextNode = label
                    AvailableSpace = NextNode.BranchLen*RatioX
                    stop = False
                    while not stop:
                        if NextNode.parent:
                            AvailableSpace += NextNode.parent.BranchLen*RatioX
                            if NextNode.parent.isUpperChild() is NextNode.isUpperChild():
                                NextNode = NextNode.parent
                            else :
                                stop = True
                        else :
                            stop = True
                    print 'AvailableSpace', AvailableSpace

                    print "CurrSupport", CurrSupport, "AvailableSpace", AvailableSpace, "isUpperChild", label.isUpperChild(),"StringWidth",PdfCanvas.stringWidth(str(CurrSupport))
                    

                    if AvailableSpace >= 1.2*PdfCanvas.stringWidth(str(CurrSupport)):
                        if label.isUpperChild():
                            PosX = label.x * RatioX + Margin - SepFromLine - PdfCanvas.stringWidth(str(CurrSupport))
                            PosY = (1 - label.y) * (Height-(Margin*2)) + Margin + ShiftYNode
                        else:
                            PosX = label.x * RatioX + Margin - SepFromLine - PdfCanvas.stringWidth(str(CurrSupport))
                            PosY = (1 - label.y) * (Height-(Margin*2)) + Margin - 2*ShiftYNode
                    # Put label at node
                    else:
                        PosX = label.x * RatioX + Margin + SepFromLine
                        PosY = (1 - label.y) * (Height-(Margin*2)) + Margin - ShiftYNode


                    if self.GetParent().Configurator.ExcludeLabels :
                        if (CurrSupport >= 65 and CurrSupport != 100) :
                            print "not Excluded"
                            PdfCanvas.drawString(
                                                PosX,
                                                PosY,
                                                str(CurrSupport)
                                                )
                    else:
                        PdfCanvas.drawString(
                                            PosX,
                                            PosY,
                                            str(CurrSupport)
                                            )

            # Printing Len Bar
            PdfCanvas.setFont("Helvetica", LeafFontHeight)
            BarLen = round(ActiveTree.LongestSegment[0] / 5, 2) * RatioX
            BarText = str(round(ActiveTree.LongestSegment[0] / 5, 2))

            PdfCanvas.line  (
                            Width - Margin - BarLen,
                                            Margin,
                                            Width - Margin,
                                            Margin
                                            )

            PdfCanvas.setFont("Helvetica", LeafFontHeight)
            PdfCanvas.drawString(
                                                    Width - Margin - BarLen/2 - PdfCanvas.stringWidth(BarText)/2.0,
                                                    Margin + ShiftYLeaf,
                                                    BarText
                                                    )

            PdfCanvas.showPage()
            PdfCanvas.save()

            # except:
                # Errordlg = wx.MessageDialog(self, 'Unable to save file','Error',wx.OK | wx.ICON_ERROR)
                # Errordlg.ShowModal()
                # Errordlg.Destroy()

    def OnSaveMultiPDFBtn(self, event):
        dlg = wx.FileDialog(self, "Save Tree in PDF", wildcard = "PDF file (*.pdf)|*.pdf", style = wx.SAVE|wx.OVERWRITE_PROMPT)
        dlg.SetFilename(self.GetParent().TreeFileName.split('.')[0])
        DlgResponse = dlg.ShowModal()
        FilePath = dlg.GetPath()
        FileName = dlg.GetFilename()
        dlg.Destroy()
        if DlgResponse == wx.ID_OK and FileName != "":
            #try :
            NodeFontHeight = self.GetParent().Configurator.NodeFont.GetPointSize()
            LeafFontHeight = self.GetParent().Configurator.LeafFont.GetPointSize()
            LeafSeparation = 1
            Margin = 20
            SepFromLine = 2
            PdfCanvas = canvas.Canvas(FilePath, pagesize=A4size)
            Width, Height = A4size
            PdfCanvas.setFont("Helvetica", LeafFontHeight)
            RatioX = (Width - Margin*2 - SepFromLine - PdfCanvas.stringWidth(self.GetParent().CurrentTreeObj.LongestSegment[1].LabelData.LabelToWrite)) / self.GetParent().CurrentTreeObj.LongestSegment[0]
            ShiftYLeaf = LeafFontHeight/3.0
            ShiftYNode = NodeFontHeight/3.0

            NbPDFPages = math.ceil(self.GetParent().CurrentTreeObj.NumberOfLeaf() * (LeafFontHeight + LeafSeparation) / (Height - Margin*2))
            CurrentPDFPage = 0
            PageUpLimit = 1.0/NbPDFPages
            PageDownLimit = 0
            while CurrentPDFPage < NbPDFPages:
                UppestLeafY = 0.0
                #Get the modified value of the last leaf to draw
                for label in self.GetParent().CurrentTreeObj.TreeParts:
                    if label.IsLeaf() and PageDownLimit <= label.y <= PageUpLimit:
                        if label.y - PageDownLimit > UppestLeafY : UppestLeafY = label.y - PageDownLimit

                for segment in self.GetParent().CurrentTreeObj.segments:
                    if PageDownLimit <= segment.yfrom <= PageUpLimit or PageDownLimit <= segment.yto <= PageUpLimit:
                        PdfCanvas.line  (
                                                        segment.xfrom * RatioX + Margin,
                                                        (1 - ((segment.yfrom - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin,
                                                        segment.xto * RatioX + Margin,
                                                        (1 - ((segment.yto - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin
                                                        )
                    elif PageDownLimit >= segment.yfrom and segment.yto >= PageUpLimit:
                        PdfCanvas.line  (
                                                        segment.xfrom * RatioX + Margin,
                                                        (1 - ((segment.yfrom - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin,
                                                        segment.xto * RatioX + Margin,
                                                        (1 - ((segment.yto - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin
                                                        )
                for label in self.GetParent().CurrentTreeObj.TreeParts:
                    if label.IsLeaf():
                        if PageDownLimit <= label.y <= PageUpLimit :
                            PdfCanvas.setFont("Helvetica", LeafFontHeight)
                            if label.Color:
                                PdfCanvas.setFillColorRGB(label.Color[0].Red()/255.0, label.Color[0].Green()/255.0, label.Color[0].Blue()/255.0)
                                PdfCanvas.rect(
                                                                label.x * RatioX + Margin + SepFromLine,
                                                                (1 - ((label.y - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin - 2*ShiftYLeaf,
                                                                PdfCanvas.stringWidth(str(label.LabelData.LabelToWrite)),
                                                                LeafFontHeight + ShiftYLeaf,
                                                                stroke=0, fill=1
                                                                )
                                #FillColor = rpColor(label.Color[1].Red(), label.Color[1].Green(), label.Color[1].Blue(), 1)
                                #PdfCanvas.setFillColor(FillColor)
                                PdfCanvas.setFillColorRGB(label.Color[1].Red()/255.0, label.Color[1].Green()/255.0, label.Color[1].Blue()/255.0)
                            else : PdfCanvas.setFillColorRGB(0,0,0)
                            PdfCanvas.drawString(
                                                                    label.x * RatioX + Margin + SepFromLine,
                                                                    (1 - ((label.y - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin - ShiftYLeaf,
                                                                    str(label.LabelData.LabelToWrite)
                                                                    )
                for label in self.GetParent().CurrentTreeObj.TreeParts:
                    if label.IsNode():
                        if PageDownLimit <= label.y <= PageUpLimit :
                            PdfCanvas.setFont("Helvetica", NodeFontHeight)
                            PdfCanvas.setFillColorRGB(0,0,0)
                            PdfCanvas.drawString(
                                                                    label.x * RatioX + Margin + SepFromLine,
                                                                    (1 - ((label.y - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin - ShiftYNode,
                                                                    str((round(label.support) if (label.support>1) else round(label.support)*100))
                                                                    )
                            #(round(label.support) if (label.support>1) else round(label.support)*100)

                # Printing Len Bar
                BarLen = round(self.GetParent().CurrentTreeObj.LongestSegment[0] / 5, 2) * RatioX
                BarText = str(round(self.GetParent().CurrentTreeObj.LongestSegment[0] / 5, 2))

                PdfCanvas.line  (
                                                Width - Margin - BarLen,
                                                Height - Margin,
                                                Width - Margin,
                                                Height - Margin
                                                # (1 - ((segment.yfrom - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin,
                                                # segment.xto * RatioX + Margin,
                                                # (1 - ((segment.yto - PageDownLimit) * (1 / UppestLeafY))) * (Height-(Margin*2)) + Margin
                                                )

                PdfCanvas.setFont("Helvetica", LeafFontHeight)
                PdfCanvas.drawString(
                                                        Width - Margin - BarLen/2 - PdfCanvas.stringWidth(BarText),
                                                        Height - Margin - LeafFontHeight - 2,
                                                        BarText
                                                        )

                PdfCanvas.showPage()
                CurrentPDFPage += 1
                PageUpLimit += 1.0/NbPDFPages
                PageDownLimit += 1.0/NbPDFPages

            PdfCanvas.save()

            # except:
                # Errordlg = wx.MessageDialog(self, 'Unable to save file','Error',wx.OK | wx.ICON_ERROR)
                # Errordlg.ShowModal()
                # Errordlg.Destroy()


    def OnLoadExtFasta(self, event):
        dlg = wx.FileDialog(self, "Open a Reference Fasta file", wildcard = "Fasta files (*.fas;*.fasta)|*.fas;*.fasta|All files (*.*)|*.*", style = wx.OPEN|wx.FD_CHANGE_DIR)
        DlgResponse = dlg.ShowModal()
        FilePath = dlg.GetPath()
        FileName = dlg.GetFilename()
        dlg.Destroy()
        if DlgResponse == wx.ID_OK and FileName != "":
            try :
                self.GetParent().ReferenceFasta = MyFasta(dlg.GetPath())
            except:
                Errordlg = wx.MessageDialog(self, 'Unable to Load file','Error',wx.OK | wx.ICON_ERROR)
                Errordlg.ShowModal()
                Errordlg.Destroy()

    def OnSubFastaBtn(self, event):
        if self.GetParent().ReferenceFasta:
            dlg = wx.FileDialog(self, "Save A sub fasta from reference", wildcard = "Fasta files (*.fas;*.fasta)|*.fas;*.fasta|All files (*.*)|*.*", style = wx.SAVE|wx.OVERWRITE_PROMPT|wx.FD_CHANGE_DIR)
            dlg.SetFilename(self.GetParent().TreeFileName.split('.')[0])
            DlgResponse = dlg.ShowModal()
            FilePath = dlg.GetPath()
            FileName = dlg.GetFilename()
            dlg.Destroy()
            if DlgResponse == wx.ID_OK and FileName != "":
                ListOfNotFound = []
                OutputFasta = MyFasta()
                try :
                    for leaf in self.GetParent().CurrentTreeObj.GetActiveLeafs():
                        FastaEntryToFind = self.GetParent().ReferenceFasta.FindSeqByDef(leaf.LabelData.Label)
                        if FastaEntryToFind: OutputFasta.AddSequence(FastaEntryToFind)
                        else : ListOfNotFound.append(leaf.LabelData.Label)
                    ofObj = open(FilePath,"w")
                    ofObj.write(OutputFasta.FullOutputInFasta())
                    ofObj.close()
                    if len(ListOfNotFound)!=0:
                        ErrorMessage = "Unable to save these entries"
                        for NotFound in ListOfNotFound:
                            ErrorMessage += "\n" + str(NotFound)
                        Errordlg = wx.MessageDialog(self, ErrorMessage,'Error',wx.OK | wx.ICON_ERROR)
                        Errordlg.ShowModal()
                        Errordlg.Destroy()
                except:
                    Errordlg = wx.MessageDialog(self, 'Unable to save file','Error',wx.OK | wx.ICON_ERROR)
                    Errordlg.ShowModal()
                    Errordlg.Destroy()
        else:
            Errordlg = wx.MessageDialog(self, 'No Reference Fasta is defined','Error',wx.OK | wx.ICON_ERROR)
            Errordlg.ShowModal()
            Errordlg.Destroy()

    def OnFirstBtn(self, event):
        self.GetParent().SwitchTree("First")
    def OnBackBtn(self, event):
        self.GetParent().SwitchTree("Back")
    def OnFwdBtn(self, event):
        self.GetParent().SwitchTree("Fwd")
    def OnLastBtn(self, event):
        self.GetParent().SwitchTree("Last")
    def OnCurrentTreeNumberEnter(self, event):
        self.GetParent().SwitchTree(self.CurrentTreeNumber.GetValue())
    def OnNodesFontSpin(self, event):
        self.GetParent().Configurator.NodeFont.SetPointSize(int(self.NodesFontSpin.GetValue()))
        self.GetParent().RefreshTreePanel()
    def OnLeafFontSpin(self, event):
        self.GetParent().Configurator.LeafFont.SetPointSize(int(self.LeafFontSpin.GetValue()))
        self.GetParent().RefreshTreePanel()
    def OnOrderNodes(self, event):
        self.GetParent().OrderNodes()

class MyTreeWindow(wx.ScrolledWindow):
    def __init__(self, parent):
        wx.ScrolledWindow.__init__(self, parent, -1, wx.DefaultPosition, wx.DefaultSize)

        #self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)
        self.SetScrollRate(20,20)
        self.SetBackgroundColour("WHITE")

        # Variables
        self.FirstOpen = False
        self.resized = False
        self.NodeBitmap = wx.Bitmap(os.path.join(self.GetParent().Configurator.ImgDir, "NodeImg.png"), wx.BITMAP_TYPE_PNG)

        self.BlackColor = wx.Colour(0, 0, 0, wx.ALPHA_OPAQUE)
        self.WhiteColor = wx.Colour(255, 255, 255, wx.ALPHA_OPAQUE)
        self.RedColor = wx.Colour(255, 0, 0, wx.ALPHA_OPAQUE)

        self.Margin = 10
        self.SepFromLine = 5
        self.Height = 20
        self.Width = 20
        self.RatioX = 0
        self.ShiftYNode = 0
        self.ShiftYLeaf = 0
        self.buffer = wx.EmptyBitmap(self.Width,self.Height)

        # Events Binding
        self.Bind(wx.EVT_SIZE , self.OnSize)
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_ENTER_WINDOW, self.OnEnter)
        self.Bind(wx.EVT_LEFT_UP, self.OnLeftClick)
        self.Bind(wx.EVT_RIGHT_UP, self.OnRightClick)

    def OnPaint(self, event):
        dc = wx.BufferedPaintDC(self, self.buffer, wx.BUFFER_VIRTUAL_AREA)
        event.Skip()
    def OnSize(self, event):
        self.resized = True
        event.Skip()
    def OnEnter(self, event):
        if self.resized :
            self.DrawTree()
            self.resized = False

    def OnLeftClick(self, event):
        ModifiedLeafs = False
        ClickCoords = self.CalcUnscrolledPosition(event.GetX(), event.GetY())
        for part in self.GetParent().CurrentTreeObj.TreeParts:
            if part.Region.ContainsPoint(ClickCoords) == wx.InRegion:
                ModifiedLeafs = self.GetParent().CurrentTreeObj.ActivateOrDeactivatePart(part)
        if ModifiedLeafs: self.UpdateLeafs(ModifiedLeafs)

    def OnRightClick(self, event):
        ModifiedLeafs = False
        ClickCoords = self.CalcUnscrolledPosition(event.GetX(), event.GetY())
        for part in self.GetParent().CurrentTreeObj.TreeParts:
            if part.Region.ContainsPoint(ClickCoords) == wx.InRegion:
                TempPopUpMenu = DefaultTreePopUpMenu(self, part, self.GetParent().CurrentTreeObj)
                self.PopupMenu(TempPopUpMenu)
                TempPopUpMenu.Destroy()

    def DrawTree(self):
        self.Refresh()

        self.buffer = wx.EmptyBitmap(self.Width,self.Height)
        dc = wx.BufferedDC(None, self.buffer)
        dc.SetFont(self.GetParent().Configurator.LeafFont)

        # Width is always deduced from window
        self.Width = self.GetSizeTuple()[0]-20
        # Height is from window*Expansion Config
        self.Height = self.GetParent().Configurator.Expansion * self.GetParent().CurrentTreeObj.NumberOfLeaf()*dc.GetTextExtent("A")[1] + (self.Margin * 2)

        self.buffer = wx.EmptyBitmap(self.Width,self.Height)
        # try: dc = wx.GCDC(wx.BufferedDC(None, self.buffer))
        # except: dc = wx.BufferedDC(None, self.buffer)

        dc = wx.BufferedDC(None, self.buffer)

        dc.SetFont(self.GetParent().Configurator.NodeFont)
        self.ShiftYNode = dc.GetTextExtent("A")[1]/2
        dc.SetFont(self.GetParent().Configurator.LeafFont)
        self.ShiftYLeaf = dc.GetTextExtent("A")[1]/2

        # Updating Longest Segment
        self.GetParent().CurrentTreeObj.UpdateLongestSegment(dc, self.Width)

        print 'LongestSegment[1].LabelData.LabelToWrite', dc.GetTextExtent(self.GetParent().CurrentTreeObj.LongestSegment[1].LabelData.LabelToWrite)[0]
        print 'CurrentTreeObj.LongestSegment[0]', self.GetParent().CurrentTreeObj.LongestSegment[0]
        self.RatioX = (self.Width - self.Margin*2 - self.SepFromLine - dc.GetTextExtent(self.GetParent().CurrentTreeObj.LongestSegment[1].LabelData.LabelToWrite)[0]) / self.GetParent().CurrentTreeObj.LongestSegment[0]
        print 'RatioX', self.RatioX

        dc.SetPen(wx.Pen(self.BlackColor))
        dc.SetBackground(wx.Brush(self.WhiteColor))
        dc.SetBackgroundMode(wx.SOLID)

        dc.Clear()
        #dc.StartDrawing()
        # start with horizontal segments
        for segment in self.GetParent().CurrentTreeObj.segments:
            dc.DrawLine(
                                    segment.xfrom * self.RatioX + self.Margin,
                                    segment.yfrom * (self.Height-(self.Margin*2)) + self.Margin,
                                    segment.xto * self.RatioX + self.Margin,
                                    segment.yto * (self.Height-(self.Margin*2)) + self.Margin
                                    )

        for part in self.GetParent().CurrentTreeObj.TreeParts:
            if part.IsNode():
                dc.SetTextBackground(self.WhiteColor)
                dc.SetTextForeground(self.BlackColor)
                dc.SetFont(self.GetParent().Configurator.NodeFont)
                dc.DrawText(
                                        str(part.support),
                                        part.x * self.RatioX + self.Margin + self.SepFromLine,
                                        part.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYNode
                                        )
                dc.DrawBitmap(
                                        self.NodeBitmap,
                                        part.x * self.RatioX + self.Margin - 2,
                                        part.y * (self.Height-(self.Margin*2)) + self.Margin -2
                                        )
                part.Region = wx.Region(
                                                                        part.x * self.RatioX + self.Margin - 3,
                                                                        part.y * (self.Height-(self.Margin*2)) + self.Margin - 3,
                                                                        7,
                                                                        7
                                                                )

            if part.IsLeaf():
                dc.SetFont(self.GetParent().Configurator.LeafFont)
                if part.Color:
                    dc.SetTextBackground(part.Color[0])
                    dc.SetTextForeground(part.Color[1])
                else :
                    dc.SetTextBackground(self.WhiteColor)
                    dc.SetTextForeground(self.BlackColor)
                if not part.IsActive():
                    dc.SetTextBackground(self.WhiteColor)
                    dc.SetTextForeground(self.RedColor)
                dc.DrawText(
                                        str(part.LabelData.LabelToWrite),
                                        part.x * self.RatioX + self.Margin + self.SepFromLine,
                                        part.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYLeaf
                                        )
                part.Region = wx.Region(
                                                                        part.x * self.RatioX + self.Margin + self.SepFromLine,
                                                                        part.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYNode,
                                                                        dc.GetTextExtent(part.LabelData.LabelToWrite)[0],
                                                                        dc.GetTextExtent(part.LabelData.LabelToWrite)[1]
                                                                )

        # Defining scale indicator
        # print 'Choose len of bar', round(self.GetParent().CurrentTreeObj.LongestSegment[0] / 5, 2)
        # print 'len bar display', round(self.GetParent().CurrentTreeObj.LongestSegment[0] / 5, 2) * self.RatioX

        BarLen = round(self.GetParent().CurrentTreeObj.LongestSegment[0] / 5, 2) * self.RatioX
        BarText = str(round(self.GetParent().CurrentTreeObj.LongestSegment[0] / 5, 2))

        dc.DrawLine(
                                self.Width - self.Margin - BarLen,
                                self.Height - self.Margin,
                                self.Width - self.Margin,
                                self.Height - self.Margin
                                )

        dc.SetTextBackground(self.WhiteColor)
        dc.SetTextForeground(self.BlackColor)

        dc.DrawText(
                                BarText,
                                self.Width - self.Margin - BarLen/2 - dc.GetTextExtent(BarText)[0]/2 ,
                                self.Height - self.Margin - dc.GetTextExtent("A")[1] - 2
                                )


        self.SetVirtualSize((self.Width, self.Height))

    def UpdateLeafs(self, ModifiedLeafs):
        self.Refresh()
        # try: dc = wx.GCDC(wx.BufferedDC(None, self.buffer))
        # except: dc = wx.BufferedDC(None, self.buffer)

        dc = wx.BufferedDC(None, self.buffer)

        dc.SetFont(self.GetParent().Configurator.LeafFont)
        dc.SetPen(wx.Pen(self.BlackColor))
        dc.SetBackground(wx.Brush(self.WhiteColor))
        dc.SetBackgroundMode(wx.SOLID)

        for label in ModifiedLeafs:
            if label.Color:
                dc.SetTextBackground(label.Color[0])
                dc.SetTextForeground(label.Color[1])
            else :
                dc.SetTextBackground(self.WhiteColor)
                dc.SetTextForeground(self.BlackColor)
            if not label.IsActive():
                dc.SetTextBackground(self.WhiteColor)
                dc.SetTextForeground(self.RedColor)
            dc.DrawText(
                                    str(label.LabelData.LabelToWrite),
                                    label.x * self.RatioX + self.Margin + self.SepFromLine,
                                    label.y * (self.Height-(self.Margin*2)) + self.Margin - self.ShiftYLeaf
                                    )

class DefaultTreePopUpMenu(wx.Menu):
    def __init__(self, parent, ClickedPart, CurrentTreeObj):
        wx.Menu.__init__(self, "Actions")

        self.ClickedPart = ClickedPart
        ItemList =      [
                                [wx.NewId(),"Reroot from here",0]
                                ]

        if ClickedPart.IsNode() : ItemList.append([wx.NewId(),"Flip this node",1])
        if ClickedPart.IsLeaf() :
            ItemList.append([wx.NewId(),"Copy name to clipboard",2])
            ItemList.append([wx.NewId(),"Copy sequence to clipboard",3])

        self.ActionList = {}
        self.parent = parent
        for MenuItem in ItemList:
            self.Append(MenuItem[0], MenuItem[1])
            wx.EVT_MENU(self, MenuItem[0], self.OnPopMenuAction)
            self.ActionList[str(MenuItem[0])] = MenuItem[2]

        if self.parent.GetParent().ExternalData :
            ExternalDataMenu = ExternalDataLib.ExternalDataPopUpMenu(ClickedPart, CurrentTreeObj, self.parent.GetParent().ExternalData)
            self.AppendMenu(wx.NewId(), "ExternalData", ExternalDataMenu)

        if self.parent.GetParent().Configurator.db :
            DbMenu = DbClass.DataBaseTreePopUpMenu(ClickedPart, CurrentTreeObj,self.parent.GetParent().Configurator.db, self.parent.GetParent().Configurator.UserID)
            self.AppendMenu(wx.NewId(), "Database Menu", DbMenu)

    def OnPopMenuAction(self, event):
        if self.ActionList[str(event.GetId())] == 0:
            self.parent.GetParent().CurrentTreeObj.Reroot(self.ClickedPart)
            self.parent.DrawTree()
        elif self.ActionList[str(event.GetId())] == 1:
            self.parent.GetParent().CurrentTreeObj.SwapNode(self.ClickedPart)
            self.parent.DrawTree()
        elif self.ActionList[str(event.GetId())] == 2:
            clipdata = wx.TextDataObject(self.ClickedPart.LabelData.Label)
            wx.TheClipboard.Open()
            wx.TheClipboard.SetData(clipdata)
            wx.TheClipboard.Close()
        elif self.ActionList[str(event.GetId())] == 3:
            if self.parent.GetParent().ReferenceFasta :
                print('Copy to clip using ref')
                FastaEntryToFind = self.parent.GetParent().ReferenceFasta.FindSeqByDef(self.ClickedPart.LabelData.Label)
                if FastaEntryToFind:
                    clipdata = wx.TextDataObject( FastaEntryToFind.GetInFasta() )
                    wx.TheClipboard.Open()
                    wx.TheClipboard.SetData(clipdata)
                    wx.TheClipboard.Close()
            elif self.ClickedPart.LabelData.LocalID:
                query = "SELECT * FROM proteomes WHERE gene_id="+self.ClickedPart.LabelData.LocalID
                get_Protein = self.parent.GetParent().Configurator.db.query(query)
                if get_Protein.ntuples() > 0:
                    Protein = get_Protein.dictresult()[0]
                    clipdata = wx.TextDataObject( '>'+str(Protein['species_name'])+' '+str(Protein['assembly_type_medium'])+'@g_id#'+str(Protein['gene_id'])+'@p_id#'+str(Protein['protein_id'])+'\n'+str(Protein['protein_seq']))
                    wx.TheClipboard.Open()
                    wx.TheClipboard.SetData(clipdata)
                    wx.TheClipboard.Close()
