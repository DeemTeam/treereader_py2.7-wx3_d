# -*- coding: iso-8859-1 -*-

import wx, os, re
from wx.lib.pubsub import pub as Publisher

class DbPanel(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent, -1, wx.DefaultPosition, wx.DefaultSize)
		
		ID_RefreshBtn = wx.NewId()
		ID_SaveFastaBtn = wx.NewId()
		ID_CreaGroupBtn = wx.NewId()
		ID_LinkGroupBtn = wx.NewId()
		ID_CreaPhyloEvtBtn = wx.NewId()
		ID_LinkPhyloEvtBtn = wx.NewId()
		
		self.ParentalTreeFrame = parent.GetParent()
		
		V_Sizer = wx.BoxSizer(wx.VERTICAL)
		
		TitleText = wx.StaticText(self, label = "Database", style = wx.ALIGN_CENTRE)
		TitleText.SetFont(wx.Font(12, wx.DEFAULT, wx.FONTSTYLE_NORMAL, wx.NORMAL))
		V_Sizer.Add(TitleText, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		V_Sizer.Add(wx.StaticLine(self, -1, (-1,-1), (140,1), wx.HORIZONTAL), 0, wx.ALIGN_CENTER)
	
		self.DefinitionsCB = wx.CheckBox(self, -1, "Show Definitions")
		self.GICB = wx.CheckBox(self, -1, "Show GI number")
		self.RefCB = wx.CheckBox(self, -1, "Show Ref/Access number")
		self.GroupsCB = wx.CheckBox(self, -1, "Show Groups")
		
		self.RefreshNeeded = False
		
		V_Sizer.AddSpacer(3)
		V_Sizer.Add(self.DefinitionsCB, 0, wx.ALIGN_LEFT|wx.LEFT, 10)
		V_Sizer.AddSpacer(3)
		V_Sizer.Add(self.GICB, 0, wx.ALIGN_LEFT|wx.LEFT, 10)
		V_Sizer.AddSpacer(3)
		V_Sizer.Add(self.RefCB, 0, wx.ALIGN_LEFT|wx.LEFT, 10)
		V_Sizer.AddSpacer(3)
		V_Sizer.Add(self.GroupsCB, 0, wx.ALIGN_LEFT|wx.LEFT, 10)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		RefreshBtn = wx.BitmapButton(self, ID_RefreshBtn, wx.Bitmap(os.path.join(self.ParentalTreeFrame.Configurator.ImgDir, "Refresh.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		SaveFastaBtn = wx.BitmapButton(self, ID_SaveFastaBtn, wx.Bitmap(os.path.join(self.ParentalTreeFrame.Configurator.ImgDir, "SaveFasta.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
	
		H_Sizer.Add(RefreshBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(SaveFastaBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
	
		V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 0)
	
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		CreaGroupBtn = wx.BitmapButton(self, ID_CreaGroupBtn, wx.Bitmap(os.path.join(self.ParentalTreeFrame.Configurator.ImgDir, "database_process.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		LinkGroupBtn = wx.BitmapButton(self, ID_LinkGroupBtn, wx.Bitmap(os.path.join(self.ParentalTreeFrame.Configurator.ImgDir, "database_add.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		H_Sizer.Add(CreaGroupBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(LinkGroupBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
	
		#CreaPhyloEvtBtn = wx.BitmapButton(self, ID_CreaPhyloEvtBtn, wx.Bitmap(os.path.join(self.ParentalTreeFrame.Configurator.ImgDir, "database_process_evt.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
		LinkPhyloEvtBtn = wx.BitmapButton(self, ID_LinkPhyloEvtBtn, wx.Bitmap(os.path.join(self.ParentalTreeFrame.Configurator.ImgDir, "database_add_evt.png"), wx.BITMAP_TYPE_PNG), wx.DefaultPosition, wx.DefaultSize, style = wx.BU_AUTODRAW)
	
		#H_Sizer.Add(CreaPhyloEvtBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(LinkPhyloEvtBtn, 0, wx.ALIGN_CENTER|wx.ALL, 1)
	
		V_Sizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 0)
		
		self.SetSizer(V_Sizer)
		
		# Event Bindings For DB interaction 
		self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.DefinitionsCB)
		self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.GroupsCB)
		self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.GICB)
		self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox, self.RefCB)
		
		self.Bind(wx.EVT_BUTTON, self.OnRefreshBtn, RefreshBtn)
		self.Bind(wx.EVT_BUTTON, self.OnSaveFastaBtn, SaveFastaBtn)
	
		self.Bind(wx.EVT_BUTTON, self.OnCreaGroupBtn, CreaGroupBtn)
		self.Bind(wx.EVT_BUTTON, self.OnLinkGroupBtn, LinkGroupBtn)
	
		#self.Bind(wx.EVT_BUTTON, self.OnCreaPhyloEvtBtn, CreaPhyloEvtBtn)
		self.Bind(wx.EVT_BUTTON, self.OnLinkPhyloEvtBtn, LinkPhyloEvtBtn)

		Publisher.subscribe(self.OnOpenTagPanel, 'open.tag.panel')
		Publisher.subscribe(self.OnOpenGeneEditor, 'open.gene.editor')
	
	def EvtCheckBox(self, event):
		self.ParentalTreeFrame.Configurator.ShowDefinitions = self.DefinitionsCB.GetValue()
		self.ParentalTreeFrame.Configurator.ShowGroups = self.GroupsCB.GetValue()
		self.ParentalTreeFrame.Configurator.ShowGI = self.GICB.GetValue()
		self.ParentalTreeFrame.Configurator.ShowRef = self.RefCB.GetValue()
		self.RefreshNeeded = True
	
	def OnRefreshBtn(self, event):
		if self.RefreshNeeded:
			self.ParentalTreeFrame.RefreshTreePanel()
			self.RefreshNeeded = False
	
	def OnSaveFastaBtn(self, event):
		dlg = wx.FileDialog(self, "Save Selected sequences in Fasta", wildcard = "Fasta files (*.fas)|*.fas", style = wx.SAVE|wx.OVERWRITE_PROMPT|wx.FD_CHANGE_DIR)
		dlg.SetFilename(self.ParentalTreeFrame.TreeFileName.split('.')[0])
		DlgResponse = dlg.ShowModal()
		FilePath = dlg.GetPath()
		FileName = dlg.GetFilename()
		dlg.Destroy()
		if DlgResponse == wx.ID_OK and FileName != "": 
			#try :
			ofObj = open(FilePath,"w")
			SpiltPattern = re.compile("@g_id#(\d+)@p_id#")
			List_of_IDs = []
			for Leaf in self.ParentalTreeFrame.CurrentTreeObj.GetActiveLeafs():
				#LocalID = False
				# Trying to define the localID of the Sequence 
				#SlipData = SpiltPattern.split(Leaf.LabelData.Label)
				List_of_IDs.append(Leaf.LabelData.LocalID)
				#else : print "can't get local id"+str(Leaf.LabelData.Label)
			
			get_Proteins = self.ParentalTreeFrame.Configurator.db.query("SELECT * FROM proteomes WHERE gene_id IN ("+','.join(List_of_IDs)+");")
			for Proteins in  get_Proteins.dictresult():
				ofObj.write('>'+str(Proteins['species_name'])+' '+str(Proteins['assembly_type_medium'])+'@g_id#'+str(Proteins['gene_id'])+'@p_id#'+str(Proteins['protein_id'])+'\n'+str(Proteins['protein_seq'])+'\n')
			ofObj.close()
			#except:
				# Errordlg = wx.MessageDialog(self, 'Unable to save file','Error',wx.OK | wx.ICON_ERROR)
				# Errordlg.ShowModal()
				# Errordlg.Destroy()
	
	def OnCreaGroupBtn(self, event):
		dlg = DbNewGroupDialog(self, self.ParentalTreeFrame.Configurator.db, self.ParentalTreeFrame.Configurator.UserID)
		if dlg.ShowModal() == wx.ID_OK:
			query = "SELECT tag_id FROM tags WHERE tag_name='"+dlg.ChosenName.GetValue()+"' AND parent_id='"+str(dlg.ChosenDir)+"' AND db_users_id="+str(self.ParentalTreeFrame.Configurator.UserID)
			GetExistingGpname = self.ParentalTreeFrame.Configurator.db.query(query)
			if GetExistingGpname.ntuples()!=0:
				Errordlg = wx.MessageDialog(self, 'This name already exists','Error',wx.OK | wx.ICON_ERROR)
				Errordlg.ShowModal()
				Errordlg.Destroy()
			else:
				print dlg.ChosenDir, dlg.ChosenName.GetValue() , dlg.ChosenNotes.GetValue()
				if dlg.ChosenDir >=0 and dlg.ChosenName.GetValue() != "":
					query = "INSERT INTO tags VALUES(DEFAULT,'"+dlg.ChosenName.GetValue()+"','"+str(self.ParentalTreeFrame.Configurator.UserID)+"','"+dlg.ChosenNotes.GetValue()+"','"+str(dlg.ChosenDir)+"')"
					self.ParentalTreeFrame.Configurator.db.query(query)
		dlg.Destroy()
	
	def OnLinkGroupBtn(self, event):
		dlg = DbGroupingDialog(self, self.ParentalTreeFrame.Configurator.db, self.ParentalTreeFrame.Configurator.UserID)
		if dlg.ShowModal() == wx.ID_OK:
			print dlg.ChosenGroup
			if dlg.ChosenGroup != 0:
				# Making up gene_id list
				Local_IDs = []
				for Leaf in self.ParentalTreeFrame.CurrentTreeObj.GetActiveLeafs():
					if Leaf.local and Leaf.LabelData.LocalID not in Local_IDs: Local_IDs.append(Leaf.LabelData.LocalID)
				#print "All To register", Local_IDs
				# Looking for already registered Ids to tags
				GetRegistered = self.ParentalTreeFrame.Configurator.db.query("SELECT gene_id FROM gene2tag WHERE gene_id IN ("+','.join(Local_IDs)+") AND tag_id="+str(dlg.ChosenGroup))
				# And removing them from the lst to register
				for Registered in [str(i['gene_id']) for i in GetRegistered.dictresult()]:
					Local_IDs.remove(Registered)
				#print "Left To register", Local_IDs
				# Building query
				if len(Local_IDs) > 0 :
					query = "INSERT INTO gene2tag VALUES "+','.join(["(DEFAULT,"+str(SeqId)+","+str(dlg.ChosenGroup)+")" for SeqId in Local_IDs])+";"
					#print "Query", query
					self.ParentalTreeFrame.Configurator.db.query(query)
					self.RefreshNeeded = True
		dlg.Destroy()
	
	# def OnCreaPhyloEvtBtn(self, event):
		# pass
	def OnLinkPhyloEvtBtn(self, event):
		dlg = DbNewPhyloEvtDialog(self, self.ParentalTreeFrame.Configurator.db)
		if dlg.ShowModal() == wx.ID_OK:
			GroupsToActOn = []
			for GroupsChoice in dlg.ExistingGroupsChoice:
				if GroupsChoice.IsChecked() : GroupsToActOn.append(dlg.ExistingGroupsList[GroupsChoice.GetLabel()])
			if len(GroupsToActOn)>1:
				#print dlg.ChosenNotes.GetValue(), dlg.ChosenEvent
				query = "INSERT INTO phyloevents VALUES(DEFAULT,'"+str(dlg.ChosenEvent)+"','"+str(dlg.ChosenNotes.GetValue())+"') RETURNING evtid"
				insert_evt = self.ParentalTreeFrame.Configurator.db.query(query)
				inserted_evt_id = insert_evt.dictresult()[0]['evtid']
				for group in GroupsToActOn:
					query = "INSERT INTO phyloevent2groups VALUES(DEFAULT,'"+str(inserted_evt_id)+"','"+str(group)+"')" 
					self.ParentalTreeFrame.Configurator.db.query(query)
		dlg.Destroy()

	def OnOpenTagPanel(self, message):
		dlg = TagDetailsDialog(self, message.data, self.ParentalTreeFrame.Configurator.db, self.ParentalTreeFrame.Configurator.UserID)
		dlg.ShowModal()
		#dlg.Destroy()

	def OnOpenGeneEditor(self, message):
		pass
		#dlg = TagDetailsDialog(self, message.data, self.ParentalTreeFrame.Configurator.db, self.ParentalTreeFrame.Configurator.UserID)
		#dlg.ShowModal()
		#dlg.Destroy()

class DbGroupingDialog(wx.Dialog):
	def __init__(self, parent, db, UserID):
		wx.Dialog.__init__(self, parent, -1, "Link sequences to a DB group", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		# Variables
		self.ChosenGroup = 0
		self.db = db
		# Getting groups at creation
		
		GlobalSizer = wx.BoxSizer(wx.VERTICAL)
		
		GlobalSizer.Add(wx.StaticText(self, -1, "Choose the group dir"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
		self.DirsList={}
		GetDirs = self.db.query("SELECT * FROM tags WHERE is_active AND parent_id=0 AND db_users_id="+str(UserID)+" ORDER BY tag_name")
		for Dirs in GetDirs.dictresult():
			self.DirsList[Dirs['tag_name']] = int(Dirs['tag_id'])
		
		DirsChoice = wx.Choice(self, -1, (100, 50), choices = sorted(self.DirsList.keys()))
		self.Bind(wx.EVT_CHOICE, self.EvtChoiceDirs, DirsChoice)
		
		GlobalSizer.Add(DirsChoice, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		
		GlobalSizer.Add(wx.StaticText(self, -1, "Choose the group name in the Dir"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
		self.GroupsList={}
		
		self.GroupsChoice = wx.Choice(self, -1, (100, 50), choices = [])
		self.Bind(wx.EVT_CHOICE, self.EvtGroupsChoice, self.GroupsChoice)
		
		GlobalSizer.Add(self.GroupsChoice, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		btn = wx.Button(self, wx.ID_CANCEL)
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		
		GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		self.SetSizer(GlobalSizer)
		GlobalSizer.Fit(self)
	
	def EvtChoiceDirs(self, event):
		#print "Clearing Choices"
		self.GroupsChoice.Clear()
		self.GroupsList = {}
		GetGroups = self.db.query("SELECT * FROM tags WHERE is_active AND parent_id="+str(self.DirsList[event.GetString()])+" ORDER BY tag_name")
		for Groups in GetGroups.dictresult():
			self.GroupsList[Groups['tag_name']] = int(Groups['tag_id'])
		for key in sorted(self.GroupsList.keys()):
			self.GroupsChoice.Append(key)
	
	def EvtGroupsChoice(self, event):
		self.ChosenGroup = self.GroupsList[event.GetString()]

class DbNewGroupDialog(wx.Dialog):
	def __init__(self, parent, db, UserID):
		wx.Dialog.__init__(self, parent, -1, "Create a new DB group", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		# Variables
		self.ChosenDir = False
		self.db = db
		self.ChosenNameValue = False
		self.ChosenNotes = False
		# Getting groups at creation
		
		GlobalSizer = wx.BoxSizer(wx.VERTICAL)
		
		GlobalSizer.Add(wx.StaticText(self, -1, "Choose the group dir"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
		self.DirsList={}
		GetDirs = self.db.query("SELECT * FROM tags WHERE is_active AND parent_id=0 AND db_users_id="+str(UserID)+" ORDER BY tag_name")
		for Dirs in GetDirs.dictresult():
			self.DirsList[Dirs['tag_name']] = int(Dirs['tag_id'])
		
		self.DirsList['New Super Group'] = 0

		DirsChoice = wx.Choice(self, -1, (100, 50), choices = sorted(self.DirsList.keys()))
		self.Bind(wx.EVT_CHOICE, self.EvtChoiceDirs, DirsChoice)
		
		GlobalSizer.Add(DirsChoice, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		GlobalSizer.Add(wx.StaticText(self, -1, "Name of the group"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
		self.ChosenName = wx.TextCtrl(self, -1, "", size=(100, -1))
		GlobalSizer.Add(self.ChosenName, 0, wx.ALIGN_CENTER|wx.ALL, 2)
		
		GlobalSizer.Add(wx.StaticText(self, -1, "Notes"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
		self.ChosenNotes = wx.TextCtrl(self, -1, "", size=(150, -1))
		GlobalSizer.Add(self.ChosenNotes, 0, wx.ALIGN_CENTER|wx.ALL, 2)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		btn = wx.Button(self, wx.ID_CANCEL)
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		
		GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		self.SetSizer(GlobalSizer)
		GlobalSizer.Fit(self)
	
	def EvtChoiceDirs(self, event):
		self.ChosenDir = self.DirsList[event.GetString()]
		
class DbNewPhyloEvtDialog(wx.Dialog):
	def __init__(self, parent, db):
		wx.Dialog.__init__(self, parent, -1, "Create a new DB group", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		# Variables
		self.ExistingGroupsChoice = []
		self.db = db
		self.ChosenEvent = False
		# Getting groups at creation
		
		GlobalSizer = wx.BoxSizer(wx.VERTICAL)
		GlobalSizer.Add(wx.StaticText(self, -1, "Choose one or several groups"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
		# Detecter les groupes dans l'arbres
		self.ExistingGroupsList={}
		for leaf in parent.GetParent().CurrentTreeObj.GetAllLeafList():
			if leaf.label.find("@")>0:
				GetExistingGroups = self.db.query("SELECT usergroups.groupid,usergroups.groupname FROM usergroups LEFT JOIN seq2usergroups ON seq2usergroups.groupid=usergroups.groupid WHERE seq2usergroups.seqid="+leaf.label.split("@")[1].split(" ")[0]+" ORDER BY groupname")
				for Groups in GetExistingGroups.dictresult():
					if not self.ExistingGroupsList.has_key(Groups['tag_name']):
						self.ExistingGroupsList[Groups['tag_name']] = int(Groups['tag_id'])
						Choice = wx.CheckBox(self, -1, Groups['tag_name'])
						GlobalSizer.Add(Choice, 0, wx.ALIGN_CENTER|wx.ALL, 5)
						self.ExistingGroupsChoice.append(Choice)
		if len(self.ExistingGroupsList)==0:
			GlobalSizer.Add(wx.StaticText(self, -1, "There are no group to be linked"), 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		# Choisir le type d'evenement à associer
		GlobalSizer.Add(wx.StaticText(self, -1, "Type of event"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
		GetEventtypes = self.db.query("SELECT * FROM phyloeventtypes ORDER BY pevttypename")
		self.EventList = {}
		for Eventtype in GetEventtypes.dictresult():
			self.EventList[Eventtype['pevttypename']] = int(Eventtype['pevttypeid'])
		
		EventTypeChoice = wx.Choice(self, -1, (100, 50), choices = sorted(self.EventList.keys()))
		self.Bind(wx.EVT_CHOICE, self.OnEventTypeChoice, EventTypeChoice)
		GlobalSizer.Add(EventTypeChoice, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		
		GlobalSizer.Add(wx.StaticText(self, -1, "Notes"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
		self.ChosenNotes = wx.TextCtrl(self, -1, "", size=(150, -1))
		GlobalSizer.Add(self.ChosenNotes, 0, wx.ALIGN_CENTER|wx.ALL, 2)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		btn = wx.Button(self, wx.ID_CANCEL)
		H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		
		GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		self.SetSizer(GlobalSizer)
		GlobalSizer.Fit(self)
	
	def OnEventTypeChoice(self, event):
		self.ChosenEvent = self.EventList[event.GetString()]
		
class DataBaseTreePopUpMenu(wx.Menu):
	def __init__(self, ClickedPart, CurrentTreeObj, db, UserID):
		wx.Menu.__init__(self)
		
		self.ClickedPart = ClickedPart
		self.db = db
		self.CurrentTreeObj = CurrentTreeObj
		self.UserID = UserID
		
		self.LinkActionList = {}
		self.UnlinkActionList = {}
		self.MoveActionList = {}
		self.OpenTagActionList = {}
		
		# Get all Tags present in the tree
		LocalID_list = [Leaf.LabelData.LocalID for Leaf in CurrentTreeObj.GetAllLeafList() if Leaf.LabelData.LocalID]
		if len(LocalID_list)>0:
			get_Tags = self.db.query(
					"""	SELECT tag_id, tag_name, parent_name FROM gene2tag 
						JOIN tags_geneal USING(tag_id)
						WHERE db_users_id= """+str(self.UserID)+"""
						AND gene_id IN ("""+','.join(LocalID_list)+""")
						GROUP BY tag_id, tag_name, parent_name"""
						)
			AllTagsInTree = [Tag for Tag in get_Tags.dictresult()] if get_Tags.ntuples()>0 else  []
			#print "AllTagsInTree", AllTagsInTree
			
		if ClickedPart.IsLeaf() and ClickedPart.LabelData.LocalID:
			# Get geneToTagsIDs linked to this Leaf
			get_Tags = self.db.query(
						"""	SELECT tag_name, parent_name, array_agg(gene2tag_id) as gene2taglist FROM gene2tag 
							JOIN tags_geneal USING(tag_id)
							WHERE db_users_id="""+str(self.UserID)+"""
							AND gene_id IN ("""+str(ClickedPart.LabelData.LocalID)+""")
							GROUP BY tag_name, parent_name"""
							)
			TagsInLeaf = [Tag for Tag in get_Tags.dictresult()] if get_Tags.ntuples()>0 else  []
			print "TagsInLeaf", TagsInLeaf

			# Generating MENUS
			SubMenu = wx.Menu()
			for Tag in AllTagsInTree:
				NewID = wx.NewId()
				SubMenu.Append(NewID, str(Tag['tag_name'])+"("+str(Tag['parent_name'])+")")
				wx.EVT_MENU(SubMenu, NewID, self.OnLinkGroup)
				self.LinkActionList[str(NewID)] = [(str(ClickedPart.LabelData.LocalID), str(Tag['tag_id']))]
			self.AppendMenu(wx.NewId(),"Link seq. to", SubMenu)

			SubMenu = wx.Menu()
			for Tag in TagsInLeaf:
				NewID = wx.NewId()
				SubMenu.Append(NewID, str(Tag['tag_name'])+"("+str(Tag['parent_name'])+")")
				wx.EVT_MENU(SubMenu, NewID, self.OnUnlinkGroup)
				self.UnlinkActionList[str(NewID)] = Tag['gene2taglist']#.replace('{','(').replace('}',')')
			self.AppendMenu(wx.NewId(),"Unlink seq. from", SubMenu)

			for Group in ClickedPart.LabelData.Groups:
				SubMenu = wx.Menu(Group.GroupName)
				#Get_UserGroups
				NewID = wx.NewId()
				SubMenu.Append(NewID, "View genes linked to "+str(Group.GroupName))
				wx.EVT_MENU(SubMenu, NewID, self.OpenTagDetails)
				self.OpenTagActionList[str(NewID)] = str(Group.GroupID)

				Get_UserGroups = db.query("SELECT * FROM tags WHERE is_active AND parent_id=0 AND db_users_id="+str(UserID)+" ORDER BY tag_name")
				for UserGroups in Get_UserGroups.dictresult():
					NewID = wx.NewId()
					SubMenu.Append(NewID, "Move Group to "+str(UserGroups['tag_name']))
					wx.EVT_MENU(SubMenu, NewID, self.OnMoveGroup)
					self.MoveActionList[str(NewID)] = (str(Group.GroupID), str(UserGroups['tag_id']))
				self.AppendMenu(wx.NewId(), Group.GroupName, SubMenu)
		
		elif ClickedPart.IsNode():
			# Get all geneToTagsIDs present in children of this node 
			LocalID_list = [Leaf.LabelData.LocalID for Leaf in ClickedPart.GetAllLeafs() if Leaf.LabelData.LocalID and Leaf.IsActive()]
			if len(LocalID_list)>0:
				query = """	SELECT tag_name, parent_name, array_agg(gene2tag_id) as gene2taglist FROM gene2tag 
							JOIN tags_geneal USING(tag_id)
							WHERE db_users_id="""+str(self.UserID)+"""
							AND gene_id IN ("""+','.join(LocalID_list)+""")
							GROUP BY tag_name, parent_name"""
				get_Tags = self.db.query(query)
				TagsInNode = [Tag for Tag in get_Tags.dictresult()] if get_Tags.ntuples()>0 else  []
				#print "TagsInNode", TagsInNode
				
				SubMenu = wx.Menu()
				for Tag in AllTagsInTree:
					NewID = wx.NewId()
					SubMenu.Append(NewID, str(Tag['tag_name'])+"("+str(Tag['parent_name'])+")")
					wx.EVT_MENU(SubMenu, NewID, self.OnLinkGroup)
					self.LinkActionList[str(NewID)] = [(str(LocalID), str(Tag['tag_id'])) for LocalID in LocalID_list]
				self.AppendMenu(wx.NewId(),"Link ALL seqs. to", SubMenu)
					
				SubMenu = wx.Menu()
				for Tag in TagsInNode:
					NewID = wx.NewId()
					SubMenu.Append(NewID, str(Tag['tag_name'])+"("+str(Tag['parent_name'])+")")
					wx.EVT_MENU(SubMenu, NewID, self.OnUnlinkGroup)
					self.UnlinkActionList[str(NewID)] = Tag['gene2taglist'] #.replace('{','(').replace('}',')')
				self.AppendMenu(wx.NewId(),"Unlink ALL seqs. from", SubMenu)
	
	def OpenTagDetails(self, event):
		Publisher.sendMessage('open.tag.panel', self.OpenTagActionList[str(event.GetId())])

	def OnUnlinkGroup(self, event):
		query = "DELETE FROM gene2tag WHERE gene2tag_id IN ({})".format(','.join( [str(h) for h in self.UnlinkActionList[str(event.GetId())]] ))
		#print query
		self.db.query(query)
		self.CurrentTreeObj.AddDatabaseInfo(self.db, self.UserID)


	def OnLinkGroup(self, event):
		#print "OnLinkGroup"
		#print self.LinkActionList[str(event.GetId())]
		
		# SELECT
		ToRegister = [couple for couple in self.LinkActionList[str(event.GetId())] if self.db.query("SELECT gene_id FROM gene2tag WHERE gene_id = "+couple[0]+" AND tag_id="+couple[1]).ntuples() == 0] 
		#print ToRegister
		query = "INSERT INTO gene2tag VALUES "+','.join([ ','.join(['(DEFAULT',couple[0],couple[1]+')'] ) for couple in ToRegister])+";"
		#print query
		self.db.query(query)
		self.CurrentTreeObj.AddDatabaseInfo(self.db, self.UserID)

		# if self.ClickedPart.IsLeaf():
		# 	print "Unlink group"
		# 	# Unlink group:
		# 	query = " DELETE FROM gene2tag"
		# 	query+= " WHERE gene_id="+str(self.ClickedPart.LabelData.LocalID) 
		# 	query+= " AND tag_id="+str(self.UnlinkActionList[str(event.GetId())])
		# 	print query
		# 	self.db.query(query)
		# elif self.ClickedPart.IsNode():
		# 	query = "DELETE FROM gene2tag WHERE gene2tag_id IN "+self.UnlinkActionList[str(event.GetId())]
		# 	print query
		# 	self.db.query(query)
		#self.CurrentTreeObj.AddDatabaseInfo(self.db, self.UserID)
		
	def OnMoveGroup(self, event):
		# Move group:
		query = " UPDATE tags"
		query+= " SET parent_id="+str(self.MoveActionList[str(event.GetId())][1])
		query+= " WHERE tag_id="+str(self.MoveActionList[str(event.GetId())][0])
		self.db.query(query)
		
class TagDetailsDialog(wx.Dialog):
	def __init__(self, parent, tag_id, db, UserID):
		wx.Dialog.__init__(self, parent, -1, "Tag Detail", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		# Variables
		
		self.db = db
		self.tag_id = tag_id
		self.UserID = UserID
		
		# Getting and formating the list of tags
		# 1) Getting the list of genes in the tag + others attached tags
		query = """	SELECT proteomes.species_name, proteomes.assembly_type_medium, protein_id, gene_id, array_agg(tag_name) AS other_tags
					FROM proteomes 
					JOIN gene2tag USING(gene_id)
					JOIN tags USING(tag_id)
					WHERE proteomes.gene_id IN
					(
					SELECT gene_id FROM gene2tag WHERE tag_id = """+str(self.tag_id)+"""
					)
					AND tags.db_users_id = """+str(self.UserID)+"""
					GROUP BY proteomes.species_name, proteomes.assembly_type_medium, protein_id, gene_id
					ORDER BY gene_id;
				"""
		get_TaggedGenes = db.query(query)
		#print get_TaggedGenes.dictresult()

		# # 2) Getting the genealogy of tags 
		query = """	SELECT * FROM tags_geneal WHERE tag_id IN
					(
						SELECT tag_id 
						FROM proteomes 
						JOIN gene2tag USING(gene_id)
						JOIN tags USING(tag_id)
						WHERE proteomes.gene_id IN
						(
						SELECT gene_id FROM gene2tag WHERE tag_id = """+str(self.tag_id)+"""

						)
						AND tags.db_users_id = """+str(self.UserID)+"""
						GROUP BY tag_id
					);"""

		get_TagGeneal = db.query(query)
		TagIDToGeneal = {}
		for TagGeneal in get_TagGeneal.dictresult():
			TagIDToGeneal[TagGeneal['tag_id']] = TagGeneal['tag_name']+"("+TagGeneal['parent_name']+")"

		# Getting groups at creation
		GlobalSizer = wx.BoxSizer(wx.VERTICAL)
		
		self.ProteinsInTagList = wx.ListCtrl(self, -1, wx.DefaultPosition, (700,600), style=wx.LC_REPORT)
	
		self.ProteinsInTagList.InsertColumn(0, "Species", wx.LIST_FORMAT_LEFT)
		self.ProteinsInTagList.InsertColumn(1, "type", wx.LIST_FORMAT_RIGHT)
		self.ProteinsInTagList.InsertColumn(2, "g_ID", wx.LIST_FORMAT_RIGHT)
		self.ProteinsInTagList.InsertColumn(3, "p_id", wx.LIST_FORMAT_RIGHT)
		self.ProteinsInTagList.InsertColumn(4, "Belongs to", wx.LIST_FORMAT_RIGHT)
		
		for i, TaggedGenes in enumerate(get_TaggedGenes.dictresult()):
			index = self.ProteinsInTagList.InsertStringItem(i, TaggedGenes['species_name'])
			self.ProteinsInTagList.SetStringItem(index, 1, str(TaggedGenes['assembly_type_medium']))
			self.ProteinsInTagList.SetStringItem(index, 2, str(TaggedGenes['gene_id']))
			self.ProteinsInTagList.SetStringItem(index, 3, str(TaggedGenes['protein_id']))
			self.ProteinsInTagList.SetStringItem(index, 4, str(TaggedGenes['other_tags']))

		self.ProteinsInTagList.SetColumnWidth(0, wx.LIST_AUTOSIZE)
		self.ProteinsInTagList.SetColumnWidth(1, wx.LIST_AUTOSIZE)
		self.ProteinsInTagList.SetColumnWidth(2, wx.LIST_AUTOSIZE)
		self.ProteinsInTagList.SetColumnWidth(3, wx.LIST_AUTOSIZE)
		self.ProteinsInTagList.SetColumnWidth(4, wx.LIST_AUTOSIZE)

		self.ProteinsInTagList.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.OnRightClick)
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		H_Sizer.Add(self.ProteinsInTagList, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		
		GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		self.SetSizer(GlobalSizer)
		GlobalSizer.Fit(self)
	

	def OnRightClick(self, event):
		print self.ProteinsInTagList.GetItem(event.GetEventObject().GetFocusedItem(), 2).GetText()
		TempPopUpMenu = TagDetailsDialogPopUpMenu(	self, 
													self.ProteinsInTagList.GetItem(event.GetEventObject().GetFocusedItem(), 2).GetText(),
													self.db
													)
		self.PopupMenu(TempPopUpMenu)
		TempPopUpMenu.Destroy()

class TagDetailsDialogPopUpMenu(wx.Menu):
	def __init__(self, parent, g_id, db):
		wx.Menu.__init__(self, "Actions")
		
		self.g_id = g_id
		self.db = db
		self.Remove_Gen2TagIDs = {}

		Menu_id = wx.NewId()
		self.Append(Menu_id, "Copy sequence to clipboard")
		wx.EVT_MENU(self, Menu_id, self.CopySeqClip)

		# Getting attached Tags
		GetTags = self.db.query("""	SELECT gene2tag_id, tag_name FROM gene2tag 
									JOIN tags USING(tag_id)
									WHERE gene_id="""+self.g_id)
		if GetTags.ntuples() !=0 :
			for Tags in GetTags.dictresult():
				Menu_id = wx.NewId()
				self.Append(Menu_id, "Unlink this seq from "+str(Tags['tag_name']))
				wx.EVT_MENU(self, Menu_id, self.RemoveTag)
				self.Remove_Gen2TagIDs[str(Menu_id)] = str(Tags['gene2tag_id'])


	def CopySeqClip(self, event):
		query = "SELECT * FROM proteomes WHERE gene_id="+self.g_id
		get_Protein = self.db.query(query)
		if get_Protein.ntuples() > 0:
			Protein = get_Protein.dictresult()[0]
			clipdata = wx.TextDataObject( '>'+str(Protein['species_name'])+' '+str(Protein['assembly_type_medium'])+'@g_id#'+str(Protein['gene_id'])+'@p_id#'+str(Protein['protein_id'])+'\n'+str(Protein['protein_seq']))
			wx.TheClipboard.Open()
			wx.TheClipboard.SetData(clipdata)
			wx.TheClipboard.Close()

	def RemoveTag(self, event):
		query = "DELETE FROM gene2tag WHERE gene2tag_id="+self.Remove_Gen2TagIDs[str(event.GetId())]
		print query
		get_Protein = self.db.query(query)
		
class GeneEditor(wx.Dialog):
	def __init__(self, parent, gene_id, db, UserID):
		wx.Dialog.__init__(self, parent, -1, "View and edit Genes in DB", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		# Variables
		self.db = db
		self.gene_id = gene_id
		self.UserID = UserID
		
		# Getting Gene informations
		query = """	SELECT * FROM genes 
					JOIN gene2prot USING(gene_id)
					WHERE gene_id="""+str(self.gene_id)
		
		GeneData = self.db.query(query).dictresult()[1]
		
		GlobalSizer = wx.BoxSizer(wx.VERTICAL)
		
		GlobalSizer.Add(wx.StaticText(self, -1, "Details of Gene "+str(self.gene_id)), 0, wx.ALIGN_CENTER|wx.ALL, 2)
		self.ProteinSeq = wx.TextCtrl(self, -1, str(GeneData['protein_seq']), style=wx.TE_MULTILINE|wx.TE_PROCESS_ENTER|wx.TE_READONLY)
		GlobalSizer.Add(self.ProteinSeq, 0, wx.ALIGN_CENTER|wx.ALL, 5)

		#Font = wx.Font(11, wx.DEFAULT, wx.NORMAL, wx.NORMAL, faceName = "Courier")
		#self.ProteinSeq.SetDefaultStyle(wx.TextAttr("BLACK", "WHITE", Font)	)
		
	# 	self. = wx.CheckBox(ToolBar, wx.NewId(), "Update Protein sequence", wx.DefaultPosition, size = wx.DefaultSize)
		
	# 	cb.SetValue(True)


	# 	self.DirsList={}
	# 	GetDirs = self.db.query("SELECT * FROM tags WHERE is_active AND parent_id=0 AND db_users_id="+str(UserID)+" ORDER BY tag_name")
	# 	for Dirs in GetDirs.dictresult():
	# 		self.DirsList[Dirs['tag_name']] = int(Dirs['tag_id'])
		
	# 	DirsChoice = wx.Choice(self, -1, (100, 50), choices = sorted(self.DirsList.keys()))
	# 	self.Bind(wx.EVT_CHOICE, self.EvtChoiceDirs, DirsChoice)
		
	# 	GlobalSizer.Add(DirsChoice, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		
	# 	GlobalSizer.Add(wx.StaticText(self, -1, "Choose the group name in the Dir"), 0, wx.ALIGN_CENTER|wx.ALL, 2)
	# 	self.GroupsList={}
		
	# 	self.GroupsChoice = wx.Choice(self, -1, (100, 50), choices = [])
	# 	self.Bind(wx.EVT_CHOICE, self.EvtGroupsChoice, self.GroupsChoice)
		
	# 	GlobalSizer.Add(self.GroupsChoice, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		
	# 	H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
	# 	btn = wx.Button(self, wx.ID_OK)
	# 	btn.SetDefault()
	# 	H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
	# 	btn = wx.Button(self, wx.ID_CANCEL)
	# 	H_Sizer.Add(btn, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		
	# 	GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
	# 	self.SetSizer(GlobalSizer)
	# 	GlobalSizer.Fit(self)
	
	# def EvtChoiceDirs(self, event):
	# 	#print "Clearing Choices"
	# 	self.GroupsChoice.Clear()
	# 	self.GroupsList = {}
	# 	GetGroups = self.db.query("SELECT * FROM tags WHERE is_active AND parent_id="+str(self.DirsList[event.GetString()])+" ORDER BY tag_name")
	# 	for Groups in GetGroups.dictresult():
	# 		self.GroupsList[Groups['tag_name']] = int(Groups['tag_id'])
	# 	for key in sorted(self.GroupsList.keys()):
	# 		self.GroupsChoice.Append(key)
	
	# def EvtGroupsChoice(self, event):
	# 	self.ChosenGroup = self.GroupsList[event.GetString()]



	# 	wx.Dialog.__init__(self, parent, -1, "Tag Detail", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
	# 	# Variables
		
	# 	self.db = db
	# 	self.tag_id = tag_id
	# 	self.UserID = UserID
		
	# 	# Getting and formating the list of tags
	# 	# 1) Getting the list of genes in the tag + others attached tags
	# 	query = """	SELECT proteomes.species_name, proteomes.assembly_type_medium, protein_id, gene_id, array_agg(tag_name) AS other_tags
	# 				FROM proteomes 
	# 				JOIN gene2tag USING(gene_id)
	# 				JOIN tags USING(tag_id)
	# 				WHERE proteomes.gene_id IN
	# 				(
	# 				SELECT gene_id FROM gene2tag WHERE tag_id = """+str(self.tag_id)+"""

	# 				)
	# 				AND tags.db_users_id = """+str(self.UserID)+"""
	# 				GROUP BY proteomes.species_name, proteomes.assembly_type_medium, protein_id, gene_id
	# 				ORDER BY gene_id;
	# 			"""
	# 	get_TaggedGenes = db.query(query)
	# 	#print get_TaggedGenes.dictresult()

	# 	# # 2) Getting the genealogy of tags 
	# 	query = """	SELECT * FROM tags_geneal WHERE tag_id IN
	# 				(
	# 					SELECT tag_id 
	# 					FROM proteomes 
	# 					JOIN gene2tag USING(gene_id)
	# 					JOIN tags USING(tag_id)
	# 					WHERE proteomes.gene_id IN
	# 					(
	# 					SELECT gene_id FROM gene2tag WHERE tag_id = """+str(self.tag_id)+"""

	# 					)
	# 					AND tags.db_users_id = """+str(self.UserID)+"""
	# 					GROUP BY tag_id
	# 				);"""

	# 	get_TagGeneal = db.query(query)
	# 	TagIDToGeneal = {}
	# 	for TagGeneal in get_TagGeneal.dictresult():
	# 		TagIDToGeneal[TagGeneal['tag_id']] = TagGeneal['tag_name']+"("+TagGeneal['parent_name']+")"

	# 	# Getting groups at creation
	# 	GlobalSizer = wx.BoxSizer(wx.VERTICAL)
		
	# 	self.ProteinsInTagList = wx.ListCtrl(self, -1, wx.DefaultPosition, (700,600), style=wx.LC_REPORT)
	
	# 	self.ProteinsInTagList.InsertColumn(0, "Species", wx.LIST_FORMAT_LEFT)
	# 	self.ProteinsInTagList.InsertColumn(1, "type", wx.LIST_FORMAT_RIGHT)
	# 	self.ProteinsInTagList.InsertColumn(2, "g_ID", wx.LIST_FORMAT_RIGHT)
	# 	self.ProteinsInTagList.InsertColumn(3, "p_id", wx.LIST_FORMAT_RIGHT)
	# 	self.ProteinsInTagList.InsertColumn(4, "Belongs to", wx.LIST_FORMAT_RIGHT)
		
	# 	for i, TaggedGenes in enumerate(get_TaggedGenes.dictresult()):
	# 		index = self.ProteinsInTagList.InsertStringItem(i, TaggedGenes['species_name'])
	# 		self.ProteinsInTagList.SetStringItem(index, 1, str(TaggedGenes['assembly_type_medium']))
	# 		self.ProteinsInTagList.SetStringItem(index, 2, str(TaggedGenes['gene_id']))
	# 		self.ProteinsInTagList.SetStringItem(index, 3, str(TaggedGenes['protein_id']))
	# 		self.ProteinsInTagList.SetStringItem(index, 4, str(TaggedGenes['other_tags']))

	# 	self.ProteinsInTagList.SetColumnWidth(0, wx.LIST_AUTOSIZE)
	# 	self.ProteinsInTagList.SetColumnWidth(1, wx.LIST_AUTOSIZE)
	# 	self.ProteinsInTagList.SetColumnWidth(2, wx.LIST_AUTOSIZE)
	# 	self.ProteinsInTagList.SetColumnWidth(3, wx.LIST_AUTOSIZE)
	# 	self.ProteinsInTagList.SetColumnWidth(4, wx.LIST_AUTOSIZE)

	# 	self.ProteinsInTagList.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.OnRightClick)
		
	# 	H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
	# 	H_Sizer.Add(self.ProteinsInTagList, 0, wx.ALIGN_CENTER|wx.ALL, 3)
		
	# 	GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 5)
		
		self.SetSizer(GlobalSizer)
		GlobalSizer.Fit(self)
