# -*- coding: iso-8859-1 -*-

from wx import SystemSettings
from wx import SYS_DEFAULT_GUI_FONT, Timer

class ConfigObject(object):
	def __init__(self):
		self.DebugMode = False
		# User
		self.UserName = ''
		self.UserID = ''
		self.DBuser = ''
		self.DBpass = ''
		# Directories
		self.DefaultDir = ''
		self.SoftDir = ''
		self.ImgDir = ''
		self.DbData = ''
		# Color Stuff
		self.UseColors = False
		self.Nodes = False
		self.NamesToSets = {}

		#self.TermsIndexes = {}
		#self.TermsTree = {}
		self.MyColors = {}

		#self.SingleTerms = []
		#self.TermsToTaxID = []

		# Tree Config Stuff
		self.NodeFont = SystemSettings.GetFont(SYS_DEFAULT_GUI_FONT)
		self.LeafFont = SystemSettings.GetFont(SYS_DEFAULT_GUI_FONT)
		self.Expansion = 1
		# Database Stuff
		self.UseDB = False
		self.pgServerIP = ''
		self.pgUser = False
		self.pgPassword = False
		self.pgDatabase = False

		self.db = False
		self.ShowGI = False
		self.ShowRef = False
		self.ShowDefinitions = False
		self.ShowGroups = False
		# Local Data using file Stuff
		self.ShowExternalData = False

		self.ExcludeLabels = False
		self.MoveLabels = False



def importIniFile(iniObj):
	Configurator = ConfigObject()
	for line in iniObj:
		if not line.startswith('#') and not len(line)<=5:
			line = line.strip("\r\n").replace(' ','')
			line = line.split('#')[0]
			data = line.split(":")
			# ---- Directories
			if data[0] == 'DefaultDir' : 		Configurator.DefaultDir = data[1]
			
			# ---- Users
			if data[0] == 'UserName' : 		Configurator.UserName = data[1]

			# Color Stuff
			elif data[0] == 'UseColors' : 		Configurator.UseColors = str2bool(data[1])
			
			# ---- Tree Config Stuff
			elif data[0] == 'NodeFontType' :
				if data[1] != 'Default' :	Configurator.NodeFont.SetNativeFontInfo(data[1])
			elif data[0] == 'NodeFontSize' : 	Configurator.NodeFont.SetPointSize(int(data[1]))
			elif data[0] == 'LeafFontType' : 	
				if data[1] != 'Default' :	Configurator.LeafFont.SetNativeFontInfo(data[1])
			elif data[0] == 'LeafFontSize' : 	Configurator.LeafFont.SetPointSize(int(data[1]))
			elif data[0] == 'Expansion' : 		Configurator.Expansion = int(data[1])
			
			# ---- Database Stuff
			elif data[0] == 'UseDB' : 			Configurator.UseDB = str2bool(data[1])
			elif data[0] == 'pgServerIP' : 		Configurator.pgServerIP = data[1]
			elif data[0] == 'pgUser' : 			Configurator.pgUser = data[1]
			elif data[0] == 'pgPassword' : 		Configurator.pgPassword = data[1]
			elif data[0] == 'pgDatabase' : 		Configurator.pgDatabase = data[1]

			# ---- CorePanStuff
			elif data[0] == 'UseExternalGpFile' : 	Configurator.UseExternalGpFile = str2bool(data[1])
			elif data[0] == 'ShowExternalGroups' : 	Configurator.UseExternalGpFile = str2bool(data[1])

			# PDF styles
			elif data[0] == 'ExcludeLabels' : 	Configurator.ExcludeLabels = str2bool(data[1])
			elif data[0] == 'MoveLabels' : 	Configurator.MoveLabels = str2bool(data[1])

	return Configurator
	
def str2bool(v):
	return v.lower() in ["yes", "true", "t", "1"]

