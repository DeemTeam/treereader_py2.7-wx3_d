# -*- coding: iso-8859-1 -*-

import wx

class SettingsDialog(wx.Frame):
	def __init__(self, parent):
		wx.Frame.__init__(self, parent, -1, 'Display Settings', wx.DefaultPosition, (460,290), wx.DEFAULT_FRAME_STYLE | wx.CLIP_CHILDREN | wx.FRAME_FLOAT_ON_PARENT)
		colorPanel = wx.Panel(self, -1)
		self.SetMinSize(wx.Size(460,290))
		
	# VARIABLES
		self.MyColorSettings = {}
		for items in self.GetParent().Configurator.MyColors.iteritems():
			self.MyColorSettings[items[1][0]] = [items[0],items[1][1]]
		
		self.selected_in_list = 0
		self.taxonList = []
		self.colorList = []
		self.taxidList = []
		
		ID_CHG_COLOR_BUTTON = wx.NewId()
		ID_ACCEPT_COLOR_BUTTON = wx.NewId()
		ID_CANCEL_COLOR_BUTTON = wx.NewId()
	# // VARIABLES

	# Get the list of define taxon
		#self.MakeLists()
	
	#Print the Panel Content
		
		GlobalSizer = wx.BoxSizer(wx.VERTICAL)
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		
		V_Sizer = wx.BoxSizer(wx.VERTICAL)
		H_Sizer.Add(V_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		
		
		V_Sizer.Add(wx.StaticText(colorPanel, -1, "Taxon names"), 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		self.TaxonListBox = wx.ListBox(colorPanel, -1, wx.DefaultPosition, (180, 160), self.MyColorSettings.keys(), wx.LB_SINGLE)
		
		#self.TaxonListBox.SetSelection(0)
		V_Sizer.Add(self.TaxonListBox, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		# previewBitmap = self.CreateColorBitmap(self.colorList[self.selected_in_list])
		# self.preview = wx.StaticBitmap(colorPanel, -1, previewBitmap, (20,191))
		wx.Button(colorPanel, ID_CHG_COLOR_BUTTON,"Change color", wx.DefaultPosition, wx.DefaultSize)
		wx.StaticLine(colorPanel, -1, (200,10), (2,200), wx.LI_VERTICAL)
		
		GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		
		
		H_Sizer = wx.BoxSizer(wx.HORIZONTAL)
		
		H_Sizer.Add(wx.Button(colorPanel, ID_ACCEPT_COLOR_BUTTON,"Accept changes", wx.DefaultPosition, wx.DefaultSize), 1, wx.ALIGN_CENTER|wx.ALL, 1)
		H_Sizer.Add(wx.Button(colorPanel, ID_CANCEL_COLOR_BUTTON,"Cancel", wx.DefaultPosition, wx.DefaultSize), 1, wx.ALIGN_CENTER|wx.ALL, 1)
		GlobalSizer.Add(H_Sizer, 0, wx.ALIGN_CENTER|wx.ALL, 1)
		
		colorPanel.SetSizer(GlobalSizer)
	
	# Bindings
		# self.Bind(wx.EVT_LISTBOX, self.EvtListBox, self.TaxonListBox)
		# self.Bind(wx.EVT_BUTTON, self.OnChgColorBtn, id=ID_CHG_COLOR_BUTTON)
		self.Bind(wx.EVT_BUTTON, self.OnAccept, id=ID_ACCEPT_COLOR_BUTTON)
		self.Bind(wx.EVT_BUTTON, self.OnCancel, id=ID_CANCEL_COLOR_BUTTON)
		self.Bind(wx.EVT_CLOSE, self.OnClose)

	# def EvtListBox(self, event):
		# self.selected_in_list = event.GetInt()
		# previewBitmap = self.CreateColorBitmap(self.colorList[self.selected_in_list])
		# self.preview.SetBitmap(previewBitmap)
		
	# def OnChgColorBtn(self, event):
		# ColorData = wx.ColourData()
		# ColorData.SetChooseFull(True)
		# ColorData.SetColour(self.ConvertColor(self.colorList[self.selected_in_list]))
		# dlg = wx.ColourDialog(self, ColorData)
		# if dlg.ShowModal() == wx.ID_OK:
			# data = dlg.GetColourData()
			# self.colorList[self.selected_in_list] = str(data.GetColour().Red())+":"+str(data.GetColour().Green())+":"+str(data.GetColour().Blue())
			# previewBitmap = self.CreateColorBitmap(self.colorList[self.selected_in_list])
			# self.preview.SetBitmap(previewBitmap)
		# dlg.Destroy()
		
	def CreateColorBitmap(self, color):
		image = wx.EmptyImage(30, 20)
		color = color.split(":")
		for x in xrange(30):
			for y in xrange(20):
				image.SetRGB(x, y, int(color[0]), int(color[1]), int(color[2]))
		return image.ConvertToBitmap()
	
	def ConvertColor(self, MyColor):
		MyColor = MyColor.split(":")
		return wx.Colour(int(MyColor[0]), int(MyColor[1]), int(MyColor[2]))
		
	def OnClose(self, event):
		if event.CanVeto():
			event.Veto()
		self.MakeLists()
		self.selected_in_list = 0
		self.TaxonListBox.SetSelection(0)
		previewBitmap = self.CreateColorBitmap(self.colorList[self.selected_in_list])
		self.preview.SetBitmap(previewBitmap)
		self.Hide()
		
	def OnCancel(self, event):
		self.MakeLists()
		self.selected_in_list = 0
		self.TaxonListBox.SetSelection(0)
		previewBitmap = self.CreateColorBitmap(self.colorList[self.selected_in_list])
		self.preview.SetBitmap(previewBitmap)
		self.Hide()
	
	def OnAccept(self, event):
		fileString = ''
		for i in range(len(self.taxidList)):
			fileString += self.taxidList[i]+"|"+self.colorList[i]+"|"+self.taxonList[i].split(" (")[0]+"\n"
		ofObj = open("mycolors.lst",'w')
		ofObj.write(fileString)
		ofObj.close()
		self.MakeLists(True)
		self.selected_in_list = 0
		self.TaxonListBox.SetSelection(0)
		previewBitmap = self.CreateColorBitmap(self.colorList[self.selected_in_list])
		self.preview.SetBitmap(previewBitmap)
		self.Hide()
