## TreeReader

A program to visualize and interact with phylogenetic trees, written in python 2 and wxWidgets

This program is distributed under the wxWindows Library Licence, which is the more specific license of all libraries used.


__!!!!! WARNING !!!!!__

_This program is not stable and contains many bugs._

_It should not be used in production in its present state._


### Installation

Version of libraries used are old, the best way to make the program work is to install it into a conda environment.

1. Install conda 
2. Activate base environment
3. Create a new environmeent using python 2.7 `conda create -n /path/to/env python=2.7`
4. Load the environement `conda activate /path/to/env`
5. Install dependencies :
```
conda install -c free wxpython
pip install pygresql
pip install reportlab
```
6. Clone this repository in /path/to/env 
```
cd /path/to/env
git clone !url
```

### Usage

With the Conda environement loaded, run python `/path/to/env/TreeReader_py2.7-wx3_d/TreeReader.py`

